/*
 Navicat Premium Data Transfer

 Source Server         : csjsolutions
 Source Server Type    : MySQL
 Source Server Version : 50644
 Source Host           : 166.62.6.66:3306
 Source Schema         : butuanwheels

 Target Server Type    : MySQL
 Target Server Version : 50644
 File Encoding         : 65001

 Date: 24/01/2020 17:51:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_admin
-- ----------------------------
DROP TABLE IF EXISTS `tbl_admin`;
CREATE TABLE `tbl_admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  PRIMARY KEY (`admin_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_admin
-- ----------------------------
BEGIN;
INSERT INTO `tbl_admin` VALUES (1, 'admin', 'admin@gmail.com', 'admin', 'admin', 1);
COMMIT;

-- ----------------------------
-- Table structure for tbl_brand
-- ----------------------------
DROP TABLE IF EXISTS `tbl_brand`;
CREATE TABLE `tbl_brand` (
  `brand_id` int(200) NOT NULL AUTO_INCREMENT,
  `brand` varchar(200) NOT NULL,
  PRIMARY KEY (`brand_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_brand
-- ----------------------------
BEGIN;
INSERT INTO `tbl_brand` VALUES (1, 'Toyota');
INSERT INTO `tbl_brand` VALUES (2, 'Mitsubishi');
INSERT INTO `tbl_brand` VALUES (5, 'Ford');
INSERT INTO `tbl_brand` VALUES (6, 'Suzuki');
INSERT INTO `tbl_brand` VALUES (7, 'Hyundai');
INSERT INTO `tbl_brand` VALUES (8, 'Chevrolet');
INSERT INTO `tbl_brand` VALUES (9, 'Kia');
INSERT INTO `tbl_brand` VALUES (10, 'Isuzu');
INSERT INTO `tbl_brand` VALUES (11, 'HI-ACE');
INSERT INTO `tbl_brand` VALUES (12, 'Nissan');
COMMIT;

-- ----------------------------
-- Table structure for tbl_car
-- ----------------------------
DROP TABLE IF EXISTS `tbl_car`;
CREATE TABLE `tbl_car` (
  `car_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(200) NOT NULL,
  `brand_id` varchar(200) NOT NULL,
  `model_id` varchar(200) NOT NULL,
  `capacity` varchar(200) NOT NULL,
  `transmission` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `active` int(11) DEFAULT NULL,
  `imei` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`car_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_car
-- ----------------------------
BEGIN;
INSERT INTO `tbl_car` VALUES (11, '1579656891.jpg', '1', '3', '4-5', 'Automatic', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nis', 1, '123');
INSERT INTO `tbl_car` VALUES (12, '1578522936.jpg', '2', '5', '7-8', 'Automatic', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi u', 1, NULL);
INSERT INTO `tbl_car` VALUES (13, '1578522886.jpg', '1', '7', '5', 'Automatic', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi u', 1, NULL);
INSERT INTO `tbl_car` VALUES (18, '1578522347.jpg', '9', '25', '4', 'Automatic', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi u', 1, NULL);
INSERT INTO `tbl_car` VALUES (19, '1579657033.jpg', '1', '3', '4-5', 'Automatic', 'Red Vios', 1, '123');
INSERT INTO `tbl_car` VALUES (20, '1579657428.jpg', '1', '3', '4-5', 'Automatic', 'Gray Vios', 1, '123');
INSERT INTO `tbl_car` VALUES (21, '1579657393.jpg', '8', '21', '4-5', 'Automatic', 'Chevrolet Blue Green', 1, '123');
INSERT INTO `tbl_car` VALUES (22, '1579657686.jpg', '5', '6', '5-6', 'Manual', 'Ford Ecosport Blue', 1, '');
INSERT INTO `tbl_car` VALUES (23, '1579657855.jpg', '5', '26', '6-7', 'Manual', 'Ford Ranger Wildtrak 4x4 Orange', 1, '');
INSERT INTO `tbl_car` VALUES (24, '1579658042.jpg', '11', '27', '6-10', 'Manual', 'White Van di mangidnap', 1, '');
INSERT INTO `tbl_car` VALUES (25, '1579658150.jpg', '7', '23', '4-5', 'Automatic', 'Hyundai Accent Black', 1, '');
INSERT INTO `tbl_car` VALUES (26, '1579658245.jpg', '7', '23', '4-5', 'Automatic', 'Hyundai Accent Blue', 1, '');
INSERT INTO `tbl_car` VALUES (27, '1579658331.jpg', '7', '23', '4-5', 'Automatic', 'Hyundai Accent Red', 1, '');
INSERT INTO `tbl_car` VALUES (28, '1579658439.jpg', '7', '23', '4-5', 'Automatic', 'Hyundai Accent White', 1, '');
INSERT INTO `tbl_car` VALUES (29, '1579658517.jpg', '7', '23', '4-5', 'Manual', 'Hyundai Accent White', 1, '');
INSERT INTO `tbl_car` VALUES (30, '1579658645.jpg', '7', '22', '4-5', 'Manual', 'Hyundai Elantra Red', 1, '');
INSERT INTO `tbl_car` VALUES (31, '1579658774.jpg', '7', '28', '4-5', 'Manual', 'Hyundai Getz Black', 1, '');
INSERT INTO `tbl_car` VALUES (32, '1579658879.jpg', '10', '20', '5-6', 'Automatic', 'Isuzu MUX white', 1, '');
INSERT INTO `tbl_car` VALUES (33, '1579658983.jpg', '9', '25', '4-5', 'Automatic', 'Kia Picanto Blue', 1, '');
INSERT INTO `tbl_car` VALUES (34, '1579659143.jpg', '2', '29', '4-5', 'Manual', 'Mitsubishi Mirage G4', 1, '');
INSERT INTO `tbl_car` VALUES (35, '1579659251.jpg', '2', '5', '6-8', 'Automatic', 'Mitsubishi Montero Sport Gray', 1, '');
INSERT INTO `tbl_car` VALUES (36, '1579659354.jpg', '2', '5', '6-8', 'Manual', 'Mitsubishi Montero Sport Gray', 1, '');
INSERT INTO `tbl_car` VALUES (37, '1579659459.jpg', '2', '5', '6-8', 'Manual', 'Mitsubishi Montero Sport White', 1, '');
INSERT INTO `tbl_car` VALUES (38, '1579659691.jpg', '12', '30', '4-6', 'Automatic', 'Nissan Almerra', 1, '');
INSERT INTO `tbl_car` VALUES (39, '1579659901.jpg', '6', '31', '6-8', 'Manual', 'Suzuki APV Maroon', 1, '');
INSERT INTO `tbl_car` VALUES (40, '1579660022.jpg', '1', '18', '4-6', 'Automatic', 'Toyota Avanza Veloz White', 1, '');
INSERT INTO `tbl_car` VALUES (41, '1579660186.jpg', '1', '7', '4-6', 'Automatic', 'Toyota Fortuner Creamy', 1, '');
INSERT INTO `tbl_car` VALUES (42, '1579660314.jpg', '1', '7', '4-6', 'Manual', 'Toyota Fortuner White', 1, '');
INSERT INTO `tbl_car` VALUES (43, '1579660516.jpg', '1', '32', '6-8', 'Manual', 'Toyota Innova White', 1, '');
INSERT INTO `tbl_car` VALUES (44, '1579660641.jpg', '1', '33', '4-5', 'Automatic', 'Toyota Wigo Black', 1, '');
INSERT INTO `tbl_car` VALUES (45, '1579660736.jpg', '1', '33', '4-5', 'Automatic', 'Toyota Wigo White', 1, '');
COMMIT;

-- ----------------------------
-- Table structure for tbl_car_model
-- ----------------------------
DROP TABLE IF EXISTS `tbl_car_model`;
CREATE TABLE `tbl_car_model` (
  `model_id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`model_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_car_model
-- ----------------------------
BEGIN;
INSERT INTO `tbl_car_model` VALUES (3, 1, 'Vios');
INSERT INTO `tbl_car_model` VALUES (5, 2, 'Montero Sport');
INSERT INTO `tbl_car_model` VALUES (6, 5, 'Ecosport');
INSERT INTO `tbl_car_model` VALUES (7, 1, 'Fortuner');
INSERT INTO `tbl_car_model` VALUES (18, 1, 'Avanza');
INSERT INTO `tbl_car_model` VALUES (19, 6, 'Ciaz');
INSERT INTO `tbl_car_model` VALUES (20, 10, 'MUX (soon)');
INSERT INTO `tbl_car_model` VALUES (21, 8, 'Trailblazer');
INSERT INTO `tbl_car_model` VALUES (22, 7, 'Elantra');
INSERT INTO `tbl_car_model` VALUES (23, 7, 'Accent');
INSERT INTO `tbl_car_model` VALUES (24, 7, 'Reina');
INSERT INTO `tbl_car_model` VALUES (25, 9, 'Picanto');
INSERT INTO `tbl_car_model` VALUES (26, 5, 'Ranger');
INSERT INTO `tbl_car_model` VALUES (27, 11, 'GL Grandia');
INSERT INTO `tbl_car_model` VALUES (28, 7, 'Getz');
INSERT INTO `tbl_car_model` VALUES (29, 2, 'Mirage G4');
INSERT INTO `tbl_car_model` VALUES (30, 12, 'Almerra');
INSERT INTO `tbl_car_model` VALUES (31, 6, 'APV');
INSERT INTO `tbl_car_model` VALUES (32, 1, 'Innova');
INSERT INTO `tbl_car_model` VALUES (33, 1, 'Wigo');
COMMIT;

-- ----------------------------
-- Table structure for tbl_car_rate
-- ----------------------------
DROP TABLE IF EXISTS `tbl_car_rate`;
CREATE TABLE `tbl_car_rate` (
  `rate_id` int(11) NOT NULL AUTO_INCREMENT,
  `car_id` int(11) DEFAULT NULL,
  `coverage` varchar(255) DEFAULT NULL,
  `rate` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`rate_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_car_rate
-- ----------------------------
BEGIN;
INSERT INTO `tbl_car_rate` VALUES (10, 18, 'BXU', '1000');
INSERT INTO `tbl_car_rate` VALUES (11, 18, 'BXU-CDO', '2000');
INSERT INTO `tbl_car_rate` VALUES (12, 18, 'BXU-DAVAO', '5000');
INSERT INTO `tbl_car_rate` VALUES (13, 13, 'Butuan City Only', '1000');
INSERT INTO `tbl_car_rate` VALUES (14, 12, 'Butuan City Only', '1000');
INSERT INTO `tbl_car_rate` VALUES (15, 11, 'Butuan City Only', '1000');
INSERT INTO `tbl_car_rate` VALUES (16, 19, 'Butuan City Only', '1000');
INSERT INTO `tbl_car_rate` VALUES (17, 19, 'Butuan City to CDO', '2000');
INSERT INTO `tbl_car_rate` VALUES (18, 19, 'Butuan City to Davao', '3000');
INSERT INTO `tbl_car_rate` VALUES (19, 19, 'Within Caraga', '2500');
INSERT INTO `tbl_car_rate` VALUES (20, 11, 'Butuan City Only', '2000');
INSERT INTO `tbl_car_rate` VALUES (21, 11, 'With in Caraga', '2500');
INSERT INTO `tbl_car_rate` VALUES (22, 11, 'Davao/CDO', '3000');
INSERT INTO `tbl_car_rate` VALUES (23, 19, 'Butuan City', '2000');
INSERT INTO `tbl_car_rate` VALUES (24, 19, 'Within Caraga', '2500');
INSERT INTO `tbl_car_rate` VALUES (25, 19, 'Davao/CDO', '3000');
INSERT INTO `tbl_car_rate` VALUES (26, 20, 'Butuan City', '2000');
INSERT INTO `tbl_car_rate` VALUES (27, 20, 'Within Caraga', '2500');
INSERT INTO `tbl_car_rate` VALUES (28, 20, 'Davao/CDO', '3000');
INSERT INTO `tbl_car_rate` VALUES (29, 21, 'Self Drive', '4000');
INSERT INTO `tbl_car_rate` VALUES (30, 22, 'Butuan City', '2200');
INSERT INTO `tbl_car_rate` VALUES (31, 22, 'Within Caraga', '2700');
INSERT INTO `tbl_car_rate` VALUES (32, 22, 'Davao/CDO', '3200');
INSERT INTO `tbl_car_rate` VALUES (33, 23, 'Self Drive', '4000');
INSERT INTO `tbl_car_rate` VALUES (34, 24, 'Self Drive', '5000');
INSERT INTO `tbl_car_rate` VALUES (35, 25, 'Butuan City', '2000');
INSERT INTO `tbl_car_rate` VALUES (36, 25, 'Within Caraga', '2500');
INSERT INTO `tbl_car_rate` VALUES (37, 25, 'Davao/CDO', '3000');
INSERT INTO `tbl_car_rate` VALUES (38, 26, 'Butuan City', '2000');
INSERT INTO `tbl_car_rate` VALUES (39, 26, 'Within Caraga', '2500');
INSERT INTO `tbl_car_rate` VALUES (40, 26, 'Davao/CDO', '3000');
INSERT INTO `tbl_car_rate` VALUES (41, 27, 'Butuan City', '2000');
INSERT INTO `tbl_car_rate` VALUES (42, 27, 'Within Caraga', '2500');
INSERT INTO `tbl_car_rate` VALUES (43, 27, 'Davao/CDO', '3000');
INSERT INTO `tbl_car_rate` VALUES (44, 28, 'Butuan City', '2000');
INSERT INTO `tbl_car_rate` VALUES (45, 28, 'Within Caraga', '2500');
INSERT INTO `tbl_car_rate` VALUES (46, 28, 'Davao/CDO', '3000');
INSERT INTO `tbl_car_rate` VALUES (47, 29, 'Butuan City', '2000');
INSERT INTO `tbl_car_rate` VALUES (48, 29, 'Within Caraga', '2500');
INSERT INTO `tbl_car_rate` VALUES (49, 29, 'Davao/CDO', '3000');
INSERT INTO `tbl_car_rate` VALUES (50, 30, 'Butuan City', '2000');
INSERT INTO `tbl_car_rate` VALUES (51, 30, 'Within Caraga', '2500');
INSERT INTO `tbl_car_rate` VALUES (52, 30, 'Davao/CDO', '3000');
INSERT INTO `tbl_car_rate` VALUES (53, 31, 'Butuan City', '1500');
INSERT INTO `tbl_car_rate` VALUES (54, 31, 'Within Caraga', '2000');
INSERT INTO `tbl_car_rate` VALUES (55, 31, 'Davao/CDO', '2500');
INSERT INTO `tbl_car_rate` VALUES (56, 32, 'Butuan City', '2700');
INSERT INTO `tbl_car_rate` VALUES (57, 32, 'Within Caraga', '3200');
INSERT INTO `tbl_car_rate` VALUES (58, 32, 'Davao/CDO', '3500');
INSERT INTO `tbl_car_rate` VALUES (59, 33, 'Butuan City', '1200');
INSERT INTO `tbl_car_rate` VALUES (60, 33, 'Within Caraga', '1700');
INSERT INTO `tbl_car_rate` VALUES (61, 33, 'Davao/CDO', '2200');
INSERT INTO `tbl_car_rate` VALUES (62, 34, 'Butuan City', '1800');
INSERT INTO `tbl_car_rate` VALUES (63, 34, 'Within Caraga', '2300');
INSERT INTO `tbl_car_rate` VALUES (64, 34, 'Davao/CDO', '2800');
INSERT INTO `tbl_car_rate` VALUES (65, 35, 'Butuan City', '2700');
INSERT INTO `tbl_car_rate` VALUES (66, 35, 'Within Caraga', '3200');
INSERT INTO `tbl_car_rate` VALUES (67, 35, 'Davao/CDO', '3500');
INSERT INTO `tbl_car_rate` VALUES (68, 36, 'Butuan City', '2500');
INSERT INTO `tbl_car_rate` VALUES (69, 36, 'Within Caraga', '3000');
INSERT INTO `tbl_car_rate` VALUES (70, 36, 'Davao/CDO', '3500');
INSERT INTO `tbl_car_rate` VALUES (71, 37, 'Butuan City', '2500');
INSERT INTO `tbl_car_rate` VALUES (72, 37, 'Within Caraga', '3000');
INSERT INTO `tbl_car_rate` VALUES (73, 37, 'Davao/CDO', '3500');
INSERT INTO `tbl_car_rate` VALUES (74, 38, 'Butuan City', '2000');
INSERT INTO `tbl_car_rate` VALUES (75, 38, 'Within Caraga', '2500');
INSERT INTO `tbl_car_rate` VALUES (76, 38, 'Davao/CDO', '3000');
INSERT INTO `tbl_car_rate` VALUES (77, 39, 'Butuan City', '2500');
INSERT INTO `tbl_car_rate` VALUES (78, 39, 'Within Caraga', '3000');
INSERT INTO `tbl_car_rate` VALUES (79, 39, 'Davao/CDO', '3500');
INSERT INTO `tbl_car_rate` VALUES (80, 40, 'Butuan City', '2400');
INSERT INTO `tbl_car_rate` VALUES (81, 40, 'Within Caraga', '2900');
INSERT INTO `tbl_car_rate` VALUES (82, 40, 'Davao/CDO', '3400');
INSERT INTO `tbl_car_rate` VALUES (83, 41, 'Butuan City', '2700');
INSERT INTO `tbl_car_rate` VALUES (84, 41, 'Within Caraga', '3200');
INSERT INTO `tbl_car_rate` VALUES (85, 41, 'Davao/CDO', '3500');
INSERT INTO `tbl_car_rate` VALUES (86, 42, 'Butuan City', '2500');
INSERT INTO `tbl_car_rate` VALUES (87, 42, 'Within Caraga', '3000');
INSERT INTO `tbl_car_rate` VALUES (88, 42, 'Davao/CDO', '3500');
INSERT INTO `tbl_car_rate` VALUES (89, 43, 'Butuan City', '2500');
INSERT INTO `tbl_car_rate` VALUES (90, 43, 'Within Caraga', '3000');
INSERT INTO `tbl_car_rate` VALUES (91, 43, 'Davao/CDO', '3500');
INSERT INTO `tbl_car_rate` VALUES (92, 44, 'Butuan City', '1500');
INSERT INTO `tbl_car_rate` VALUES (93, 44, 'Within Caraga', '2000');
INSERT INTO `tbl_car_rate` VALUES (94, 44, 'Davao/CDO', '2500');
INSERT INTO `tbl_car_rate` VALUES (95, 45, 'Butuan City', '1500');
INSERT INTO `tbl_car_rate` VALUES (96, 45, 'Within Caraga', '2000');
INSERT INTO `tbl_car_rate` VALUES (97, 45, 'Davao/CDO', '2500');
COMMIT;

-- ----------------------------
-- Table structure for tbl_client
-- ----------------------------
DROP TABLE IF EXISTS `tbl_client`;
CREATE TABLE `tbl_client` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `car_id` int(11) NOT NULL,
  `car_rate` varchar(255) DEFAULT NULL,
  `client_name` varchar(255) NOT NULL,
  `client_email` varchar(255) NOT NULL,
  `client_phone` varchar(255) NOT NULL,
  `client_note` varchar(255) DEFAULT NULL,
  `transaction_type` varchar(255) NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `transaction_status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`client_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_client
-- ----------------------------
BEGIN;
INSERT INTO `tbl_client` VALUES (7, 18, 'BXU-CDO: 2000', 'testing testing', 'test123@gmail.com', '09109680920', 'test', 'Self Drive', '2020-01-14 07:00:00', '2020-01-15 18:00:00', 'Pending');
INSERT INTO `tbl_client` VALUES (8, 11, 'Butuan City Only: 1000', 'testing testing', 'joshuasaubon@gmail.com', '8134409146', 'testing only', 'Self Drive', '2020-01-16 06:00:00', '2020-01-16 18:00:00', 'Pending');
COMMIT;

-- ----------------------------
-- View structure for view_tbl_car
-- ----------------------------
DROP VIEW IF EXISTS `view_tbl_car`;
CREATE ALGORITHM=UNDEFINED DEFINER=`csjsolutions`@`49.146.%.%` SQL SECURITY DEFINER VIEW `view_tbl_car` AS select `tbl_car`.`car_id` AS `car_id`,`tbl_car`.`imei` AS `imei`,`tbl_car`.`active` AS `active`,`tbl_car`.`image` AS `image`,`tbl_car`.`brand_id` AS `brand_id`,`tbl_car`.`model_id` AS `model_id`,`tbl_car`.`capacity` AS `capacity`,`tbl_car`.`transmission` AS `transmission`,`tbl_car`.`description` AS `description`,(select `tbl_brand`.`brand` from `tbl_brand` where (`tbl_brand`.`brand_id` = `tbl_car`.`brand_id`)) AS `brand`,(select `tbl_car_model`.`model` from `tbl_car_model` where (`tbl_car_model`.`model_id` = `tbl_car`.`model_id`)) AS `model`,(select group_concat(concat(`tbl_car_rate`.`coverage`,': ',`tbl_car_rate`.`rate`) separator '<br>') from `tbl_car_rate` where (`tbl_car_rate`.`car_id` = `tbl_car`.`car_id`)) AS `rate` from `tbl_car`;

-- ----------------------------
-- View structure for view_tbl_car_model
-- ----------------------------
DROP VIEW IF EXISTS `view_tbl_car_model`;
CREATE ALGORITHM=UNDEFINED DEFINER=`csjsolutions`@`49.146.%.%` SQL SECURITY DEFINER VIEW `view_tbl_car_model` AS select `tbl_car_model`.`model_id` AS `model_id`,`tbl_car_model`.`brand_id` AS `brand_id`,`tbl_car_model`.`model` AS `model`,(select `tbl_brand`.`brand` from `tbl_brand` where (`tbl_brand`.`brand_id` = `tbl_car_model`.`brand_id`) limit 1) AS `brand_name` from `tbl_car_model`;

-- ----------------------------
-- View structure for view_tbl_client
-- ----------------------------
DROP VIEW IF EXISTS `view_tbl_client`;
CREATE ALGORITHM=UNDEFINED DEFINER=`csjsolutions`@`49.146.%.%` SQL SECURITY DEFINER VIEW `view_tbl_client` AS select `tbl_client`.`client_id` AS `client_id`,`tbl_client`.`car_id` AS `car_id`,`tbl_client`.`car_rate` AS `car_rate`,`tbl_client`.`client_name` AS `client_name`,`tbl_client`.`client_email` AS `client_email`,`tbl_client`.`client_phone` AS `client_phone`,`tbl_client`.`client_note` AS `client_note`,`tbl_client`.`transaction_type` AS `transaction_type`,`tbl_client`.`date_start` AS `date_start`,`tbl_client`.`date_end` AS `date_end`,`tbl_client`.`transaction_status` AS `transaction_status`,(select concat(convert(`view_tbl_car`.`brand` using utf8mb4),' ',`view_tbl_car`.`model`) from `view_tbl_car` where (`view_tbl_car`.`car_id` = `tbl_client`.`car_id`)) AS `car` from `tbl_client`;

SET FOREIGN_KEY_CHECKS = 1;
