<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CarReviews extends MY_Controller { 
	
	public function __construct()
  	{
		parent::__construct(); 
	    $this->checkLog();
  	}

	public function index()
	{
		$data['userdata'] = $this->session->userdata('user_data');
		$data['cars'] = $this->fetchRawData("SELECT * FROM view_tbl_car ORDER BY brand,model ASC");
		$data['clients'] = $this->fetchRawData("SELECT * FROM tbl_clients ORDER BY client_name ASC");
		$this->load->view('admin/CarReviews.php',$data); 
	}   

	function ajax_table() {
		$aColumns = [
			'client_name',
			'car',
			'review',
			'star',
			'date',
		];
		$order = '';
		$by = '';
		$limit = 0;
		$offset = 0;
		//sort
		if ($this->input->post('iSortCol_0') != null) {
			for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
				if ($this->input->post('bSortable_' . $this->input->post('iSortCol_' . $i)) == true) {
					// $order_by = [ $aColumns[$this->input->post('iSortCol_'.$i)] => $this->input->post('sSortDir_'.$i) ];
					$order = $aColumns[$this->input->post('iSortCol_' . $i)];
					$by = $this->input->post('sSortDir_' . $i); 

				}
			}
		}

		//limit
		if ($this->input->post('iDisplayLength') != null) {
			$limit = $this->input->post('iDisplayLength');
			$offset = 0;
		}

		//paginate
		if ($this->input->post('iDisplayStart') != 0) {
			// $limit = [$this->input->post('iDisplayLength') => $this->input->post('iDisplayStart')];
			$limit = $this->input->post('iDisplayLength');
			$offset = $this->input->post('iDisplayStart');
		}

		//search to get all data
		if ($this->input->post('sSearch') != '') { 
			$search = $this->trim_str($this->input->post('sSearch'));
			
			$this->db->group_start();
			foreach ($aColumns as $key => $value) {
				$this->db->or_like($value, $search);
			} 
			$this->db->group_end();

			 

		} 
 		$this->load->model('Model_Query');
 		$tbl_car_reviews = new Model_Query();
 		// search($where = [], $field = '', $order = '', $limit = 0, $offset = 0, $group_by = '')
		$dataTable = $tbl_car_reviews->getView('view_tbl_car_reviews',[],$order,$by,$limit,$offset);
		$data['data'] = [];
		foreach ($dataTable as $key => $value) {
			$btn_edit = '<button review_id="'.$value['review_id'].'" class="btn_edit btn btn-success btn-xs"><i class="fas fa-edit"></i> Edit</button>';
			$btn_delete = '<button review_id="'.$value['review_id'].'" class="btn_delete btn btn-danger btn-xs"><i class="fas fa-trash"></i> Delete</button>';
			$data['data'][] = [
				$value['client_name'],
				$value['car'],
				$value['review'],
				$value['star'],
				date('Y-m-d h:i:s A',strtotime($value['date'])),
				// $btn_edit.$btn_delete
			];
		}

		$data['iTotalRecords'] = $this->get_total_records($this->input->post());;
		$data['iTotalDisplayRecords'] = count($dataTable);
		echo json_encode($data);
	}

	function get_total_records($post_data) {
		
		$aColumns = [
			'client_name',
			'car',
			'review',
			'star',
			'date',
		];
		$order = '';
		$by = '';
		$limit = 0;
		$offset = 0;
		if ($this->input->post('iSortCol_0') != null) {
			for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
				if ($this->input->post('bSortable_' . $this->input->post('iSortCol_' . $i)) == true) {
					// $order_by = [ $aColumns[$this->input->post('iSortCol_'.$i)] => $this->input->post('sSortDir_'.$i) ];
					$order = $aColumns[$this->input->post('iSortCol_' . $i)];
					$by = $this->input->post('sSortDir_' . $i); 

				}
			}
		}

		//limit
		if ($this->input->post('iDisplayLength') != null) {
			$limit = $this->input->post('iDisplayLength');
			$offset = 0;
		}

		//paginate
		if ($this->input->post('iDisplayStart') != 0) {
			// $limit = [$this->input->post('iDisplayLength') => $this->input->post('iDisplayStart')];
			$limit = $this->input->post('iDisplayLength');
			$offset = $this->input->post('iDisplayStart');
		}

		//search to get all data
		if ($this->input->post('sSearch') != '') { 
			$search = $this->trim_str($this->input->post('sSearch'));
			
			$this->db->group_start();
			foreach ($aColumns as $key => $value) {
				$this->db->or_like($value, $search);
			} 
			$this->db->group_end();
		} 
 		$this->load->model('Model_Query');
 		$tbl_car_reviews = new Model_Query();
 		// search($where = [], $field = '', $order = '', $limit = 0, $offset = 0, $group_by = '')
		$dataTable = $tbl_car_reviews->getView('view_tbl_car_reviews',[],$order,$by,$limit,$offset);

		return count($dataTable);
	}


	function save_detail() {
		$review_id = $this->input->post('review_id');
		$car_id = $this->input->post('car_id');
		$client_id = $this->input->post('client_id');
		$review = $this->input->post('review');
		$star = $this->input->post('star');
		$date = $this->input->post('date');

		$this->load->model('Model_tbl_car_reviews');
		$tbl_car_reviews = new Model_tbl_car_reviews();
		$tbl_car_reviews->load($review_id);
		$tbl_car_reviews->date = $date;
		$tbl_car_reviews->car_id = $car_id;
		$tbl_car_reviews->client_id = $client_id;
		$tbl_car_reviews->review = $review;
		$tbl_car_reviews->star = $star;
		$tbl_car_reviews->save();

		echo $tbl_car_reviews->review_id;
	}
	
	
}
