<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Maintenance extends MY_Controller {

	public function __construct()
  	{
		parent::__construct(); 
	    $this->checkLog();
  	}

	public function index()
	{
		$data['userdata'] = $this->session->userdata('user_data');
		$data['cars'] = $this->get_cars();
		$this->load->view('admin/Maintenance.php',$data); 
	}   
	

	function get_cars() {
		$data = $this->fetchRawData("SELECT * FROM view_tbl_car ORDER BY brand ASC");
		return $data;
	}


}