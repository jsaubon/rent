<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bookings extends MY_Controller { 
	
	public function __construct()
  	{
		parent::__construct(); 
	    $this->checkLog();
	    date_default_timezone_set('Asia/Manila');
  	}

	public function index()
	{
		$data['get_status'] = $this->input->get('status');
		$data['userdata'] = $this->session->userdata('user_data');
		
		$data['transaction_statuses'] = $this->fetchRawData("SELECT * FROM tbl_transaction_statuses");
		$data['users'] = $this->fetchRawData("SELECT * FROM tbl_admin WHERE active=1");
		if ($this->input->get('status') == 'new') {
			$data['cars'] = $this->get_cars();
			$this->load->view('admin/NewBooking.php',$data); 
		} else {
			$data['cars'] = $this->get_cars();
			$this->load->view('admin/Bookings.php',$data); 
		}
		
	}   

	function get_cars() {
		return $this->fetchRawData("SELECT * FROM view_tbl_car  ORDER BY brand ASC");
	}

	function get_available_cars() {
		return $this->fetchRawData("SELECT * FROM view_tbl_car WHERE car_id NOT IN (SELECT car_id FROM tbl_client_booking WHERE transaction_status IN ('Pending','Approved','Ongoing')) ORDER BY brand ASC");
	}

	function ajax_table() {
		$aColumns = [
			'car_id',
			'car_rate',
			'car',
			'client_name',
			'client_email',
			'client_phone',
			'client_note',
			'transaction_type',
			'date_start',
			'date_end',
			'transaction_status'
		];
		$order = 'date_start';
		$by = 'ASC';
		$limit = 0;
		$offset = 0;
		//sort
		if ($this->input->post('iSortCol_0') != null) {
			for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
				if ($this->input->post('bSortable_' . $this->input->post('iSortCol_' . $i)) == true) {
					// $order_by = [ $aColumns[$this->input->post('iSortCol_'.$i)] => $this->input->post('sSortDir_'.$i) ];
					$order = $aColumns[$this->input->post('iSortCol_' . $i)];
					$by = $this->input->post('sSortDir_' . $i); 

				}
			}
		}

		//limit
		if ($this->input->post('iDisplayLength') != null) {
			$limit = $this->input->post('iDisplayLength');
			$offset = 0;
		}

		//paginate
		if ($this->input->post('iDisplayStart') != 0) {
			// $limit = [$this->input->post('iDisplayLength') => $this->input->post('iDisplayStart')];
			$limit = $this->input->post('iDisplayLength');
			$offset = $this->input->post('iDisplayStart');
		}

		//search to get all data
		if ($this->input->post('sSearch') != '') { 
			$search = $this->trim_str($this->input->post('sSearch'));
			
			$this->db->group_start();
			foreach ($aColumns as $key => $value) {
				$this->db->or_like($value, $search);
			} 
			$this->db->group_end();

		} 

		if ($this->input->post('get_status')) {
			$status = $this->input->post('get_status');
			$this->db->where('transaction_status',$status);
		}
 		$this->load->model('Model_Query');
 		$clients = new Model_Query();
 		// search($where = [], $field = '', $order = '', $limit = 0, $offset = 0, $group_by = '')
		$dataTable = $clients->getView('view_tbl_client_booking',[],$order,$by,$limit,$offset);
		$data['data'] = [];
		$select_status_data = $this->fetchRawData("SELECT * FROM tbl_transaction_statuses ORDER BY status_id ASC");
		foreach ($select_status_data as $key => $value) {
			$select_status_options[] = "<option value='".$value['transaction_status']."'>".$value['transaction_status']."</option>";
		}
		$select_status_options = implode("",$select_status_options);
		foreach ($dataTable as $key => $value) {
			$select_status = '<select current_value="'.$value['transaction_status'].'" class="form-control select_status" booking_id="'.$value['booking_id'].'" >'.$select_status_options.'</select>';
			$btn_edit_text = 'Update';
			$t_status = $this->input->post('get_status');
			if ($t_status == 'Ongoing') {
				$btn_edit_text = 'Rebook';
			}
			$btn_edit = '<button booking_id="'.$value['booking_id'].'" class="btn_edit btn btn-success btn-xs"><i class="fas fa-edit"></i> '.$btn_edit_text.'</button>';
			$btn_delete = '<button booking_id="'.$value['booking_id'].'" class="btn_delete btn btn-danger btn-xs"><i class="fas fa-trash"></i> Delete</button>';
			$time = time();
			$starttime = strtotime($value['date_start']);;
			if ($time < $starttime) {
				$starttime_class = 'text-danger';
			} else {
				$starttime_class = 'text-success';
			}
			$endtime = strtotime($value['date_end']);;
			if ($time > $endtime) {
				$endtime_class = 'text-danger';
			} else {
				$endtime_class = 'text-success';
			}
			if ($t_status == 'Approved') {
				$data['data'][] = [
					$select_status,
					$value['car'],
					$this->format_rates($value['car_rate']),
					$value['client_name']."<br>".$value['client_email']."<br>".$value['client_phone'],
					$value['transaction_type'],
					'<span class="'.$starttime_class.'">'.date('m/d/Y h:i a',strtotime($value['date_start'])).'</span>',
					'<span class="'.$endtime_class.'">'.date('m/d/Y h:i a',strtotime($value['date_end'])).'</span>',
					$value['client_note'],
				];
			} else {
				$data['data'][] = [
					$select_status,
					$value['car'],
					$this->format_rates($value['car_rate']),
					$value['client_name']."<br>".$value['client_email']."<br>".$value['client_phone'],
					$value['transaction_type'],
					'<span class="'.$starttime_class.'">'.date('m/d/Y h:i a',strtotime($value['date_start'])).'</span>',
					'<span class="'.$endtime_class.'">'.date('m/d/Y h:i a',strtotime($value['date_end'])).'</span>',
					$value['client_note'],
					$btn_edit
				];
			}
				
		}

		$data['iTotalRecords'] = $this->get_total_records($this->input->post());;
		$data['iTotalDisplayRecords'] = count($dataTable);
		echo json_encode($data);
			
	}

	function get_total_records($post_data) {
		
		$aColumns = [
			'car_id',
			'car_rate',
			'car',
			'client_name',
			'client_email',
			'client_phone',
			'client_note',
			'transaction_type',
			'date_start',
			'date_end',
			'transaction_status'
		];
		$order = '';
		$by = '';
		$limit = 0;
		$offset = 0;
		if ($this->input->post('iSortCol_0') != null) {
			for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
				if ($this->input->post('bSortable_' . $this->input->post('iSortCol_' . $i)) == true) {
					// $order_by = [ $aColumns[$this->input->post('iSortCol_'.$i)] => $this->input->post('sSortDir_'.$i) ];
					$order = $aColumns[$this->input->post('iSortCol_' . $i)];
					$by = $this->input->post('sSortDir_' . $i); 

				}
			}
		}

		//limit
		if ($this->input->post('iDisplayLength') != null) {
			$limit = $this->input->post('iDisplayLength');
			$offset = 0;
		}

		//paginate
		if ($this->input->post('iDisplayStart') != 0) {
			// $limit = [$this->input->post('iDisplayLength') => $this->input->post('iDisplayStart')];
			$limit = $this->input->post('iDisplayLength');
			$offset = $this->input->post('iDisplayStart');
		}

		//search to get all data
		if ($this->input->post('sSearch') != '') { 
			$search = $this->trim_str($this->input->post('sSearch'));
			
			$this->db->group_start();
			foreach ($aColumns as $key => $value) {
				$this->db->or_like($value, $search);
			} 
			$this->db->group_end();
		} 

		
		if ($this->input->post('get_status')) {
			$status = $this->input->post('get_status');
			$this->db->where('transaction_status',$status);
		}
 		$this->load->model('Model_Query');
 		$clients = new Model_Query();
 		// search($where = [], $field = '', $order = '', $limit = 0, $offset = 0, $group_by = '')
		$dataTable = $clients->getView('view_tbl_client_booking',[],$order,$by,$limit,$offset);

		return count($dataTable);
	}


	function save_client() {
		$booking_id = $this->input->post('booking_id');
		$client_id = $this->input->post('client_id');
		$car_id = $this->input->post('car_id');
		$car_rate = $this->input->post('car_rate');
		$client_name = $this->input->post('client_name');
		$client_email = $this->input->post('client_email');
		$client_phone = $this->input->post('client_phone');
		$client_note = $this->input->post('client_note');
		$transaction_type = $this->input->post('transaction_type');
		$date_of_payment = $this->input->post('date_of_payment');
		$invoice_number = $this->input->post('invoice_number');
		$amount_paid = $this->input->post('amount_paid');
		$remitted_by = $this->input->post('remitted_by');
		$received_by = $this->input->post('received_by'); 

		$date_start = $this->input->post('date_start');
		$time_start = $this->input->post('time_start');
		$date_start = date('Y-m-d H:i:s',strtotime($date_start. ' '.str_replace(' ','',$time_start)));
		$date_end = $this->input->post('date_end');
		$time_end = $this->input->post('time_end');
		$date_end = date('Y-m-d H:i:s',strtotime($date_end. ' '.str_replace(' ','',$time_end)));
		$transaction_status  = $this->input->post('transaction_status');
		// if ($password != $current_password) {
		// 	$password = password_hash($password, PASSWORD_BCRYPT);	
		// }

		$booking_exist = $this->fetchRawData("SELECT * FROM tbl_client_booking WHERE (DATE(date_start) BETWEEN DATE('$date_start') AND DATE('$date_end') OR DATE(date_end) BETWEEN DATE('$date_start') AND DATE('$date_end')) AND car_id=$car_id");
		
		if (count($booking_exist) > 0) {
			echo "booking exist";
		} else {
			$this->load->model('Model_tbl_clients');
			$clients = new Model_tbl_clients();
			if ($client_id || $client_id != 0) {
				$clients->load($client_id);
			} else {
				$clients->date_registered = date('Y-m-d H:i:s');
				$exist = $this->fetchRawData("SELECT * FROM tbl_clients WHERE client_email='$client_email'");
				if (count($exist) > 0) {
					$clients->load($exist[0]['client_id']);
					echo 'exist';;
				}
			}
			$clients->client_name = $client_name;
			$clients->client_email = $client_email;
			$clients->client_phone = $client_phone;
			$clients->client_password = $client_phone;
			$clients->save();

			$client_id = $clients->client_id;



			$this->load->model('Model_tbl_client_booking');
			$client_booking = new Model_tbl_client_booking();
			if ($booking_id) {
				$client_booking->load($booking_id);
			}
			$client_booking->client_id = $client_id;
			$client_booking->car_id = $car_id;
			$client_booking->car_rate = $car_rate;
			$client_booking->client_note = $client_note;
			$client_booking->transaction_type = $transaction_type;
			$client_booking->date_start = $date_start;
			$client_booking->date_end = $date_end;
			$client_booking->transaction_status = $transaction_status;
			$client_booking->date_of_payment = $date_of_payment;
			$client_booking->invoice_number = $invoice_number;
			$client_booking->amount_paid = $amount_paid;
			$client_booking->remitted_by = $remitted_by;
			$client_booking->received_by = $received_by;
			$client_booking->save();
		}



	}

	function get_client_booking() {
		$booking_id = $this->input->post('booking_id');
		$data = $this->fetchRawData("SELECT * FROM view_tbl_client_booking WHERE booking_id=$booking_id");
		echo json_encode($data);
	}

	function get_alerts_center() {
		
		$data = $this->fetchRawData("SELECT * FROM view_tbl_client_booking WHERE transaction_status='Pending' ORDER BY date_start ASC");

		echo json_encode($data);;
	}
	
}