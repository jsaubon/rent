<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cars extends MY_Controller { 
	
	public function __construct()
  	{
		parent::__construct(); 
	    $this->checkLog();
  	}

	public function index()
	{
		$data['car_active'] = $this->input->get('active');
		$data['userdata'] = $this->session->userdata('user_data');
		$data['brands'] = $this->get_brands();
		$data['models'] = $this->get_models();
		$this->load->view('admin/cars/Cars.php',$data); 
	}   



	function get_brands() {
		$data = $this->fetchRawData("SELECT * FROM tbl_brand ORDER BY brand ASC");
		return $data;
	}
	function get_models() {
		$data = $this->fetchRawData("SELECT * FROM tbl_car_model ORDER BY model ASC");
		return $data;
	}

	function ajax_table() {
		$aColumns = [
			'active',
			'image',
			'brand',
			'model',
			'imei',
			'capacity',
			'transmission',
			'description',
			'rate',
			'car_id',
		];
		$order = '';
		$by = '';
		$limit = 0;
		$offset = 0;
		//sort
		if ($this->input->post('iSortCol_0') != null) {
			for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
				if ($this->input->post('bSortable_' . $this->input->post('iSortCol_' . $i)) == true) {
					// $order_by = [ $aColumns[$this->input->post('iSortCol_'.$i)] => $this->input->post('sSortDir_'.$i) ];
					$order = $aColumns[$this->input->post('iSortCol_' . $i)];
					$by = $this->input->post('sSortDir_' . $i); 

				}
			}
		}

		//limit
		if ($this->input->post('iDisplayLength') != null) {
			$limit = $this->input->post('iDisplayLength');
			$offset = 0;
		}

		//paginate
		if ($this->input->post('iDisplayStart') != 0) {
			// $limit = [$this->input->post('iDisplayLength') => $this->input->post('iDisplayStart')];
			$limit = $this->input->post('iDisplayLength');
			$offset = $this->input->post('iDisplayStart');
		}

		//search to get all data
		if ($this->input->post('sSearch') != '') { 
			$search = $this->trim_str($this->input->post('sSearch'));
			
			$this->db->group_start();
			foreach ($aColumns as $key => $value) {
				$this->db->or_like($value, $search);
			} 
			$this->db->group_end();
		} 
 		$this->load->model('Model_Query');
 		$tbl_car = new Model_Query();
 		// search($where = [], $field = '', $order = '', $limit = 0, $offset = 0, $group_by = '')
 		if ($this->input->post('active') != '') {
 			$this->db->where('active',$this->input->post('active'));
 		}
		$dataTable = $tbl_car->getView('view_tbl_car',[],$order,$by,$limit,$offset);
		$data['data'] = [];
		foreach ($dataTable as $key => $value) {
			$rates = $this->format_rates($value['rate']);
			
			
			$btn_edit = '<button car_id="'.$value['car_id'].'" class="btn_edit btn  btn-success btn-sm"><i class="fas fa-edit"></i> </button>';
			$btn_delete = '<button car_id="'.$value['car_id'].'" class="btn_delete btn  btn-danger btn-sm"><i class="fas fa-archive"></i> </button>';
			$btn_logs = '<button car_id="'.$value['car_id'].'" class="btn_logs btn  btn-info btn-sm"><i class="fas fa-cog"></i> </button>';

			$buttons = '';
			if ($this->input->post('active')) {
				$buttons = $btn_edit.$btn_delete.$btn_logs;
			} else {
				$buttons = $btn_edit.$btn_logs;
			}
			$data['data'][] = [
				$value['active'] == 1 ? 'Active' : 'Inactive',
				'<img width="100%;;" class="img-thumbnail" src="'.base_url('assets/uploads/'.$value['image']).'"/>',
				$value['brand'],
				$value['model'],
				$value['imei'],
				$value['capacity'],
				$value['transmission'],
				'<div style="font-size: 14px;">'.$value['description'].'</div>',
				'<div style="font-size: 14px;">'.$rates.'</div>',
				'<div class="btn-group">'.$buttons.'</div>'
			];
		}

		$data['iTotalRecords'] = $this->get_total_records($this->input->post());;
		$data['iTotalDisplayRecords'] = count($dataTable);
		echo json_encode($data);
	}



	function get_total_records($post_data) {
		
		$aColumns = [
			'active',
			'image',
			'brand',
			'model',
			'imei',
			'capacity',
			'transmission',
			'description',
			'rate',
			'car_id',
		];
		$order = '';
		$by = '';
		$limit = 0;
		$offset = 0;
		if ($this->input->post('iSortCol_0') != null) {
			for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
				if ($this->input->post('bSortable_' . $this->input->post('iSortCol_' . $i)) == true) {
					// $order_by = [ $aColumns[$this->input->post('iSortCol_'.$i)] => $this->input->post('sSortDir_'.$i) ];
					$order = $aColumns[$this->input->post('iSortCol_' . $i)];
					$by = $this->input->post('sSortDir_' . $i); 

				}
			}
		}

		//limit
		if ($this->input->post('iDisplayLength') != null) {
			$limit = $this->input->post('iDisplayLength');
			$offset = 0;
		}

		//paginate
		if ($this->input->post('iDisplayStart') != 0) {
			// $limit = [$this->input->post('iDisplayLength') => $this->input->post('iDisplayStart')];
			$limit = $this->input->post('iDisplayLength');
			$offset = $this->input->post('iDisplayStart');
		}

		//search to get all data
		if ($this->input->post('sSearch') != '') { 
			$search = $this->trim_str($this->input->post('sSearch'));
			
			$this->db->group_start();
			foreach ($aColumns as $key => $value) {
				$this->db->or_like($value, $search);
			} 
			$this->db->group_end();
		} 
 		$this->load->model('Model_Query');
 		$tbl_car = new Model_Query();
 		// search($where = [], $field = '', $order = '', $limit = 0, $offset = 0, $group_by = '')
		$dataTable = $tbl_car->getView('view_tbl_car',[],$order,$by,$limit,$offset);

		return count($dataTable);
	}


	function save_detail() {
		$car_id = $this->input->post('car_car_id');
		$new_image = $_FILES['car_new_image']; 
		$image = $this->input->post('car_image'); 
		$brand_id = $this->input->post('car_brand_id'); 
		$model_id = $this->input->post('car_model_id'); 
		$imei = $this->input->post('car_imei'); 
		$capacity = $this->input->post('car_capacity'); 
		$transmission = $this->input->post('car_transmission'); 
		$description = $this->input->post('car_description'); 
		$active = $this->input->post('car_active'); 
		// $this->pprint($_FILES['car_new_image']);
		if (($_FILES['car_new_image']['name']) != '') {
			$config['upload_path']          = 'assets/uploads/';
			$config['allowed_types']        = 'gif|jpg|png';
			$config['max_size']             = 10000;
			$config['max_width']            = 10024;
			$config['max_height']           = 10024;

 			$ext = explode('.',$_FILES['car_new_image']['name']);
 			$ext = $ext[1];
		    $image = time().'.'.$ext;

			$config['file_name'] = $image;

			$this->load->library('upload', $config);


	  
			 
   
			if ( $this->upload->do_upload('car_new_image'))
		  	{        
			   $info = $this->upload->data();
			   
		  	} else {
		  		$error = array('error' => $this->upload->display_errors());
		  		$this->pprint($error);
			}
		}
		// echo $image;


		$this->load->model('Model_tbl_car');
		$tbl_car = new Model_tbl_car();
		if ($car_id != '') {
			$tbl_car->load($car_id);
		}
		$tbl_car->image = $image;
		$tbl_car->brand_id = $brand_id;
		$tbl_car->model_id = $model_id;
		$tbl_car->imei = $imei;
		$tbl_car->capacity = $capacity;
		$tbl_car->transmission = $transmission;
		$tbl_car->description = $description;
		$tbl_car->active = $active;
		$tbl_car->save();


		$rate_ids = $this->input->post('rate_rate_id');
		$coverages = $this->input->post('rate_coverage');
		$rates = $this->input->post('rate_rate');

		$this->load->model('Model_tbl_car_rate');
		for ($i=0; $i < count($coverages); $i++) { 
			$tbl_car_rate = new Model_tbl_car_rate();
			if (isset($rate_ids[$i])) {
				$tbl_car_rate->load($rate_ids[$i]);
			}
			$tbl_car_rate->car_id = $tbl_car->car_id;
			$tbl_car_rate->coverage = $coverages[$i];
			$tbl_car_rate->rate = $rates[$i];
			$tbl_car_rate->save();
		}


		redirect(base_url('admin/cars/cars?active='.$active),'refresh');
		  	


		



	}
	
	
}
