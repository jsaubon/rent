<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brands extends MY_Controller { 
	
	public function __construct()
  	{
		parent::__construct(); 
	    $this->checkLog();
  	}

	public function index()
	{
		$data['userdata'] = $this->session->userdata('user_data');
		$this->load->view('admin/cars/Brands.php',$data); 
	}   

	function ajax_table() {
		$aColumns = [
			'brand',
			'brand_id',
		];
		$order = '';
		$by = '';
		$limit = 0;
		$offset = 0;
		//sort
		if ($this->input->post('iSortCol_0') != null) {
			for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
				if ($this->input->post('bSortable_' . $this->input->post('iSortCol_' . $i)) == true) {
					// $order_by = [ $aColumns[$this->input->post('iSortCol_'.$i)] => $this->input->post('sSortDir_'.$i) ];
					$order = $aColumns[$this->input->post('iSortCol_' . $i)];
					$by = $this->input->post('sSortDir_' . $i); 

				}
			}
		}

		//limit
		if ($this->input->post('iDisplayLength') != null) {
			$limit = $this->input->post('iDisplayLength');
			$offset = 0;
		}

		//paginate
		if ($this->input->post('iDisplayStart') != 0) {
			// $limit = [$this->input->post('iDisplayLength') => $this->input->post('iDisplayStart')];
			$limit = $this->input->post('iDisplayLength');
			$offset = $this->input->post('iDisplayStart');
		}

		//search to get all data
		if ($this->input->post('sSearch') != '') { 
			$search = $this->trim_str($this->input->post('sSearch'));
			
			$this->db->group_start();
			foreach ($aColumns as $key => $value) {
				$this->db->or_like($value, $search);
			} 
			$this->db->group_end();

			 

		} 
 		$this->load->model('Model_tbl_brand');
 		$tbl_brand = new Model_tbl_brand();
 		// search($where = [], $field = '', $order = '', $limit = 0, $offset = 0, $group_by = '')
		$dataTable = $tbl_brand->search([],$order,$by,$limit,$offset);
		$data['data'] = [];
		foreach ($dataTable as $key => $value) {
			$btn_edit = '<button brand_id="'.$value['brand_id'].'" class="btn_edit btn btn-success btn-xs"><i class="fas fa-edit"></i> Update</button>';
			$btn_delete = '<button brand_id="'.$value['brand_id'].'" class="btn_delete btn btn-danger btn-xs"><i class="fas fa-trash"></i> Delete</button>';
			$data['data'][] = [
				$value['brand'],
				$btn_edit
			];
		}

		$data['iTotalRecords'] = $this->get_total_records($this->input->post());;
		$data['iTotalDisplayRecords'] = count($dataTable);
		echo json_encode($data);
	}

	function get_total_records($post_data) {
		
		$aColumns = [
			'brand',
			'brand_id',
		];
		$order = '';
		$by = '';
		$limit = 0;
		$offset = 0;
		if ($this->input->post('iSortCol_0') != null) {
			for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
				if ($this->input->post('bSortable_' . $this->input->post('iSortCol_' . $i)) == true) {
					// $order_by = [ $aColumns[$this->input->post('iSortCol_'.$i)] => $this->input->post('sSortDir_'.$i) ];
					$order = $aColumns[$this->input->post('iSortCol_' . $i)];
					$by = $this->input->post('sSortDir_' . $i); 

				}
			}
		}

		//limit
		if ($this->input->post('iDisplayLength') != null) {
			$limit = $this->input->post('iDisplayLength');
			$offset = 0;
		}

		//paginate
		if ($this->input->post('iDisplayStart') != 0) {
			// $limit = [$this->input->post('iDisplayLength') => $this->input->post('iDisplayStart')];
			$limit = $this->input->post('iDisplayLength');
			$offset = $this->input->post('iDisplayStart');
		}

		//search to get all data
		if ($this->input->post('sSearch') != '') { 
			$search = $this->trim_str($this->input->post('sSearch'));
			
			$this->db->group_start();
			foreach ($aColumns as $key => $value) {
				$this->db->or_like($value, $search);
			} 
			$this->db->group_end();
		} 
 		$this->load->model('Model_tbl_brand');
 		$tbl_brand = new Model_tbl_brand();
 		// search($where = [], $field = '', $order = '', $limit = 0, $offset = 0, $group_by = '')
		$dataTable = $tbl_brand->search([],$order,$by,0,0);

		return count($dataTable);
	}


	function save_detail() {
		$brand_id = $this->input->post('brand_id');
		$brand = $this->input->post('brand'); 
		// if ($password != $current_password) {
		// 	$password = password_hash($password, PASSWORD_BCRYPT);	
		// }


		$this->load->model('Model_tbl_brand');
		$tbl_brand = new Model_tbl_brand();
		$tbl_brand->load($brand_id);
		$tbl_brand->brand = $brand;
		$tbl_brand->save();

		echo $tbl_brand->brand_id;


		



	}
	
	
}
