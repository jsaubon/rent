<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Protrack extends MY_Controller {

	public function index()
	{
        $data['userdata'] = $this->session->userdata('user_data');
		$this->load->view('admin/ProTrack',$data);
	}

	function get_access_token() {
        date_default_timezone_set("Asia/Manila");
        echo date('Y-m-d h:i:s a');
		$time = time();
		$account = 'bws';
		$signature = md5(md5(61902) . $time);
		$url = "http://api.protrack365.com/api/authorization?time=$time&account=$account&signature=$signature";

		echo "<br>".$url;


		// create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                            'Content-Type: application/json; charset=UTF-8',
                                            ));
        curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);
        $this->pprint($output);


        // close curl resource to free up system resources
        curl_close($ch);     
	}

}

/* End of file Protrack.php */
/* Location: ./application/controllers/admin/Protrack.php */