<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends MY_Controller {

	public function index() {
		$this->checklog();
		$this->load->view('admin/Login.php');
	}
	function loginUser() {
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		// $data = $this->fetchRawData("SELECT * FROM admins WHERE username='$username'");
		$this->load->model('Model_tbl_admin');
		$admins = new Model_tbl_admin();
		$data = $admins->search(['username' => $username], '', '', 1);
		$data = reset($data);
		// $this->pprint($data);

		// $password_hashed = $data['password'];
		// if (password_verify($password, $password_hashed)) {

			if (!empty($data)) {
				if ($data['active']) {
					echo "Valid";
					$data_session = array(
						'name' => $data['name'],
						'email_address' => $data['email_address'],
						'admin_id' => $data['admin_id'],
						'login_type' => $data['user_type_id'],
						'login_folder' => 'admins'
					);
					$this->session->set_userdata(array('user_data' => $data_session));
					$this->session->show_notif = 1;

					$this->session->login_error = 0;
					redirect(base_url('admin/dashboard'));
				} else {
					$this->session->login_error = 3;
					redirect(base_url('admin/login'));
				}
					
			} else {
				$this->session->login_error = 1;
				redirect(base_url('admin/login'));
			}
				
		// } else {
		// 	$this->session->login_error = 1;
		// 	redirect(base_url('admin/login'));
		// }
	}




	function logout() {
		session_destroy();

		redirect(base_url('admin/login'));
	}

}