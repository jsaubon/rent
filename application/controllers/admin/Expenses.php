<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expenses extends MY_Controller { 
	
	public function __construct()
  	{
		parent::__construct(); 
	    $this->checkLog();
  	}

	public function index()
	{
		$data['userdata'] = $this->session->userdata('user_data');
		$this->load->view('admin/Expenses.php',$data); 
	}   

	function ajax_table() {
		$aColumns = [
			'date',
			'description',
			'particulars',
			'amount',
			'requested_by',
			'remarks',
			'expense_id'
		];
		$order = '';
		$by = '';
		$limit = 0;
		$offset = 0;
		//sort
		if ($this->input->post('iSortCol_0') != null) {
			for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
				if ($this->input->post('bSortable_' . $this->input->post('iSortCol_' . $i)) == true) {
					// $order_by = [ $aColumns[$this->input->post('iSortCol_'.$i)] => $this->input->post('sSortDir_'.$i) ];
					$order = $aColumns[$this->input->post('iSortCol_' . $i)];
					$by = $this->input->post('sSortDir_' . $i); 

				}
			}
		}

		//limit
		if ($this->input->post('iDisplayLength') != null) {
			$limit = $this->input->post('iDisplayLength');
			$offset = 0;
		}

		//paginate
		if ($this->input->post('iDisplayStart') != 0) {
			// $limit = [$this->input->post('iDisplayLength') => $this->input->post('iDisplayStart')];
			$limit = $this->input->post('iDisplayLength');
			$offset = $this->input->post('iDisplayStart');
		}

		//search to get all data
		if ($this->input->post('sSearch') != '') { 
			$search = $this->trim_str($this->input->post('sSearch'));
			
			$this->db->group_start();
			foreach ($aColumns as $key => $value) {
				$this->db->or_like($value, $search);
			} 
			$this->db->group_end();

			 

		} 
 		$this->load->model('Model_tbl_expenses');
 		$tbl_expenses = new Model_tbl_expenses();
 		// search($where = [], $field = '', $order = '', $limit = 0, $offset = 0, $group_by = '')
		$dataTable = $tbl_expenses->search([],$order,$by,$limit,$offset);
		$data['data'] = [];
		foreach ($dataTable as $key => $value) {
			$btn_edit = '<button expense_id="'.$value['expense_id'].'" class="btn_edit btn btn-success btn-xs"><i class="fas fa-edit"></i> Update</button>';
			$btn_delete = '<button expense_id="'.$value['expense_id'].'" class="btn_delete btn btn-danger btn-xs"><i class="fas fa-trash"></i> Delete</button>';
			$data['data'][] = [
				date('m/d/Y',strtotime($value['date'])),
				$value['description'],
				$value['particulars'],
				$value['amount'],
				$value['requested_by'],
				$value['remarks'],
				$btn_edit
			];
		}

		$data['iTotalRecords'] = $this->get_total_records($this->input->post());;
		$data['iTotalDisplayRecords'] = count($dataTable);
		echo json_encode($data);
	}

	function get_total_records($post_data) {
		
		$aColumns = [
			'date',
			'description',
			'particulars',
			'amount',
			'requested_by',
			'remarks',
			'expense_id'
		];
		$order = '';
		$by = '';
		$limit = 0;
		$offset = 0;
		if ($this->input->post('iSortCol_0') != null) {
			for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
				if ($this->input->post('bSortable_' . $this->input->post('iSortCol_' . $i)) == true) {
					// $order_by = [ $aColumns[$this->input->post('iSortCol_'.$i)] => $this->input->post('sSortDir_'.$i) ];
					$order = $aColumns[$this->input->post('iSortCol_' . $i)];
					$by = $this->input->post('sSortDir_' . $i); 

				}
			}
		}

		//limit
		if ($this->input->post('iDisplayLength') != null) {
			$limit = $this->input->post('iDisplayLength');
			$offset = 0;
		}

		//paginate
		if ($this->input->post('iDisplayStart') != 0) {
			// $limit = [$this->input->post('iDisplayLength') => $this->input->post('iDisplayStart')];
			$limit = $this->input->post('iDisplayLength');
			$offset = $this->input->post('iDisplayStart');
		}

		//search to get all data
		if ($this->input->post('sSearch') != '') { 
			$search = $this->trim_str($this->input->post('sSearch'));
			
			$this->db->group_start();
			foreach ($aColumns as $key => $value) {
				$this->db->or_like($value, $search);
			} 
			$this->db->group_end();
		} 
 		$this->load->model('Model_tbl_expenses');
 		$tbl_expenses = new Model_tbl_expenses();
 		// search($where = [], $field = '', $order = '', $limit = 0, $offset = 0, $group_by = '')
		$dataTable = $tbl_expenses->search([],$order,$by,0,0);

		return count($dataTable);
	}


	function save_detail() {
		$this->pprint($this->input->post());
		$expense_id = $this->input->post('expense_id');
		$date = $this->input->post('date');
		$description = $this->input->post('description');
		$particulars = $this->input->post('particulars');
		$amount = $this->input->post('amount');
		$requested_by = $this->input->post('requested_by');
		$remarks = $this->input->post('remarks');

		$this->load->model('Model_tbl_expenses');
		$tbl_expenses = new Model_tbl_expenses();
		$tbl_expenses->load($expense_id);
		$tbl_expenses->date = $date;
		$tbl_expenses->description = $description;
		$tbl_expenses->particulars = $particulars;
		$tbl_expenses->amount = $amount;
		$tbl_expenses->requested_by = $requested_by;
		$tbl_expenses->remarks = $remarks;
		$tbl_expenses->save();

		echo $tbl_expenses->expense_id;
	}
	
	
}
