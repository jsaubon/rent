<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends MY_Controller {

	public function __construct()
  	{
		parent::__construct(); 
	    $this->checkLog();
  	}

	public function index()
	{
		$data['userdata'] = $this->session->userdata('user_data');
		$data['rented_cars'] = $this->get_rented_cars();
		$this->load->view('admin/Dashboard.php',$data); 
	}   
	

	function get_available_cars() {
		$data = $this->fetchRawData("SELECT * FROM view_tbl_car WHERE car_id NOT IN (SELECT car_id FROM tbl_client_booking WHERE car_id=view_tbl_car.car_id AND transaction_status IN ('Pending','Ongoing','Satisfied'))");
		echo json_encode($data);;
	}
	function get_rented_cars() {
		$data = $this->fetchRawData("SELECT * FROM view_tbl_client_booking WHERE  transaction_status IN ('Pending','Ongoing','Approved')");
		return $data;
	}

	function get_cards() {
		$month = $this->input->post('month');
		$year = $this->input->post('year');
		$data = $this->fetchRawData("SELECT transaction_status,COUNT(*) as `count` FROM tbl_client_booking WHERE MONTH(date_start)=$month AND YEAR(date_start)=$year GROUP BY transaction_status");
		echo json_encode($data);
	}

	function get_chart() {
		$month = $this->input->post('month');
		$year = $this->input->post('year');
		$transaction_status = $this->input->post('transaction_status');
		$data = $this->fetchRawData("SELECT brand,car,COUNT(*) as `count` FROM view_tbl_client_booking WHERE MONTH(date_Start)=$month AND YEAR(date_start)=$year AND transaction_status='$transaction_status' GROUP BY car");
		echo json_encode($data);
	}
}