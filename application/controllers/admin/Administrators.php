<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrators extends MY_Controller { 
	
	public function __construct()
  	{
		parent::__construct(); 
	    $this->checkLog();
  	}

	public function index()
	{
		$data['userdata'] = $this->session->userdata('user_data');
		$data['user_types'] = $this->fetchRawData("SELECT * FROM tbl_user_types");
		$this->load->view('admin/Administrators.php',$data); 
	}   

	function ajax_table() {
		$aColumns = [
			'user_type',
			'name',
			'email_address',
			'username',
			'active',
			'admin_id',
		];
		$order = '';
		$by = '';
		$limit = 0;
		$offset = 0;
		//sort
		if ($this->input->post('iSortCol_0') != null) {
			for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
				if ($this->input->post('bSortable_' . $this->input->post('iSortCol_' . $i)) == true) {
					// $order_by = [ $aColumns[$this->input->post('iSortCol_'.$i)] => $this->input->post('sSortDir_'.$i) ];
					$order = $aColumns[$this->input->post('iSortCol_' . $i)];
					$by = $this->input->post('sSortDir_' . $i); 

				}
			}
		}

		//limit
		if ($this->input->post('iDisplayLength') != null) {
			$limit = $this->input->post('iDisplayLength');
			$offset = 0;
		}

		//paginate
		if ($this->input->post('iDisplayStart') != 0) {
			// $limit = [$this->input->post('iDisplayLength') => $this->input->post('iDisplayStart')];
			$limit = $this->input->post('iDisplayLength');
			$offset = $this->input->post('iDisplayStart');
		}

		//search to get all data
		if ($this->input->post('sSearch') != '') { 
			$search = $this->trim_str($this->input->post('sSearch'));
			
			$this->db->group_start();
			foreach ($aColumns as $key => $value) {
				$this->db->or_like($value, $search);
			} 
			$this->db->group_end();

			 

		} 
 		$this->load->model('Model_Query');
 		$admins = new Model_Query();
 		// search($where = [], $field = '', $order = '', $limit = 0, $offset = 0, $group_by = '')
		$dataTable = $admins->getView('view_tbl_admin',[],$order,$by,$limit,$offset);
		$data['data'] = [];
		foreach ($dataTable as $key => $value) {
			$btn_edit = '<button admin_id="'.$value['admin_id'].'" class="btn_edit btn btn-info btn-xs"><i class="fas fa-edit"></i> Edit</button>';
			$btn_delete = '<button admin_id="'.$value['admin_id'].'" class="btn_delete btn btn-danger btn-xs"><i class="fas fa-trash"></i> Delete</button>';

			$data['data'][] = [
				$value['active'] == 1 ? 'Active' : 'Inactive',
				$value['user_type'],
				$value['name'],
				$value['email_address'],
				$value['username'],
				$btn_edit.$btn_delete
			];
		}

		$data['iTotalRecords'] = $this->get_total_records($this->input->post());;
		$data['iTotalDisplayRecords'] = count($dataTable);
		echo json_encode($data);
	}

	function get_total_records($post_data) {
		
		$aColumns = [
			'user_type',
			'name',
			'email_address',
			'username',
			'active',
			'admin_id',
		];
		$order = '';
		$by = '';
		$limit = 0;
		$offset = 0;
		if ($this->input->post('iSortCol_0') != null) {
			for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
				if ($this->input->post('bSortable_' . $this->input->post('iSortCol_' . $i)) == true) {
					// $order_by = [ $aColumns[$this->input->post('iSortCol_'.$i)] => $this->input->post('sSortDir_'.$i) ];
					$order = $aColumns[$this->input->post('iSortCol_' . $i)];
					$by = $this->input->post('sSortDir_' . $i); 

				}
			}
		}

		//limit
		if ($this->input->post('iDisplayLength') != null) {
			$limit = $this->input->post('iDisplayLength');
			$offset = 0;
		}

		//paginate
		if ($this->input->post('iDisplayStart') != 0) {
			// $limit = [$this->input->post('iDisplayLength') => $this->input->post('iDisplayStart')];
			$limit = $this->input->post('iDisplayLength');
			$offset = $this->input->post('iDisplayStart');
		}

		//search to get all data
		if ($this->input->post('sSearch') != '') { 
			$search = $this->trim_str($this->input->post('sSearch'));
			
			$this->db->group_start();
			foreach ($aColumns as $key => $value) {
				$this->db->or_like($value, $search);
			} 
			$this->db->group_end();
		} 
 		$this->load->model('Model_tbl_admin');
 		$admins = new Model_tbl_admin();
 		// search($where = [], $field = '', $order = '', $limit = 0, $offset = 0, $group_by = '')
		$dataTable = $admins->search([],$order,$by,0,0);

		return count($dataTable);
	}


	function save_admin() {
		$admin_id = $this->input->post('admin_id');
		$user_type_id = $this->input->post('user_type_id'); 
		$name = $this->input->post('name'); 
		$email_address = $this->input->post('email_address'); 
		$username = $this->input->post('username');
		$password = $this->input->post('password'); 
		$active = $this->input->post('active'); 
		$current_password = $this->input->post('current_password'); 
		// if ($password != $current_password) {
		// 	$password = password_hash($password, PASSWORD_BCRYPT);	
		// }


		$this->load->model('Model_tbl_admin');
		$admins = new Model_tbl_admin();
		if ($admin_id) {
			$admins->load($admin_id);
		}
		$admins->user_type_id = $user_type_id;
		$admins->name = $name;
		$admins->email_address = $email_address;
		$admins->username = $username;
		$admins->password = $password;
		$admins->active = $active;
		$admins->save();

		echo $admins->admin_id;


		



	}
	
	
}
