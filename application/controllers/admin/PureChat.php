<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PureChat extends MY_Controller {

	public function index()
	{
        $data['userdata'] = $this->session->userdata('user_data');
		$this->load->view('admin/PureChat',$data);
	}



}

/* End of file PureChat.php */
/* Location: ./application/controllers/admin/PureChat.php */