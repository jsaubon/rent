<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Expense extends MY_Controller {

	public function __construct()
  	{
		parent::__construct(); 
	    $this->checkLog();
  	}

	public function index()
	{
		$data['userdata'] = $this->session->userdata('user_data');
		$this->load->view('admin/reports/Expense.php',$data); 
	}   

	function get_report_filter() {
		$date_start = $this->input->post('date_start');
		$date_end = $this->input->post('date_end');


		$data = $this->fetchRawData("SELECT * FROM tbl_expenses WHERE date > '$date_start' AND date < '$date_end' ORDER BY date DESC");
		// echo "SELECT * FROM view_tbl_client_booking WHERE date_start > '$date_start' AND date_end < '$date_end' $transaction_status $transaction_type $car_brand ORDER BY date_start DESC";
		echo json_encode($data);
	}
	
}