<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Account extends MY_Controller {

	public function __construct()
  	{
		parent::__construct(); 
	    $this->checkLog();
  	}

	public function index()
	{
		$data['userdata'] = $this->session->userdata('user_data');
		$data['transaction_statuses'] = $this->fetchRawData("SELECT * FROM tbl_transaction_statuses");
		$data['transaction_types'] = $this->fetchRawData("SELECT * FROM tbl_transaction_types");
		$data['car_brands'] = $this->fetchRawData("SELECT * FROM tbl_brand ORDER BY brand ASC");
		$this->load->view('admin/reports/Account.php',$data); 
	}   

	function get_report_filter() {
		$transaction_type = $this->input->post('transaction_type');
		$transaction_status = $this->input->post('transaction_status');
		$car_brand = $this->input->post('car_brand');
		$car_model = $this->input->post('car_model');
		$date_start = $this->input->post('date_start');
		$date_end = $this->input->post('date_end');


		// $this->pprint($this->input->post());
		$transaction_status = $transaction_status != 'All' ? " AND transaction_status='".$transaction_status."'" : '';
		$transaction_type = $transaction_type != 'All' ? " AND transaction_type='".$transaction_type."'" : '';
		$car_brand = $car_brand != 'All' && $car_model == 'All' ? " AND car LIKE '%".$car_brand."%' " : '';
		$car_brand = $car_brand != 'All' && $car_model != 'All' ? " AND car= '".$car_brand.' '.$car_model."'" : $car_brand;
		$data = $this->fetchRawData("SELECT * FROM view_tbl_client_booking WHERE date_of_payment > '$date_start' AND date_of_payment < '$date_end' $transaction_status $car_brand ORDER BY date_start DESC");
		// echo "SELECT * FROM view_tbl_client_booking WHERE date_start > '$date_start' AND date_end < '$date_end' $transaction_status $transaction_type $car_brand ORDER BY date_start DESC";
		echo json_encode($data);
	}
	
}