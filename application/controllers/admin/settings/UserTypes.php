<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserTypes extends MY_Controller { 
	
	public function __construct()
  	{
		parent::__construct(); 
	    $this->checkLog();
  	}

	public function index()
	{
		$data['userdata'] = $this->session->userdata('user_data');
		$this->load->view('admin/settings/UserTypes.php',$data); 
	}   

	function ajax_table() {
		$aColumns = [
			'user_type',
			'pages',
			'user_type_id',
		];
		$order = '';
		$by = '';
		$limit = 0;
		$offset = 0;
		//sort
		if ($this->input->post('iSortCol_0') != null) {
			for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
				if ($this->input->post('bSortable_' . $this->input->post('iSortCol_' . $i)) == true) {
					// $order_by = [ $aColumns[$this->input->post('iSortCol_'.$i)] => $this->input->post('sSortDir_'.$i) ];
					$order = $aColumns[$this->input->post('iSortCol_' . $i)];
					$by = $this->input->post('sSortDir_' . $i); 

				}
			}
		}

		//limit
		if ($this->input->post('iDisplayLength') != null) {
			$limit = $this->input->post('iDisplayLength');
			$offset = 0;
		}

		//paginate
		if ($this->input->post('iDisplayStart') != 0) {
			// $limit = [$this->input->post('iDisplayLength') => $this->input->post('iDisplayStart')];
			$limit = $this->input->post('iDisplayLength');
			$offset = $this->input->post('iDisplayStart');
		}

		//search to get all data
		if ($this->input->post('sSearch') != '') { 
			$search = $this->trim_str($this->input->post('sSearch'));
			
			$this->db->group_start();
			foreach ($aColumns as $key => $value) {
				$this->db->or_like($value, $search);
			} 
			$this->db->group_end();

			 

		} 
 		$this->load->model('Model_tbl_user_types');
 		$tbl_user_types = new Model_tbl_user_types();
 		// search($where = [], $field = '', $order = '', $limit = 0, $offset = 0, $group_by = '')
		$dataTable = $tbl_user_types->search([],$order,$by,$limit,$offset);
		$data['data'] = [];
		foreach ($dataTable as $key => $value) {
			$btn_edit = '<button user_type_id="'.$value['user_type_id'].'" class="btn_edit btn btn-success btn-xs"><i class="fas fa-edit"></i> Update</button>';
			$btn_delete = '<button user_type_id="'.$value['user_type_id'].'" class="btn_delete btn btn-danger btn-xs"><i class="fas fa-trash"></i> Delete</button>';
			$data['data'][] = [
				$value['user_type'],
				$value['pages'],
				$btn_edit
			];
		}

		$data['iTotalRecords'] = $this->get_total_records($this->input->post());;
		$data['iTotalDisplayRecords'] = count($dataTable);
		echo json_encode($data);
	}

	function get_total_records($post_data) {
		
		$aColumns = [
			'user_type',
			'pages',
			'user_type_id',
		];
		$order = '';
		$by = '';
		$limit = 0;
		$offset = 0;
		if ($this->input->post('iSortCol_0') != null) {
			for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
				if ($this->input->post('bSortable_' . $this->input->post('iSortCol_' . $i)) == true) {
					// $order_by = [ $aColumns[$this->input->post('iSortCol_'.$i)] => $this->input->post('sSortDir_'.$i) ];
					$order = $aColumns[$this->input->post('iSortCol_' . $i)];
					$by = $this->input->post('sSortDir_' . $i); 

				}
			}
		}

		//limit
		if ($this->input->post('iDisplayLength') != null) {
			$limit = $this->input->post('iDisplayLength');
			$offset = 0;
		}

		//paginate
		if ($this->input->post('iDisplayStart') != 0) {
			// $limit = [$this->input->post('iDisplayLength') => $this->input->post('iDisplayStart')];
			$limit = $this->input->post('iDisplayLength');
			$offset = $this->input->post('iDisplayStart');
		}

		//search to get all data
		if ($this->input->post('sSearch') != '') { 
			$search = $this->trim_str($this->input->post('sSearch'));
			
			$this->db->group_start();
			foreach ($aColumns as $key => $value) {
				$this->db->or_like($value, $search);
			} 
			$this->db->group_end();
		} 
 		$this->load->model('Model_tbl_user_types');
 		$tbl_user_types = new Model_tbl_user_types();
 		// search($where = [], $field = '', $order = '', $limit = 0, $offset = 0, $group_by = '')
		$dataTable = $tbl_user_types->search([],$order,$by,0,0);

		return count($dataTable);
	}


	function save_detail() {
		$user_type_id = $this->input->post('user_type_id');
		$user_type = $this->input->post('user_type'); 
		$pages = $this->input->post('pages'); 
		// if ($password != $current_password) {
		// 	$password = password_hash($password, PASSWORD_BCRYPT);	
		// }


		$this->load->model('Model_tbl_user_types');
		$tbl_user_types = new Model_tbl_user_types();
		$tbl_user_types->load($user_type_id);
		$tbl_user_types->user_type = $user_type;
		$tbl_user_types->pages = $pages;
		$tbl_user_types->save();

		echo $tbl_user_types->user_type_id;


		



	}
	
	
}
