<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clients extends MY_Controller { 
	
	public function __construct()
  	{
		parent::__construct(); 
	    $this->checkLog();
  	}

	public function index()
	{
		$data['userdata'] = $this->session->userdata('user_data');
		$data['user_types'] = $this->fetchRawData("SELECT * FROM tbl_user_types");
		$this->load->view('admin/Clients.php',$data); 
	}   

	function ajax_table() {
		$aColumns = [
			'client_name',
			'client_email',
			'client_phone',
			'date_registered',
			'client_id',
		];
		$order = '';
		$by = '';
		$limit = 0;
		$offset = 0;
		//sort
		if ($this->input->post('iSortCol_0') != null) {
			for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
				if ($this->input->post('bSortable_' . $this->input->post('iSortCol_' . $i)) == true) {
					// $order_by = [ $aColumns[$this->input->post('iSortCol_'.$i)] => $this->input->post('sSortDir_'.$i) ];
					$order = $aColumns[$this->input->post('iSortCol_' . $i)];
					$by = $this->input->post('sSortDir_' . $i); 

				}
			}
		}

		//limit
		if ($this->input->post('iDisplayLength') != null) {
			$limit = $this->input->post('iDisplayLength');
			$offset = 0;
		}

		//paginate
		if ($this->input->post('iDisplayStart') != 0) {
			// $limit = [$this->input->post('iDisplayLength') => $this->input->post('iDisplayStart')];
			$limit = $this->input->post('iDisplayLength');
			$offset = $this->input->post('iDisplayStart');
		}

		//search to get all data
		if ($this->input->post('sSearch') != '') { 
			$search = $this->trim_str($this->input->post('sSearch'));
			
			$this->db->group_start();
			foreach ($aColumns as $key => $value) {
				$this->db->or_like($value, $search);
			} 
			$this->db->group_end();

			 

		} 
 		$this->load->model('Model_Query');
 		$clients = new Model_Query();
 		// search($where = [], $field = '', $order = '', $limit = 0, $offset = 0, $group_by = '')
		$dataTable = $clients->getView('tbl_clients',[],$order,$by,$limit,$offset);
		$data['data'] = [];
		foreach ($dataTable as $key => $value) {
			$btn_edit = '<button client_id="'.$value['client_id'].'" class="btn_edit btn btn-info btn-xs"><i class="fas fa-edit"></i> Update</button>';
			$btn_delete = '<button client_id="'.$value['client_id'].'" class="btn_delete btn btn-danger btn-xs"><i class="fas fa-trash"></i> Delete</button>';

			$data['data'][] = [
				$value['client_name'],
				$value['client_email'],
				$value['client_phone'],
				$value['date_registered'],
				$btn_edit
			];
		}

		$data['iTotalRecords'] = $this->get_total_records($this->input->post());;
		$data['iTotalDisplayRecords'] = count($dataTable);
		echo json_encode($data);
	}

	function get_total_records($post_data) {
		
		$aColumns = [
			'client_name',
			'client_email',
			'client_phone',
			'date_registered',
			'client_id',
		];
		$order = '';
		$by = '';
		$limit = 0;
		$offset = 0;
		if ($this->input->post('iSortCol_0') != null) {
			for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
				if ($this->input->post('bSortable_' . $this->input->post('iSortCol_' . $i)) == true) {
					// $order_by = [ $aColumns[$this->input->post('iSortCol_'.$i)] => $this->input->post('sSortDir_'.$i) ];
					$order = $aColumns[$this->input->post('iSortCol_' . $i)];
					$by = $this->input->post('sSortDir_' . $i); 

				}
			}
		}

		//limit
		if ($this->input->post('iDisplayLength') != null) {
			$limit = $this->input->post('iDisplayLength');
			$offset = 0;
		}

		//paginate
		if ($this->input->post('iDisplayStart') != 0) {
			// $limit = [$this->input->post('iDisplayLength') => $this->input->post('iDisplayStart')];
			$limit = $this->input->post('iDisplayLength');
			$offset = $this->input->post('iDisplayStart');
		}

		//search to get all data
		if ($this->input->post('sSearch') != '') { 
			$search = $this->trim_str($this->input->post('sSearch'));
			
			$this->db->group_start();
			foreach ($aColumns as $key => $value) {
				$this->db->or_like($value, $search);
			} 
			$this->db->group_end();
		} 
 		$this->load->model('Model_tbl_clients');
 		$clients = new Model_tbl_clients();
 		// search($where = [], $field = '', $order = '', $limit = 0, $offset = 0, $group_by = '')
		$dataTable = $clients->search([],$order,$by,0,0);

		return count($dataTable);
	}


	function save_client() {
		$client_id = $this->input->post('client_id');
		$client_name = $this->input->post('client_name'); 
		$client_email = $this->input->post('client_email'); 
		$client_phone = $this->input->post('client_phone'); 
		$client_password = $this->input->post('client_password');
		$current_password = $this->input->post('current_password'); 
		if ($client_password != $current_password) {
			$current_password = $client_password;
		}


		$this->load->model('Model_tbl_clients');
		$clients = new Model_tbl_clients();
		if ($client_id) {
			$clients->load($client_id);
		} else {
			$clients->date_registered = date('Y-m-d H:i:s');
		}
		$clients->client_name = $client_name;
		$clients->client_email = $client_email;
		$clients->client_phone = $client_phone;
		$clients->client_password = $current_password;
		$clients->save();

		echo $clients->client_id;

	}
	
	
}
