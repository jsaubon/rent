<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cars extends MY_Controller {

	public function index() {
		$offset = $this->input->get('offset') ? $this->input->get('offset') : 0;;
		$brand = $this->input->get('brand') ? $this->input->get('brand') : '';;
		$data['brands'] = $this->get_brands();
		$data['available_cars'] = $this->get_cars(1,$offset,$brand);
		$data['paginate_number'] = $this->get_paginate_number($brand);
		$this->load->view('public/Cars.php',$data);
	}
	
	function get_brands() {
		return $this->fetchRawData("SELECT *,(SELECT count(*) FROM tbl_car WHERE brand_id=tbl_brand.brand_id) as `count` FROM tbl_brand ORDER BY brand ASC");
	}

	function get_cars($all = 0,$offset,$brand) {
		$brand = $brand != '' ? " WHERE brand='$brand'" : '';
		$brand_id = $this->input->post('brand_id') ? ' WHERE brand_id='.$this->input->post('brand_id') : '';
		$model_id = $this->input->post('model_id') ? ' AND model_id='.$this->input->post('model_id') : '';
		$data = $this->fetchRawData("SELECT * FROM view_tbl_car $brand_id $model_id $brand LIMIT 5 OFFSET $offset ");
		if ($all) {
			return $data;
		} else {
			echo json_encode($data);;
		}
	}

	function get_paginate_number($brand) {
		$brand = $brand != '' ? " WHERE brand='$brand'" : '';
		$data = $this->fetchRawData("SELECT * FROM view_tbl_car $brand");
		$count = count($data);
		$paginate_number = $count / 5;
		return ceil($paginate_number);
		
	}



	function detail($id) {
		$data['car'] = $this->get_car($id);
		$this->load->view('public/CarDetails.php',$data);
	}

	function get_car($car_id) {
		$data =  $this->fetchRawData("SELECT * FROM view_tbl_car WHERE car_id=$car_id lIMIT 1");
		$data = reset($data);
		return $data;
	}

	function get_car_reviews() {
		$car_id = $this->input->post('car_id');
		$data = $this->fetchRawData("SELECT tbl_car_reviews.*,(SELECT client_name FROM tbl_clients WHERE client_id=tbl_car_reviews.client_id) as `client_name` FROM tbl_car_reviews WHERE car_id=$car_id ORDER BY `date` DESC");
		echo json_encode($data);;
	}
}