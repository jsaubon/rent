<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Profile extends MY_Controller {

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->check_if_logged();
	}

	function check_if_logged() {
		if (!$this->session->userdata('client_data')) {
			redirect(base_url('register'));
		}
	}
	public function index() { 
		$data['user_data'] = $this->session->userdata('client_data');
		$data['bookings'] = $this->get_bookings();
		$this->load->view('public/Profile.php',$data);
	}

	function save_profile() {
		$client_id = $this->input->post('client_id');
		$client_name = $this->input->post('client_name');
		$client_phone = $this->input->post('client_phone');

		$this->load->model('Model_tbl_clients');
		$clients = new Model_tbl_clients();
		$clients->load($client_id);
		$clients->client_id = $client_id;
		$clients->client_name = $client_name;
		$clients->client_phone = $client_phone;
		$clients->save();

		$data = $this->fetchRawData("SELECT * FROM tbl_clients WHERE client_id=$client_id");
		$this->session->set_userdata(array('user_data' => $data[0]));
		$this->session->profile_success_message = 'Profile Information Successfully Saved!';
		redirect('profile?saved=1');
	}


	function change_password() {
		$client_id = $this->input->post('client_id');
		$client_password = $this->input->post('client_password');
		$confirm_password = $this->input->post('confirm_password');
		if (strlen($client_password) < 9) {
			
			$this->session->change_password_error = 'Password Must be at least 8 characters';
			redirect('profile?saved=2');
		} else if ($client_password != $confirm_password) {
			$this->session->change_password_error = 'Password Mismatch';
			redirect('profile?saved=2');
		} else {
			$this->load->model('Model_tbl_clients');
			$clients = new Model_tbl_clients();
			$clients->load($client_id);
			$clients->client_id = $client_id;
			$clients->client_password = $client_password;
			$clients->save();

			$data = $this->fetchRawData("SELECT * FROM tbl_clients WHERE client_id=$client_id");
			$this->session->set_userdata(array('user_data' => $data[0]));
			$this->session->change_password_error = 'Password Successfully Changed!';
			redirect('profile?saved=2');
		}

		echo $this->session->change_password_error;
			
	}

	function get_bookings() {
		$userdata = $this->session->userdata['client_data'];
		$client_id = $userdata['client_id'];
		$data = $this->fetchRawData("SELECT * FROM view_tbl_client_booking WHERE client_id=$client_id ORDER BY date_start DESC");
		return $data;
	}

	function pay_with_gcash() {
		$app_id = "abaoUBkXAzhMoT6ezKiXMKh75bzyUEn8";
		$app_secret = "278d4a54e3ea1bfa4d847f325018a454e07afe4dfc893cc77cea4c3ba2540915";
		$subscriber_number = "9156954696";
		$amount = "1.00";
		$duration = "0";

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://devapi.globelabs.com.ph/payment/v1/smsoptin?app_id=".$app_id."&app_secret=".$app_secret."&subscriber_number=".$subscriber_number."&duration=".$duration."&amount=".$amount,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "",
		  CURLOPT_HTTPHEADER => array( "Host: devapi.globelabs.com.ph" ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  echo $response;
		}
	}

	function gcash_uri() {
		$data_json = file_get_contents('php://input');
		$this->db->query("INSERT INTO gcash_webhook (`content`) VALUES('$data_json')");
	}




}