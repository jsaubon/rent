<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bookings extends MY_Controller { 
	
	public function __construct()
  	{
		parent::__construct(); 
	    date_default_timezone_set('Asia/Manila');
  	}

  	function testing() {
  		$date_start = '2020-02-14';
  		$date_end = '2020-02-21';
  		$car_id=31;
  		// $booking_exist = $this->fetchRawData("SELECT * FROM tbl_client_booking WHERE DATE(date_start) >= DATE('$date_start') AND DATE(date_end) <= DATE('$date_start') AND DATE(date_start) >= DATE('$date_end') AND DATE(date_end) <= DATE('$date_end') AND car_id=$car_id");
  		$booking_exist = $this->fetchRawData("SELECT * FROM tbl_client_booking WHERE (DATE(date_start) BETWEEN DATE('$date_start') AND DATE('$date_end') OR DATE(date_end) BETWEEN DATE('$date_start') AND DATE('$date_end')) AND car_id=$car_id");

  		$this->pprint($booking_exist);
  	}


	function save_client() {
		$booking_id = $this->input->post('booking_id');
		$client_id = $this->input->post('client_id');
		$car_id = $this->input->post('car_id');
		$car_rate = $this->input->post('car_rate');
		$client_name = $this->input->post('client_name');
		$client_email = $this->input->post('client_email');
		$client_phone = $this->input->post('client_phone');
		$client_note = $this->input->post('client_note');
		$transaction_type = $this->input->post('transaction_type');
		$date_of_payment = $this->input->post('date_of_payment');
		$invoice_number = $this->input->post('invoice_number');
		$amount_paid = $this->input->post('amount_paid');
		$remitted_by = $this->input->post('remitted_by');
		$received_by = $this->input->post('received_by'); 

		$date_start = $this->input->post('date_start');
		$time_start = $this->input->post('time_start');
		$date_start = date('Y-m-d H:i:s',strtotime($date_start. ' '.str_replace(' ','',$time_start)));
		$date_end = $this->input->post('date_end');
		$time_end = $this->input->post('time_end');
		$date_end = date('Y-m-d H:i:s',strtotime($date_end. ' '.str_replace(' ','',$time_end)));
		$transaction_status  = $this->input->post('transaction_status');
		// if ($password != $current_password) {
		// 	$password = password_hash($password, PASSWORD_BCRYPT);	
		// }

		$booking_exist = $this->fetchRawData("SELECT * FROM tbl_client_booking WHERE (DATE(date_start) BETWEEN DATE('$date_start') AND DATE('$date_end') OR DATE(date_end) BETWEEN DATE('$date_start') AND DATE('$date_end')) AND car_id=$car_id");
		
		if (count($booking_exist) > 0) {
			echo "booking exist";
		} else {
			$this->load->model('Model_tbl_clients');
			$clients = new Model_tbl_clients();
			if ($client_id || $client_id != 0) {
				$clients->load($client_id);
				echo 'exist';;
			} else {
				$clients->date_registered = date('Y-m-d H:i:s');
				$exist = $this->fetchRawData("SELECT * FROM tbl_clients WHERE client_email='$client_email'");
				if (count($exist) > 0) {
					$clients->load($exist[0]['client_id']);
					echo 'exist';;
				}
			}
			$clients->client_name = $client_name;
			$clients->client_email = $client_email;
			$clients->client_phone = $client_phone;
			$clients->client_password = $client_phone;
			$clients->save();

			$client_id = $clients->client_id;



			$this->load->model('Model_tbl_client_booking');
			$client_booking = new Model_tbl_client_booking();
			if ($booking_id) {
				$client_booking->load($booking_id);
			}
			$client_booking->client_id = $client_id;
			$client_booking->car_id = $car_id;
			$client_booking->car_rate = $car_rate;
			$client_booking->client_note = $client_note;
			$client_booking->transaction_type = $transaction_type;
			$client_booking->date_start = $date_start;
			$client_booking->date_end = $date_end;
			$client_booking->transaction_status = $transaction_status;
			$client_booking->date_of_payment = $date_of_payment;
			$client_booking->invoice_number = $invoice_number;
			$client_booking->amount_paid = $amount_paid;
			$client_booking->remitted_by = $remitted_by;
			$client_booking->received_by = $received_by;
			$client_booking->save();
		}
			



	}

	
}