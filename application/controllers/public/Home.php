<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends MY_Controller {

	public function index() {
		$data['cars'] = $this->get_cars();
		$data['user_data'] = $this->session->userdata('client_data');
		$this->load->view('public/Home.php',$data);
	}
	function get_cars() {
		return $this->fetchRawData("SELECT * FROM view_tbl_car ORDER BY brand ASC");
	}
}