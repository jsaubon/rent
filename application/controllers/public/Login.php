<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends MY_Controller {

	public function index() {
		$data = [];
		if ($this->session->userdata('client_data') != null) {
			redirect(base_url('cars'));
		} else {
			$this->load->view('public/Login.php',$data);
		}
		
	}

	function register() {
		$client_email = $this->input->get('email');
		$client_data = $this->check_exist($client_email);
		if (count($this->check_exist($client_email)) > 0 && $client_email != '') {
			$this->session->set_userdata(array('client_data' => $client_data[0]));
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		} else {
			$data['client_name'] = $this->input->get('name');
			$data['client_email'] = $this->input->get('email');
			// $client_image = $this->input->get('image');
			$this->load->view('public/Register.php',$data);
		}
		$data = [];
		
	}

	function check_exist($client_email) {
		$data = $this->fetchRawData("SELECT * FROM tbl_clients WHERE client_email='$client_email'");
		return $data;

	}

	function register_client() {
		$client_id = $this->input->post('client_id');
		$client_name = $this->input->post('client_name'); 
		$client_email = $this->input->post('client_email'); 
		$client_phone = $this->input->post('client_phone'); 
		$client_password = $this->input->post('client_password');
		$current_password = $this->input->post('current_password'); 
		if ($client_password != $current_password) {
			$client_password = $client_password;
		} 


		$this->load->model('Model_tbl_clients');
		$clients = new Model_tbl_clients();
		if ($client_id) {
			$clients->load($client_id);
		} else {
			$clients->date_registered = date('Y-m-d H:i:s');
		}
		$clients->client_name = $client_name;
		$clients->client_email = $client_email;
		$clients->client_phone = $client_phone;
		$clients->client_password = $client_password;
		$clients->save();
		$client_id = $clients->client_id;

		$data = $this->fetchRawData("SELECT * FROM tbl_clients WHERE client_id='$client_id'");
		$this->session->set_userdata(array('client_data' => $data[0]));

	}

	function logout() {
		session_destroy();

		redirect(base_url());
	}

	function login_user() {
		$client_email = $this->input->post('email');
		$client_password = $this->input->post('password');
		$client_data = $this->check_exist($client_email);
		if (count($client_data) > 0) {
			if ($client_password == $client_data[0]['client_password']) {
				$this->session->set_userdata(array('client_data' => $client_data[0]));
				redirect(base_url('cars'));
				$this->session->login_error =  "";
			} else {
				$this->session->login_error =  "Username or Password is Incorrect";
				redirect(base_url('login'));
			}
				
		} else {
			$this->session->login_error =  "User doesn't exist, do you want to register?";
			redirect(base_url('login'));
		}
	}

}