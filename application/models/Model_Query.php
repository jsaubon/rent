<?php

class Model_Query extends MY_Model {

	public $selects = array();

	public function getSelect() {
		$selectQry = "";
		if ($this->selects) {
			$counter = 0;
			foreach ($this->selects as $key => $value) {
				$counter++;
				if ($counter == count($this->selects)) {
					$selectQry .= " {$value} ";
				} else {
					$selectQry .= " {$value}, ";
				}
			}
		}
		return $selectQry;
	}

	public function get_table($table) {
		$query = $this->db->get($table);
		return $query;
	}

	public function get_table_limit($table) {
		$this->db->limit(50);
		$this->db->order_by('date', 'desc');
		$query = $this->db->get($table);
		return $query;
	}

	public function sel_table($table, $table_id, $id_val) {
		$this->db->where($table_id, $id_val);
		$query = $this->db->get($table);
		return $query;
	}
	public function sel_tableL($table, $table_id, $id_val) {
		$this->db->like($table_id, $id_val);
		$query = $this->db->get($table);
		return $query;
	}

	public function update($table, $table_id, $id_val, $array) {
		$this->db->where($table_id, $id_val);
		$this->db->update($table, $array);
	}

	public function getView($table, $where, $field = '', $order = '', $limit = 0, $offset = 0, $groupby = '') {
		if ($this->getSelect() != "") {
			$this->db->select($this->getSelect());
		}
		$this->db->where($where);
		if ($field != '') {
			$this->db->order_by($field, $order);
		}
		if ($limit != 0) {
			$this->db->limit($limit, $offset);
		}
		if ($groupby != '') {
			$this->db->group_by($groupby);
		}

		$query = $this->db->get($table);

		$result = $query->result_array();
		foreach ($result as $index => $value) {
			foreach ($value as $key => $value) {
				$result[$index][$key] = str_replace("\\", '', $value);
			}
		}
		return $result;
	}

}