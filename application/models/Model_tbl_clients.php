<?php

class Model_tbl_clients extends MY_Model
{
    const DB_TABLE = 'tbl_clients';
    const DB_TABLE_PK = 'client_id';

 	public $client_id;
 	public $client_name;
 	public $client_email;
 	public $client_phone;
 	public $client_password;
 	public $date_registered;
 
}