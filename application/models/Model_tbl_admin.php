<?php

class Model_tbl_admin extends MY_Model
{
    const DB_TABLE = 'tbl_admin';
    const DB_TABLE_PK = 'admin_id';

 	public $admin_id;
 	public $name;
 	public $email_address;
 	public $username;
 	public $password; 
 	public $active;
 	public $user_type_id;
 
}