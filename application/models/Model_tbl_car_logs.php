<?php

class Model_tbl_car_logs extends MY_Model
{
    const DB_TABLE = 'tbl_car_logs';
    const DB_TABLE_PK = 'log_id';

 	public $log_id;
 	public $car_id;
 	public $date;
 	public $changes;
 	public $brand;
 	public $part;
 
}