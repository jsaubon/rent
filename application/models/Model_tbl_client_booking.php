<?php

class Model_tbl_client_booking extends MY_Model
{
    const DB_TABLE = 'tbl_client_booking';
    const DB_TABLE_PK = 'booking_id';

 	public $booking_id;
 	public $client_id;
 	public $car_id;
 	public $car_rate;
 	public $client_note;
 	public $transaction_type;
 	public $date_start;
 	public $date_end;
 	public $transaction_status;
 	public $date_of_payment;
 	public $invoice_number;
 	public $amount_paid;
 	public $remitted_by;
 	public $received_by;
 
}