<?php

class Model_tbl_car extends MY_Model
{
    const DB_TABLE = 'tbl_car';
    const DB_TABLE_PK = 'car_id';

 	public $car_id;
 	public $image;
 	public $brand_id;
 	public $model_id;
 	public $capacity;
 	public $transmission;
 	public $description;
 	public $active;
 	public $imei;
 
}