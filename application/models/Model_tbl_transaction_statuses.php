<?php

class Model_tbl_transaction_statuses extends MY_Model
{
    const DB_TABLE = 'tbl_transaction_statuses';
    const DB_TABLE_PK = 'status_id';

 	public $status_id;
 	public $transaction_status;
 
}