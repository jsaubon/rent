<?php

class Model_tbl_car_rate extends MY_Model
{
    const DB_TABLE = 'tbl_car_rate';
    const DB_TABLE_PK = 'rate_id';

 	public $rate_id;
 	public $car_id;
 	public $coverage;
 	public $rate;
 
}