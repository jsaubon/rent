<?php

class Model_tbl_car_model extends MY_Model
{
    const DB_TABLE = 'tbl_car_model';
    const DB_TABLE_PK = 'model_id';

 	public $model_id;
 	public $brand_id;
 	public $model;
 
}