<?php

class Model_tbl_expenses extends MY_Model
{
    const DB_TABLE = 'tbl_expenses';
    const DB_TABLE_PK = 'expense_id';

 	public $expense_id;
 	public $date;
 	public $description;
 	public $particulars;
 	public $amount;
 	public $requested_by;
 	public $remarks;
}