<?php
$this->load->view('admin/includes/header.php');
$this->load->view('admin/includes/nav-left.php');
$this->load->view('admin/includes/nav-top.php');
?>

<style>
    .table-loader {
        background-color: rgba(255,255,255,.7);
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
    }

    .dataTables_length {
        float: right;;
    }

    .dataTables_filter {
        float: left;
    }

    .control-label {
        margin-bottom: 0px;
    }

</style>
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div id="listSection" class="row fade show">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-loader" style="display: none;" >
                                    <svg class="circular" viewBox="25 25 50 50">
                                        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
                                    </svg>
                                </div>
                                    <h4 class="card-title">
                                        Administrators
                                        <a href="#" class="" id="btnNewUser" data-toggle="modal" data-target="#add-edit-modal" ><small><i class="fas fa-plus-circle"></i> Add New</small></a>
                                    </h4>

                                    <div class="table-responsive" >
                                        
                                        <table id="pageTable_admins" class="table stylish-table">
                                            <thead>
                                                <tr>
                                                    <th style="width: 1%">Status</th>
                                                    <th>User Type</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Login</th>
                                                    <th style="width: 14%">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->





<!-- sample modal content -->
<div id="add-edit-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                Administrator Information
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data" id="formAddUser" method="post" action="return false" class="floating-labels">
                    <input type="hidden" name="admin_id">
                    <input type="hidden" name="admin_pass_reset" value="0">
                    
                    <div class="row">
                        <div class="col-12 col-md-6 offset-md-3 m-t-40">
                            <div class="card">
                                <div class="card-body">
                                    <input type="hidden" name="admin_admin_id">
                                    <h4 style="margin-bottom: 17px">Basic Information</h4>
                                    <div class="form-group">
                                        <label for="UserType" class="control-label">User Type:</label>
                                        <select class="form-control" name="admin_user_type_id">
                                            <?php foreach ($user_types as $key => $value): ?>
                                                <option value="<?php echo $value['user_type_id'] ?>"><?php echo $value['user_type'] ?></option>
                                            <?php endforeach ?>
                                        </select>
                                        <span class="bar"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="control-label">Name:</label>
                                        <input required type="text" name="admin_name"  class="form-control">
                                        <span class="bar"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="control-label">Email:</label>
                                        <input required type="email" name="admin_email_address"  class="form-control">
                                        <span class="bar"></span>
                                    </div>
                                    <br>
                                    <h4 style="margin-bottom: 17px">User Account Information</h4>
                                    <div class="form-group">
                                        <label for="username" class="control-label">Username:</label>
                                        <input required name="admin_username" type="text" required="" class="form-control">
                                        <span class="bar"></span>
                                        <!-- <small id="errorUsername" class="hide form-control-feedback text-danger"> Username already exist!. </small> -->
                                    </div>
                                    <div class="form-group">
                                        <label for="password" class="control-label">Password:</label>
                                        <input required name="admin_password" type="password" required="" class="form-control">
                                        <span class="bar"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="Status" class="control-label">Status:</label>
                                        <select class="form-control" name="admin_active">
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                        <span class="bar"></span>
                                    </div>

                                    <button type="submit" class="btn btn-success waves-effect waves-light btn-block btn_add_edit_admin">Save changes</button>
                                </div>
                                    
                            </div>
                        </div>
                    </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
 <script>
$(document).ready(function() {
    $('#nav_admins').addClass('active');
    
    var pageTable_admins;
    var pageTable_admins_data =    [
                                {name: '<?php echo $this->security->get_csrf_token_name(); ?>' , value: '<?php echo $this->security->get_csrf_hash(); ?>'}
                            ];
    pageTable_admins = $('#pageTable_admins').DataTable({
        language: {
            search: '',
            searchPlaceholder: 'Search'
        },
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 50,
        dom: 'Rlfrtip',
        // "aoColumnDefs": [{ "asSorting": [ "desc", "asc" ], "aTargets": [ 8 ] }],
        bProcessing: true,
        bServerSide: true,
        sServerMethod: 'POST',
        sAjaxSource: '<?php echo base_url("admin/administrators/ajax_table") ?>',
        fnServerParams: function(aoData) {
            $.each(pageTable_admins_data, function(i, field) {
                aoData.push({ name: field.name, value: field.value });
            });
            $('.client-dashboard-loader').fadeIn('50');
        },
        fnDrawCallback: function(data) {
            // console.log(data);
            $('.client-dashboard-loader').fadeOut('50');
        },createdRow: function( row, data, dataIndex ) {
            


        }
    });

    var current_password = '';
    $('#formAddUser').on('submit', function(event) {
        event.preventDefault();
        var admin_id = $('#formAddUser').find('[name="admin_admin_id"]').val();
        var user_type_id = $('#formAddUser').find('[name="admin_user_type_id"]').val();
        var name = $('#formAddUser').find('[name="admin_name"]').val();
        var email_address = $('#formAddUser').find('[name="admin_email_address"]').val();
        var username = $('#formAddUser').find('[name="admin_username"]').val();
        var password = $('#formAddUser').find('[name="admin_password"]').val();
        var active = $('#formAddUser').find('[name="admin_active"]').val();
        var pass_reset = $('#formAddUser').find('[name="admin_pass_reset"]').val();

        
        $.post('<?php echo base_url('admin/administrators/save_admin') ?>', {'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',admin_id,name,user_type_id,email_address,username,current_password,password,active,pass_reset} , function(data, textStatus, xhr) { 
            $('#add-edit-modal').modal('hide');
            pageTable_admins.ajax.reload();
        });
    });


    $('#pageTable_admins').on('click', '.btn_edit', function(event) {
        event.preventDefault();
        var admin_id = $(this).attr('admin_id');
        var data =  {
                        '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
                        table: 'tbl_admin',
                        action: 'get',
                        select: {
                                    
                                },
                        where:  {
                                    admin_id:admin_id
                                },
                        field: '',
                        order: '',
                        limit: 0,
                        offset: 0,
                        group_by: ''
        
                    };
        $.post('<?php echo base_url('admin/administrators/modelTable') ?>', data, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            $.each(data, function(index, val) {
                $.each(val, function(k, v) {
                     $('#formAddUser').find('[name="admin_'+k+'"]').val(v);
                     setTimeout(function(){
                        $('#formAddUser').find('[name="admin_'+k+'"]').focus();
                    },1000);
                });

                current_password = val.password;
            });

            $('#add-edit-modal').modal('show');
        });
    });   

    $('#pageTable_admins').on('click', '.btn_delete', function(event) {
        var admin_id = $(this).attr('admin_id');
        var data =  {
                        '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
                        table: 'tbl_admin',
                        pk: admin_id,
                        action: 'delete'
                    }; 
        $.post('<?php echo base_url('admin/administrators/modelTable') ?>', data, function(data, textStatus, xhr) { 
            pageTable_admins.ajax.reload();
        });
    });

    $('#btnNewUser').on('click', function(event) {
        event.preventDefault();
        $('#formAddUser')[0].reset();
        $('[name="admin_admin_id"]').val('');
    });


});
</script>
<?php
$this->load->view('admin/includes/footer.php');
?>
