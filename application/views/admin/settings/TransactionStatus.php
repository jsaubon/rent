<?php
$this->load->view('admin/includes/header.php');
$this->load->view('admin/includes/nav-left.php');
$this->load->view('admin/includes/nav-top.php');
?>

<style>
    .table-loader {
        background-color: rgba(255,255,255,.7);
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
    }

    .dataTables_length {
        float: right;;
    }

    .dataTables_filter {
        float: left;
    }

    .control-label {
        margin-bottom: 0px;
    }

</style>
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div id="listSection" class="row fade show">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-loader" style="display: none;" >
                                    <svg class="circular" viewBox="25 25 50 50">
                                        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
                                    </svg>
                                </div>
                                    <h4 class="card-title">
                                        Transaction Status
                                        <a href="#" class="" id="btn-new-form" data-toggle="modal" data-target="#add-edit-modal" ><small><i class="fas fa-plus-circle"></i> Add New</small></a>
                                    </h4>

                                    <div class="table-responsive" >
                                        
                                        <table id="pageTable_transaction_statuses" class="table stylish-table">
                                            <thead>
                                                <tr>
                                                    <th>Status</th>
                                                    <th style="width: 14%">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->





<!-- sample modal content -->
<div id="add-edit-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                Transaction Status Information
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data" id="formAddTransactionStatus" method="post" action="return false" class="floating-labels">
                    <input type="hidden" name="transaction_status_status_id">
                    
                    <div class="row">
                        <div class="col-12 col-md-6 offset-md-3 m-t-40">
                            <div class="card">
                                <div class="card-body">
                                    <h4 style="margin-bottom: 17px">Basic Information</h4>
                                    <div class="form-group">
                                        <label for="name" class="control-label">Name:</label>
                                        <input required type="text" name="transaction_status_transaction_status"  class="form-control">
                                        <span class="bar"></span>
                                    </div>

                                    <button type="submit" class="btn btn-success waves-effect waves-light btn-block ">Save changes</button>
                                </div>
                                    
                            </div>
                        </div>
                    </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
 <script>
$(document).ready(function() {
    $('[data-target="#setup_settings"]').click();
    
    var pageTable_transaction_statuses;
    var pageTable_transaction_statuses_data =    [
                                {name: '<?php echo $this->security->get_csrf_token_name(); ?>' , value: '<?php echo $this->security->get_csrf_hash(); ?>'}
                            ];
    pageTable_transaction_statuses = $('#pageTable_transaction_statuses').DataTable({
        language: {
            search: '',
            searchPlaceholder: 'Search'
        },
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 50,
        dom: 'Rlfrtip',
        // "aoColumnDefs": [{ "asSorting": [ "desc", "asc" ], "aTargets": [ 8 ] }],
        bProcessing: true,
        bServerSide: true,
        sServerMethod: 'POST',
        sAjaxSource: '<?php echo base_url("admin/settings/transactionStatus/ajax_table") ?>',
        fnServerParams: function(aoData) {
            $.each(pageTable_transaction_statuses_data, function(i, field) {
                aoData.push({ name: field.name, value: field.value });
            });
            $('.client-dashboard-loader').fadeIn('50');
        },
        fnDrawCallback: function(data) {
            // console.log(data);
            $('.client-dashboard-loader').fadeOut('50');
        },createdRow: function( row, data, dataIndex ) {
            


        }
    });

    var current_password = '';
    $('#formAddTransactionStatus').on('submit', function(event) {
        event.preventDefault();
        var status_id = $('#formAddTransactionStatus').find('[name="transaction_status_status_id"]').val();
        var transaction_status = $('#formAddTransactionStatus').find('[name="transaction_status_transaction_status"]').val();

        
        $.post('<?php echo base_url('admin/settings/transactionStatus/save_detail') ?>', {'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',status_id,transaction_status} , function(data, textStatus, xhr) { 
            $('#add-edit-modal').modal('hide');
            pageTable_transaction_statuses.ajax.reload();
        });
    });


    $('#pageTable_transaction_statuses').on('click', '.btn_edit', function(event) {
        event.preventDefault();
        var status_id = $(this).attr('status_id');
        var data =  {
                        '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
                        table: 'tbl_transaction_statuses',
                        action: 'get',
                        select: {
                                    
                                },
                        where:  {
                                    status_id:status_id
                                },
                        field: '',
                        order: '',
                        limit: 0,
                        offset: 0,
                        group_by: ''
        
                    };
        $.post('<?php echo base_url('admin/settings/transactionStatus/modelTable') ?>', data, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            $.each(data, function(index, val) {
                $.each(val, function(k, v) {
                     $('#formAddTransactionStatus').find('[name="transaction_status_'+k+'"]').val(v);
                     setTimeout(function(){
                        $('#formAddTransactionStatus').find('[name="transaction_status_'+k+'"]').focus();
                    },1000);
                });

            });

            $('#add-edit-modal').modal('show');
        });
    });   

    $('#pageTable_transaction_statuses').on('click', '.btn_delete', function(event) {
        var status_id = $(this).attr('status_id');
        var data =  {
                        '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
                        table: 'tbl_transaction_statuses',
                        pk: status_id,
                        action: 'delete'
                    }; 
        $.post('<?php echo base_url('admin/settings/transactionStatus/modelTable') ?>', data, function(data, textStatus, xhr) { 
            pageTable_transaction_statuses.ajax.reload();
        });
    });

    $('#btn-new-form').on('click', function(event) {
        event.preventDefault();
        $('#formAddTransactionStatus')[0].reset();
        $('[name="transaction_status_status_id"]').val('');
    });



});
</script>
<?php
$this->load->view('admin/includes/footer.php');
?>
