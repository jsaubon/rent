<?php
$this->load->view('admin/includes/header.php');
$this->load->view('admin/includes/nav-left.php');
$this->load->view('admin/includes/nav-top.php');
?>

<style>
    .table-loader {
        background-color: rgba(255,255,255,.7);
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
    }

    .dataTables_length {
        float: right;;
    }

    .dataTables_filter {
        float: left;
    }

    .control-label {
        margin-bottom: 0px;
    }

</style>
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div id="listSection" class="row fade show">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-loader" style="display: none;" >
                                    <svg class="circular" viewBox="25 25 50 50">
                                        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
                                    </svg>
                                </div>
                                    <h4 class="card-title">
                                        User Types
                                        <a href="#" class="" id="btn-new-form" data-toggle="modal" data-target="#add-edit-modal" ><small><i class="fas fa-plus-circle"></i> Add New</small></a>
                                    </h4>

                                    <div class="table-responsive" >
                                        
                                        <table id="pageTable_user_types" class="table stylish-table">
                                            <thead>
                                                <tr>
                                                    <th>User Type</th>
                                                    <th>Pages</th>
                                                    <th style="width: 14%">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->





<!-- sample modal content -->
<div id="add-edit-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                Brand Information
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data" id="formAddUserType" method="post" action="return false" class="floating-labels">
                    <input type="hidden" name="user_type_user_type_id">
                    
                    <div class="row">
                        <div class="col-12 col-md-6 offset-md-3 m-t-40">
                            <div class="card">
                                <div class="card-body">
                                    <h4 style="margin-bottom: 17px">User Type Information</h4>
                                    <div class="form-group">
                                        <label for="name" class="control-label">User Type:</label>
                                        <input required type="text" name="user_type_user_type"  class="form-control">
                                        <span class="bar"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Pages</label>
                                        <select name="user_type_pages" id="" multiple class="form-control">
                                            <option value="Dashboard">Dashboard</option>
                                            <option value="ProTrack">ProTrack</option>
                                            <option value="PureChat">PureChat</option>
                                            <option value="Bookings">Bookings</option>
                                            <option value="Cars Inventory">Cars Inventory</option>
                                            <option value="Car Reviews">Car Reviews</option>
                                            <option value="Car Maintenance">Car Maintenance</option>
                                            <option value="Expenses">Expenses</option>
                                            <option value="Reports">Reports</option>
                                            <option value="Cars Setup">Cars Setup</option>
                                            <option value="User Types">User Types</option>
                                            <option value="Transaction Statuses">Transaction Statuses</option>
                                            <option value="Clients">Clients</option>
                                            <option value="User Accounts">User Accounts</option>
                                        </select>
                                    </div>

                                    <button type="submit" class="btn btn-success waves-effect waves-light btn-block ">Save changes</button>
                                </div>
                                    
                            </div>
                        </div>
                    </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
 <script>
$(document).ready(function() {
    $('#nav_userTypes').addClass('active');
    
    var pageTable_user_types;
    var pageTable_user_types_data =    [
                                {name: '<?php echo $this->security->get_csrf_token_name(); ?>' , value: '<?php echo $this->security->get_csrf_hash(); ?>'}
                            ];
    pageTable_user_types = $('#pageTable_user_types').DataTable({
        language: {
            search: '',
            searchPlaceholder: 'Search'
        },
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 50,
        dom: 'Rlfrtip',
        // "aoColumnDefs": [{ "asSorting": [ "desc", "asc" ], "aTargets": [ 8 ] }],
        bProcessing: true,
        bServerSide: true,
        sServerMethod: 'POST',
        sAjaxSource: '<?php echo base_url("admin/settings/userTypes/ajax_table") ?>',
        fnServerParams: function(aoData) {
            $.each(pageTable_user_types_data, function(i, field) {
                aoData.push({ name: field.name, value: field.value });
            });
            $('.client-dashboard-loader').fadeIn('50');
        },
        fnDrawCallback: function(data) {
            // console.log(data);
            $('.client-dashboard-loader').fadeOut('50');
        },createdRow: function( row, data, dataIndex ) {
            


        }
    });

    var current_password = '';
    $('#formAddUserType').on('submit', function(event) {
        event.preventDefault();
        var user_type_id = $('#formAddUserType').find('[name="user_type_user_type_id"]').val();
        var user_type = $('#formAddUserType').find('[name="user_type_user_type"]').val();
        var pages = $('#formAddUserType').find('[name="user_type_pages"]').val();

        pages = pages.join(',');
        $.post('<?php echo base_url('admin/settings/userTypes/save_detail') ?>', {'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',user_type_id,user_type,pages} , function(data, textStatus, xhr) { 
            $('#add-edit-modal').modal('hide');
            pageTable_user_types.ajax.reload();
        });
    });


    $('#pageTable_user_types').on('click', '.btn_edit', function(event) {
        event.preventDefault();
        var user_type_id = $(this).attr('user_type_id');
        var data =  {
                        '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
                        table: 'tbl_user_types',
                        action: 'get',
                        select: {
                                    
                                },
                        where:  {
                                    user_type_id:user_type_id
                                },
                        field: '',
                        order: '',
                        limit: 0,
                        offset: 0,
                        group_by: ''
        
                    };
        $.post('<?php echo base_url('admin/settings/userTypes/modelTable') ?>', data, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            $.each(data, function(index, val) {
                $.each(val, function(k, v) {
                     $('#formAddUserType').find('[name="user_type_'+k+'"]').val(v);
                     setTimeout(function(){
                        $('#formAddUserType').find('[name="user_type_'+k+'"]').focus();
                    },1000);
                });

                current_password = val.password;
            });

            $('#add-edit-modal').modal('show');
        });
    });   

    $('#pageTable_user_types').on('click', '.btn_delete', function(event) {
        var user_type_id = $(this).attr('user_type_id');
        var data =  {
                        '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
                        table: 'tbl_user_types',
                        pk: user_type_id,
                        action: 'delete'
                    }; 
        $.post('<?php echo base_url('admin/settings/userTypes/modelTable') ?>', data, function(data, textStatus, xhr) { 
            pageTable_user_types.ajax.reload();
        });
    });

    $('#btn-new-form').on('click', function(event) {
        event.preventDefault();
        $('#formAddUserType')[0].reset();
        $('[name="user_type_user_type_id"]').val('');
    });



});
</script>
<?php
$this->load->view('admin/includes/footer.php');
?>
