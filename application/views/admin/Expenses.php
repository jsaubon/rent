<?php
$this->load->view('admin/includes/header.php');
$this->load->view('admin/includes/nav-left.php');
$this->load->view('admin/includes/nav-top.php');
?>

<style>
    .table-loader {
        background-color: rgba(255,255,255,.7);
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
    }

    .dataTables_length {
        float: right;;
    }

    .dataTables_filter {
        float: left;
    }

    .control-label {
        margin-bottom: 0px;
    }

</style>
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div id="listSection" class="row fade show">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-loader" style="display: none;" >
                                    <svg class="circular" viewBox="25 25 50 50">
                                        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
                                    </svg>
                                </div>
                                    <h4 class="card-title">
                                        Expenses
                                        <a href="#" class="" id="btn-new-form" data-toggle="modal" data-target="#add-edit-modal" ><small><i class="fas fa-plus-circle"></i> Add New</small></a>
                                    </h4>

                                    <div class="table-responsive" >
                                        
                                        <table id="pageTable_brands" class="table stylish-table">
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Description</th>
                                                    <th>Particulars</th>
                                                    <th>Amount</th>
                                                    <th>Requested By</th>
                                                    <th>Remarks</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->





<!-- sample modal content -->
<div id="add-edit-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                Brand Information
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data" id="formAddExpenses" method="post" action="return false" class="floating-labels">
                    <input type="hidden" name="expenses_expense_id">
                    
                    <div class="row">
                        <div class="col-12 col-md-6 offset-md-3 m-t-40">
                            <div class="card">
                                <div class="card-body">
                                    <h4 style="margin-bottom: 17px">Expenses Information</h4>

                                    <div class="form-group">
                                        <label for="date" class="control-label">Date</label>
                                        <input type="date" name="expenses_date" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="description" class="control-label">Description</label>
                                        <input type="text" name="expenses_description" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="particulars" class="control-label">Particulars</label>
                                        <input type="text" name="expenses_particulars" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="amount" class="control-label">Amount</label>
                                        <input type="number" name="expenses_amount" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="requested_by" class="control-label">Requested By</label>
                                        <input type="text" name="expenses_requested_by" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="remarks" class="control-label">Remarks</label>
                                        <textarea name="expenses_remarks" class="form-control" cols="30" rows="5"></textarea>
                                    </div>

                                    <button type="submit" class="btn btn-success waves-effect waves-light btn-block ">Save changes</button>
                                </div>
                                    
                            </div>
                        </div>
                    </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
 <script>
$(document).ready(function() {
    $('#nav_expenses').addClass('active');
    
    var pageTable_brands;
    var pageTable_brands_data =    [
                                {name: '<?php echo $this->security->get_csrf_token_name(); ?>' , value: '<?php echo $this->security->get_csrf_hash(); ?>'}
                            ];
    pageTable_brands = $('#pageTable_brands').DataTable({
        language: {
            search: '',
            searchPlaceholder: 'Search'
        },
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 50,
        dom: 'Rlfrtip',
        // "aoColumnDefs": [{ "asSorting": [ "desc", "asc" ], "aTargets": [ 8 ] }],
        bProcessing: true,
        bServerSide: true,
        sServerMethod: 'POST',
        sAjaxSource: '<?php echo base_url("admin/expenses/ajax_table") ?>',
        fnServerParams: function(aoData) {
            $.each(pageTable_brands_data, function(i, field) {
                aoData.push({ name: field.name, value: field.value });
            });
            $('.client-dashboard-loader').fadeIn('50');
        },
        fnDrawCallback: function(data) {
            // console.log(data);
            $('.client-dashboard-loader').fadeOut('50');
        },createdRow: function( row, data, dataIndex ) {
            


        }
    });

    var current_password = '';
    $('#formAddExpenses').on('submit', function(event) {
        event.preventDefault();
        var data = {
            expense_id:$('#formAddExpenses').find('[name="expenses_expense_id"]').val(),
            date:$('#formAddExpenses').find('[name="expenses_date"]').val(),
            description:$('#formAddExpenses').find('[name="expenses_description"]').val(),
            particulars:$('#formAddExpenses').find('[name="expenses_particulars"]').val(),
            amount:$('#formAddExpenses').find('[name="expenses_amount"]').val(),
            requested_by:$('#formAddExpenses').find('[name="expenses_requested_by"]').val(),
            remarks:$('#formAddExpenses').find('[name="expenses_remarks"]').val()
        };
            
        
        $.post('<?php echo base_url('admin/expenses/save_detail') ?>',data , function(data, textStatus, xhr) { 
            $('#add-edit-modal').modal('hide');
            pageTable_brands.ajax.reload();
        });
    });


    $('#pageTable_brands').on('click', '.btn_edit', function(event) {
        event.preventDefault();
        var expense_id = $(this).attr('expense_id');
        var data =  {
                        '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
                        table: 'tbl_expenses',
                        action: 'get',
                        select: {
                                    
                                },
                        where:  {
                                    expense_id:expense_id
                                },
                        field: '',
                        order: '',
                        limit: 0,
                        offset: 0,
                        group_by: ''
        
                    };
        $.post('<?php echo base_url('admin/expenses/modelTable') ?>', data, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            $.each(data, function(index, val) {
                $.each(val, function(k, v) {
                     $('#formAddExpenses').find('[name="expenses_'+k+'"]').val(v);
                });

                var date = val.date;
                $('#formAddExpenses').find('[name="expenses_date"]').val(moment(date).format('YYYY-MM-DD'));
            });

            $('#add-edit-modal').modal('show');
        });
    });   

    $('#pageTable_brands').on('click', '.btn_delete', function(event) {
        var expense_id = $(this).attr('expense_id');
        var data =  {
                        '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
                        table: 'tbl_expenses',
                        pk: expense_id,
                        action: 'delete'
                    }; 
        $.post('<?php echo base_url('admin/expenses/modelTable') ?>', data, function(data, textStatus, xhr) { 
            pageTable_brands.ajax.reload();
        });
    });

    $('#btn-new-form').on('click', function(event) {
        event.preventDefault();
        $('#formAddExpenses')[0].reset();
        $('[name="expenses_expense_id"]').val('');
    });



});
</script>
<?php
$this->load->view('admin/includes/footer.php');
?>
