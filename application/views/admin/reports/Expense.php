<?php
$this->load->view('admin/includes/header.php');
$this->load->view('admin/includes/nav-left.php');
$this->load->view('admin/includes/nav-top.php');
?>
<style>
	#map {
		height: 100%;
	}

	.table {
		font-size: 11px;
	}
	.table select{
		font-size: 11px;
	}

    #form-filter label {
        margin-bottom: 0px;
        margin-top: 10px;
    }

</style>
<!-- Begin Page Content -->
<div class="container-fluid">
	<div class="row">
        <div class="col-12 ">
            <div class="card ">
                <div class="card-body">
                    <h3>Expense Tracking</h3>

                    <h4 class="text-center">Filter</h4>
                    <div class="row">
                        <div class="col-12 col-md-6 offset-md-3">
                            <form action="return false" method="POST" id="form-filter">
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <label for="">Start Date</label>
                                        <input type="date" required class="form-control" name="date_start" >
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <label for="">End Date</label>
                                        <input  type="date" required class="form-control" name="date_end" >
                                    </div>
                                    <div class="col-12">
                                        <br>
                                        <button type="submit" class="btn btn-block btn-info btn-submit">Search</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-12" style="margin-top: 20px;">
                            <table class="table table-striped table-expense-tracking">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Description</th>
                                        <th>Particulars</th>
                                        <th>Amount</th>
                                        <th>Requested By</th>
                                        <th>Remarks</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                            
                </div>
            </div>
        </div>
		
	    
	</div>
	
	  	

</div>
<!-- /.container-fluid -->

    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
 <script>
$(document).ready(function() {
$('[data-target="#client_reports"]').click();
	
var table_expense_tracking = $('.table-expense-tracking').DataTable({
    language: {
        search: '',
        searchPlaceholder: 'Search'
    },
    'aaSorting': [],
    "bPaginate": true,
    // "bLengthChange": false,
    // "bFilter": true,
    "bInfo": false,
    dom: 'Bfrtip',
    buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
    ],
});

$('title').html('Expense Tracking');

$('.buttons-html5,.buttons-print').addClass('btn');
$('.buttons-html5,.buttons-print').addClass('btn-primary');
$('.buttons-html5,.buttons-print').addClass('mr-1');
$('#form-filter').on('submit', function(event) {
    event.preventDefault();
    
    var data = $(this).serialize();
    table_expense_tracking.clear().draw();
    $.post('<?php echo base_url('admin/reports/expense/get_report_filter') ?>', data, function(data, textStatus, xhr) {
        data = JSON.parse(data);
        $.each(data, function(index, val) {
             table_expense_tracking.row.add([
                                    moment(val.date).format('L'),
                                    val.description,
                                    val.particulars,
                                    val.amount,
                                    val.requested_by,
                                    val.remarks
                                    ]);
             table_expense_tracking.draw(false);
        });
    });
});


$('[name="car_brand"]').on('change', function(event) {
    event.preventDefault();
    var brand_id = $(this).find('option:selected').attr('brand_id');;
    var data =  {
                    table: 'tbl_car_model',
                    action: 'get',
                    select: {
                                
                            },
                    where:  {
                                brand_id: brand_id
                            },
                    field: '',
                    order: '',
                    limit: 0,
                    offset: 0,
                    group_by: ''
    
                };
    $('[name="car_model"]').empty();
    $.post('<?php echo base_url('admin/reports/expense/modelTable') ?>', data, function(data, textStatus, xhr) {
        data = JSON.parse(data);
        console.log(data);
        $('[name="car_model"]').append('<option value="All">All</option>');
        $.each(data, function(index, val) {
            $('[name="car_model"]').append('<option value="'+val.model+'">'+val.model+'</option>');
        });
    });
});


});
</script>


<?php
$this->load->view('admin/includes/footer.php');
?>
