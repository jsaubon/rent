<?php
$this->load->view('admin/includes/header.php');
$this->load->view('admin/includes/nav-left.php');
$this->load->view('admin/includes/nav-top.php');
?>
<style>
	#map {
		height: 100%;
	}

	.table {
		font-size: 11px;
	}
	.table select{
		font-size: 11px;
	}

    #form-filter label {
        margin-bottom: 0px;
        margin-top: 10px;
    }

</style>
<!-- Begin Page Content -->
<div class="container-fluid">
	<div class="row">
        <div class="col-12 ">
            <div class="card ">
                <div class="card-body">
                    <h3>Account Tracking</h3>

                    <h4 class="text-center">Filter</h4>
                    <div class="row">
                        <div class="col-12 col-md-6 offset-md-3">
                            <form action="return false" method="POST" id="form-filter">
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <label for="">Status</label>
                                        <select required name="transaction_status"  class="form-control">
                                            <option value="All">All</option>
                                            <?php foreach ($transaction_statuses as $key => $status): ?>
                                                <option value="<?php echo $status['transaction_status'] ?>"><?php echo $status['transaction_status'] ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <label for="">Transaction Type</label>
                                        <select required name="transaction_type"  class="form-control">
                                            <option value="All">All</option>
                                            <?php foreach ($transaction_types as $key => $transaction_type): ?>
                                                <option value="<?php echo $transaction_type['type'] ?>"><?php echo $transaction_type['type'] ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <label for="">Brand</label>
                                        <select required name="car_brand"  class="form-control">
                                            <option value="All">All</option>
                                            <?php foreach ($car_brands as $key => $car_brand): ?>
                                                <option brand_id="<?php echo $car_brand['brand_id'] ?>" value="<?php echo $car_brand['brand'] ?>"><?php echo $car_brand['brand'] ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <label for="">Model</label>
                                        <select required name="car_model"  class="form-control">
                                            <option value="All">All</option>
                                        </select>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <label for="">Start Date</label>
                                        <input type="date" required class="form-control" name="date_start" >
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <label for="">End Date</label>
                                        <input  type="date" required class="form-control" name="date_end" >
                                    </div>
                                    <div class="col-12">
                                        <br>
                                        <button type="submit" class="btn btn-block btn-info btn-submit">Search</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-12" style="margin-top: 20px;">
                            <table class="table table-striped table-account-tracking">
                                <thead>
                                    <tr>
                                        <th>Date of <br>Payment</th>
                                        <th>Client</th>
                                        <th>Invoice #</th>
                                        <th>Date Start</th>
                                        <th>Date End</th>
                                        <th>Amount Paid</th>
                                        <th>Vehicle Type</th>
                                        <th>Remitted By</th>
                                        <th>Received By</th>
                                        <th>Remarks</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                            
                </div>
            </div>
        </div>
		
	    
	</div>
	
	  	

</div>
<!-- /.container-fluid -->

    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
 <script>
$(document).ready(function() {
$('[data-target="#client_reports"]').click();
	
var table_account_tracking = $('.table-account-tracking').DataTable({
    language: {
        search: '',
        searchPlaceholder: 'Search'
    },
    'aaSorting': [],
    "bPaginate": true,
    // "bLengthChange": false,
    // "bFilter": true,
    "bInfo": false,
    dom: 'Bfrtip',
    buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
    ],
});

$('title').html('Account Tracking');

$('.buttons-html5,.buttons-print').addClass('btn');
$('.buttons-html5,.buttons-print').addClass('btn-primary');
$('.buttons-html5,.buttons-print').addClass('mr-1');
$('#form-filter').on('submit', function(event) {
    event.preventDefault();
    
    var data = $(this).serialize();
    table_account_tracking.clear().draw();
    $.post('<?php echo base_url('admin/reports/account/get_report_filter') ?>', data, function(data, textStatus, xhr) {
        data = JSON.parse(data);
        console.log(data);
        $.each(data, function(index, val) {
             table_account_tracking.row.add([
                                    moment(val.date_of_payment).format('L'),
                                    val.client_name,
                                    val.invoice_number,
                                    moment(val.date_start).format('L LT'),
                                    moment(val.date_end).format('L LT'),
                                    val.amount_paid,
                                    val.car,
                                    val.remitted_by_name,
                                    val.received_by_name,
                                    val.client_note
                                    ]);
             table_account_tracking.draw(false);
        });
    });
});


$('[name="car_brand"]').on('change', function(event) {
    event.preventDefault();
    var brand_id = $(this).find('option:selected').attr('brand_id');;
    var data =  {
                    table: 'tbl_car_model',
                    action: 'get',
                    select: {
                                
                            },
                    where:  {
                                brand_id: brand_id
                            },
                    field: '',
                    order: '',
                    limit: 0,
                    offset: 0,
                    group_by: ''
    
                };
    $('[name="car_model"]').empty();
    $.post('<?php echo base_url('admin/reports/account/modelTable') ?>', data, function(data, textStatus, xhr) {
        data = JSON.parse(data);
        console.log(data);
        $('[name="car_model"]').append('<option value="All">All</option>');
        $.each(data, function(index, val) {
            $('[name="car_model"]').append('<option value="'+val.model+'">'+val.model+'</option>');
        });
    });
});


});
</script>


<?php
$this->load->view('admin/includes/footer.php');
?>
