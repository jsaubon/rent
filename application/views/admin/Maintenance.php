<?php
$this->load->view('admin/includes/header.php');
$this->load->view('admin/includes/nav-left.php');
$this->load->view('admin/includes/nav-top.php');
?>
<style>
    #map {
        height: 100%;
    }

    .table {
        font-size: 11px;
    }
    .table select{
        font-size: 11px;
    }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">
    <div class="row">
        
        <div class="col-12 col-md-12">
            <div class="card">
               <div class="card-body">
                <h3>Car Maintenance</h3>
                   <form id="formAddLogs" action="return false" method="POST">
                        <div class="row" >
                            <input type="hidden" name="log_id">
                            
                            <div class="col-12 col-md-3">
                                <div class="form-group">
                                    <label for="car">Car</label>
                                    <select name="car_id" required="" class="form-control">
                                        <option value="">Select Car</option>
                                        <?php foreach ($cars as $key => $car): ?>
                                            <option value="<?php echo $car['car_id'] ?>"><?php echo $car['brand'].' '.$car['model'] ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-md-2">
                                <div class="form-group">
                                    <label for="date">Date</label>
                                    <input type="date" class="form-control" name="date">
                                </div>
                            </div>
                            <div class="col-12 col-md-2">
                                <div class="form-group">
                                    <label for="changes">Changes</label>
                                    <input type="text" class="form-control" name="changes">
                                </div>
                                
                            </div>
                            <div class="col-12 col-md-2">
                                <div class="form-group">
                                    <label for="brand">Brand</label>
                                    <input type="text" class="form-control" name="brand">
                                </div>
                                
                            </div>
                            <div class="col-12 col-md-2">
                                <div class="form-group">
                                    <label for="part">Part</label>
                                    <input type="text" class="form-control" name="part">
                                </div>
                                
                            </div>
                            <div class="col-12 col-md-1">
                                <div class="form-group">
                                    <label for="" style="color: white">Save</label>
                                    <button type="submit" class="btn btn-success waves-effect waves-light btn-block ">Save</button>
                                </div>
                                    
                            </div>
                        </div>
                    </form>

                    <div class="row">
                        <div class="col-12">
                            <table class="table table-stripped table_car_logs">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Changes</th>
                                        <th>Brand</th>
                                        <th>Part</th>
                                        <!-- <th style="width: 1px !important">Action</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
               </div>
            </div>
        </div>
        
        
    </div>
    
        

</div>
<!-- /.container-fluid -->

 <script>
$(document).ready(function() {
    $('#nav_maintenance').addClass('active');
    var table_car_logs = $('.table_car_logs').DataTable({
        language: {
            search: '',
            searchPlaceholder: 'Search'
        },
        'aaSorting': [],
        "bPaginate": true,
        "bLengthChange": true,
        "bFilter": true,
        "bInfo": true,
    });

    $('#formAddLogs').on('submit', function(event) {
        event.preventDefault();
        var data =  {
                        table: 'tbl_car_logs',
                        pk: $(this).find('[name="log_id"]').val() == '' ? '' : $(this).find('[name="log_id"]').val(),
                        action: 'save',
                        fields: {
                                    car_id: $(this).find('[name="car_id"]').val(),
                                    date: $(this).find('[name="date"]').val(),
                                    changes: $(this).find('[name="changes"]').val(),
                                    brand: $(this).find('[name="brand"]').val(),
                                    part: $(this).find('[name="part"]').val(),
                                },
                        return: ''
                    };
        
        $.post('<?php echo base_url('admin/maintenance/modelTable') ?>', data , function(data, textStatus, xhr) { 
            get_car_logs($('#formAddLogs').find('[name="car_id"]').val());
        });
    });


    get_car_logs();
    function get_car_logs() {
        $('#formAddLogs')[0].reset();
        var data =  {
                        table: 'tbl_car_logs',
                        action: 'get',
                        select: {
                                    
                                },
                        where:  {
                                    
                                },
                        field: 'date',
                        order: 'desc',
                        limit: 0,
                        offset: 0,
                        group_by: ''
        
                    };
        table_car_logs.clear().draw();
        $.post('<?php echo base_url('admin/maintenance/modelTable') ?>', data, function(data, textStatus, xhr) {
            // console.log(data);
            data = JSON.parse(data);
            $.each(data, function(index, val) {
                var btn_delete_log = '<div class="text-center"><button class="btn btn-danger btn_delete_log btn-sm" car_id="'+val.car_id+'" log_id="'+val.log_id+'"><i class="fa fa-trash"></i> </button></div>';
                 table_car_logs.row.add([moment(val.date).format('LL'),val.changes,val.brand,val.part]);
                 table_car_logs.draw(false);
            });
        });     
    }

    $('.table_car_logs').on('click', '.btn_delete_log', function(event) {
        event.preventDefault();
        var log_id = $(this).attr('log_id');
        var car_id = $(this).attr('car_id');
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                var data =  {
                                table: 'tbl_car_logs',
                                pk: log_id,
                                action: 'delete'
                            }; 
                $.post('<?php echo base_url('admin/maintenance/modelTable') ?>', data, function(data, textStatus, xhr) { 
                    get_car_logs(car_id);
                });
            }
        })
    });

});
</script>


<?php
$this->load->view('admin/includes/footer.php');
?>
