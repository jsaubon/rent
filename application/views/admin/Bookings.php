<?php
$this->load->view('admin/includes/header.php');
$this->load->view('admin/includes/nav-left.php');
$this->load->view('admin/includes/nav-top.php');
?>

<link rel="stylesheet" href="<?php echo base_url('assets/admin_template/vendor/clockpicker/clockpicker.min.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/admin_template/vendor/plugins/jqueryui/jquery-ui.min.css') ?>">
<script src="<?php echo base_url('assets/admin_template/vendor/clockpicker/clockpicker.min.js') ?>"></script>
<script src="<?php echo base_url('assets/admin_template/vendor/plugins/jqueryui/jquery-ui.min.js') ?>"></script>
<style>
    .table-loader {
        background-color: rgba(255,255,255,.7);
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
    }

    .dataTables_length {
        float: right;;
    }

    .dataTables_filter {
        float: left;
    }

    .control-label {
        margin-bottom: 0px;
    }

    .clockpicker-popover {
        z-index: 9999;
    }

    .ui-autocomplete {
        z-index: 9999999;
    }

</style>
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div id="listSection" class="row fade show">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-loader" style="display: none;" >
                                    <svg class="circular" viewBox="25 25 50 50">
                                        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
                                    </svg>
                                </div>
                                    <h4 class="card-title">
                                        <?php echo $this->input->get('status'); ?> Bookings
                                        <!-- <a href="#" class="" id="btnNewUser" data-toggle="modal" data-target="#add-edit-modal" ><small><i class="fas fa-plus-circle"></i> Add New</small></a> -->
                                    </h4>

                                    <div class="table-responsive" >
                                        
                                        <table id="pageTable_bookings" class="table stylish-table">
                                            <thead>
                                                <tr>
                                                    <th>Status</th>
                                                    <th>Car</th>
                                                    <th>Rate</th>
                                                    <th>Client</th>
                                                    <th>Transaction</th>
                                                    <th>Date Start</th>
                                                    <th>Date End</th>
                                                    <th>Note</th>
                                                    <?php if ($this->input->get('status') != 'Approved'): ?>
                                                        <th >Actions</th>
                                                    <?php endif ?>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->





<!-- sample modal content -->
<div id="add-edit-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                Booking Information
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data" id="formAddClient" method="post" action="return false" class="floating-labels">
                    <div class="row">
                        <div class="col-12 col-md-6  m-t-40">
                            <div class="card">
                                <div class="card-body">
                                    <input type="hidden" name="client_booking_id">
                                    <input type="hidden" name="client_client_id">
                                    <h4 style="margin-bottom: 17px">Client Information</h4>

                                    <div class="form-group">
                                        <label for="client_name" class="control-label">Client Name</label>
                                        <input type="text" required name="client_client_name" class="form-control stronly">
                                    </div>

                                    <script>
                                        $(document).ready(function() {
                                            get_clients();
                                            function get_clients() {
                                                var data =  {
                                                                table: 'tbl_clients',
                                                                action: 'get',
                                                                select: {
                                                                            
                                                                        },
                                                                where:  {
                                                
                                                                        },
                                                                field: '',
                                                                order: '',
                                                                limit: 0,
                                                                offset: 0,
                                                                group_by: 'client_id'
                                                
                                                            };
                                                $.post('<?php echo base_url('admin/bookings/modelTable') ?>', data, function(data, textStatus, xhr) {
                                                    data = JSON.parse(data);
                                                    console.log(data);
                                                    var clients = [];
                                                    $.each(data, function(index, val) {
                                                        clients.push({
                                                            value: val.client_email,
                                                            label: val.client_name,
                                                            desc: val.client_phone,
                                                            id: val.client_id,
                                                        });
                                                    });
                                                    
                                                    $( "[name='client_client_name']" ).autocomplete({
                                                      minLength: 0,
                                                      source: clients,
                                                      focus: function( event, ui ) {
                                                        $( "[name='client_client_name']" ).val( ui.item.label );
                                                        return false;
                                                      },
                                                      select: function( event, ui ) {
                                                        // ui.item.value
                                                        $( "[name='client_client_id']" ).val( ui.item.id );
                                                        $( "[name='client_client_name']" ).val( ui.item.label );
                                                        $( "[name='client_client_email']" ).val( ui.item.value );
                                                        $( "[name='client_client_phone']" ).val( ui.item.desc );
                                                        return false;
                                                      }
                                                    })
                                                    .autocomplete( "instance" )._renderItem = function( ul, item ) {
                                                      return $( "<li>" )
                                                        .append( "<div>" + item.label + " : " + item.value + "</div>" )
                                                        .appendTo( ul );
                                                    };
                                                });
                                            }
                                        });
                                            
                                                
                                      
                                    </script>
                                    <div class="form-group">
                                        <label for="client_email" class="control-label">Client Email</label>
                                        <input type="email" required name="client_client_email" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="client_phone" class="control-label">Client Phone</label>
                                        <input type="text" required name="client_client_phone" class="form-control numonly">
                                    </div>
                                    

                                </div>
                                    
                            </div>
                        </div>
                        <div class="col-12 col-md-6  m-t-40">
                            <div class="card">
                                <div class="card-body">
                                    <h4 style="margin-bottom: 17px">Transaction</h4>

                                    <div class="form-group">
                                        <label for="transaction_type" class="control-label">Transaction Type</label>
                                        <select required name="client_transaction_type" class="form-control">
                                            <option value="">Select Type</option>
                                            <option value="Self Drive">Self Drive</option>
                                            <option value="Pick Up and Drop Off">Pick Up and Drop Off</option>
                                            <option value="Chauffeur Driven">Chauffeur Driven</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="car_id" class="control-label">Car</label>
                                        <select required name="client_car_id" class="form-control">
                                            <option value="">Select Car</option>
                                            <?php foreach ($cars as $key => $car): ?>
                                                <option rates="<?php echo $car['rate'] ?>" value="<?php echo $car['car_id'] ?>"><?php echo $car['brand'].' '.$car['model'] ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="car_rate" class="control-label">Rate</label>
                                        <select  name="client_car_rate_select" class="form-control">
                                            
                                        </select>
                                        <input type="text" class="form-control" name="client_car_rate"> 
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <label for="date_start" class="control-label">Date Start</label>
                                            <input type="date" required name="client_date_start" class="form-control">
                                        </div>
                                        <div class="col-6 clockpicker">
                                            <label for="time_start" class="control-label">Time Start</label>
                                            <input type="text" required name="client_time_start" class="form-control " value="06:00AM">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <label for="date_end" class="control-label">Date End</label>
                                            <input type="date" required name="client_date_end" class="form-control">
                                        </div>
                                        <div class="col-6 clockpicker">
                                            <label for="time_end" class="control-label">Time End</label>
                                            <input type="text" required name="client_time_end" class="form-control " value="06:00PM">
                                        </div>
                                    </div>

                                    
                                    
                                    
                                </div>
                                    
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group mt-10">
                                <label for="transaction_status" class="control-label">Status</label>
                                <select required name="client_transaction_status" class="form-control">
                                    <?php foreach ($transaction_statuses as $key => $value): ?>
                                        <option value="<?php echo $value['transaction_status'] ?>"><?php echo $value['transaction_status'] ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <label for="date_end" class="control-label">Invoice Number</label>
                            <input type="text"  name="client_invoice_number" class="form-control">
                        </div>
                        <div class="col-12 col-md-4">
                            <label for="date_end" class="control-label">Date of Payment</label>
                            <input type="date"  name="client_date_of_payment" class="form-control">
                        </div>
                        <div class="col-12 col-md-4">
                            <label for="date_end" class="control-label">Amount Paid</label>
                            <input type="number"  name="client_amount_paid" class="form-control">
                        </div>
                        <div class="col-12 col-md-6">
                            <label for="date_end" class="control-label">Remitted By</label>
                                <select  name="client_remitted_by" class="form-control">
                                    <option value="">Select Remitted By</option>
                                    <?php foreach ($users as $key => $value): ?>
                                        <option value="<?php echo $value['admin_id'] ?>"><?php echo $value['name'] ?></option>
                                    <?php endforeach ?>
                                </select>
                        </div>
                        <div class="col-12 col-md-6">
                            <label for="date_end" class="control-label">Received By</label>
                                <select  name="client_received_by" class="form-control">
                                    <option value="">Select Received By</option>
                                    <?php foreach ($users as $key => $value): ?>
                                        <option value="<?php echo $value['admin_id'] ?>"><?php echo $value['name'] ?></option>
                                    <?php endforeach ?>
                                </select>
                        </div>
                    </div>

                    <div class="form-group mt-3">
                        <label for="client_notes">Additional Note</label>
                        <textarea name="client_client_note" class="form-control" cols="15" rows="5"></textarea>
                    </div>

                    <div class="text-center">
                        <button type="submit" class="btn btn-success waves-effect waves-light btn_add_edit_client">Save changes</button>
                    </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
 <script>
$(document).ready(function() {
    $('[data-target="#nav_bookings"]').click();
    $('.clockpicker').clockpicker({
        placement: 'right',
        align: 'right',
        autoclose: true,
        twelvehour: true 
    });
    
    var pageTable_bookings;
    var pageTable_bookings_data =    [
                                {name: '<?php echo $this->security->get_csrf_token_name(); ?>' , value: '<?php echo $this->security->get_csrf_hash(); ?>'},
                                {name: 'get_status' , value: '<?php echo $get_status; ?>'}
                            ];
    pageTable_bookings = $('#pageTable_bookings').DataTable({
        language: {
            search: '',
            searchPlaceholder: 'Search'
        },
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 50,
        dom: 'Rlfrtip',
        // "aoColumnDefs": [{ "asSorting": [ "desc", "asc" ], "aTargets": [ 8 ] }],
        bProcessing: true,
        bServerSide: true,
        sServerMethod: 'POST',
        sAjaxSource: '<?php echo base_url("admin/bookings/ajax_table") ?>',
        fnServerParams: function(aoData) {
            $.each(pageTable_bookings_data, function(i, field) {
                aoData.push({ name: field.name, value: field.value });
            });
            $('.client-dashboard-loader').fadeIn('50');
        },
        fnDrawCallback: function(data) {
            // console.log(data);
            $('.client-dashboard-loader').fadeOut('50');
        },createdRow: function( row, data, dataIndex ) {
            var select_status = $(row).find('.select_status');
            var current_value = $(row).find('.select_status').attr('current_value');;
            select_status.val(current_value);
        }
    });
 
    $('#formAddClient').on('submit', function(event) {
        event.preventDefault();
        var booking_id = $('#formAddClient').find('[name="client_booking_id"]').val();
        var client_id = $('#formAddClient').find('[name="client_client_id"]').val();
        var car_id = $('#formAddClient').find('[name="client_car_id"]').val();
        var car_rate = $('#formAddClient').find('[name="client_car_rate"]').val();
        var client_name = $('#formAddClient').find('[name="client_client_name"]').val();
        var client_email = $('#formAddClient').find('[name="client_client_email"]').val();
        var client_phone = $('#formAddClient').find('[name="client_client_phone"]').val();
        var client_note = $('#formAddClient').find('[name="client_client_note"]').val();
        var transaction_type = $('#formAddClient').find('[name="client_transaction_type"]').val();
        var date_start = $('#formAddClient').find('[name="client_date_start"]').val();
        var time_start = $('#formAddClient').find('[name="client_time_start"]').val();
        var date_end = $('#formAddClient').find('[name="client_date_end"]').val();
        var time_end = $('#formAddClient').find('[name="client_time_end"]').val();
        var transaction_status = $('#formAddClient').find('[name="client_transaction_status"]').val();
        var date_of_payment = $('#formAddClient').find('[name="client_date_of_payment"]').val();
        var invoice_number = $('#formAddClient').find('[name="client_invoice_number"]').val();
        var amount_paid = $('#formAddClient').find('[name="client_amount_paid"]').val();
        var remitted_by = $('#formAddClient').find('[name="client_remitted_by"]').val();
        var received_by = $('#formAddClient').find('[name="client_received_by"]').val();

        
        $.post('<?php echo base_url('admin/bookings/save_client') ?>', {booking_id,client_id,car_id,car_rate,client_name,client_email,client_phone,client_note,transaction_type,date_start,time_start,date_end,time_end,transaction_status,date_of_payment,invoice_number,amount_paid,remitted_by,received_by} , function(data, textStatus, xhr) { 
            $('#add-edit-modal').modal('hide');
            if (data == 'booking exist') {
                Swal.fire({
                    title: 'Booking Date is not Available!',
                    html: 'Please pick another date, thank you!',
                    icon: 'error',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Okay'
                }).then((result) => {
                    if (result.value) {
                       
                    }
                })
            } else {
                pageTable_bookings.ajax.reload();
            }
            
        });
    });


    $('#pageTable_bookings').on('click', '.btn_edit', function(event) {
        event.preventDefault();
        var booking_id = $(this).attr('booking_id');
        $.post('<?php echo base_url('admin/bookings/get_client_booking') ?>', {booking_id}, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            $.each(data, function(index, val) {
                $.each(val, function(k, v) {
                     $('#formAddClient').find('[name="client_'+k+'"]').val(v);
                });

                var date_start = moment(val.date_start).format('YYYY-MM-DD');
                var date_end = moment(val.date_end).format('YYYY-MM-DD');
                $('[name="client_date_start"]').val(date_start);
                $('[name="client_date_end"]').val(date_end);
                var date_of_payment = moment(val.date_of_payment).format('YYYY-MM-DD');
                $('[name="client_date_of_payment"]').val(date_of_payment);


                var time_start = moment(val.date_start).format('hh:mmA');
                var time_end = moment(val.date_end).format('hh:mmA');
                $('[name="client_time_start"]').val(time_start);
                $('[name="client_time_end"]').val(time_end);

                current_password = val.password;
            });

            $('[name="client_car_id"]').trigger('change');

            $('#add-edit-modal').modal('show');
        });
    });   

    $('#pageTable_bookings').on('click', '.btn_delete', function(event) {
         Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                var booking_id = $(this).attr('booking_id');
                var data =  {
                                '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
                                table: 'tbl_client_booking',
                                pk: booking_id,
                                action: 'delete'
                            }; 
                $.post('<?php echo base_url('admin/bookings/modelTable') ?>', data, function(data, textStatus, xhr) { 
                    pageTable_bookings.ajax.reload();
                });
            }
        })
                
    });

    $('[name="client_car_id"]').on('change', function(event) {
        event.preventDefault();
        var rates = $(this).find('option:selected').attr('rates');
        if (rates) {
            rates = rates.split('<br>');
            $('[name="client_car_rate_select"]').empty();
            $('[name="client_car_rate_select"]').append('<option>Select Rate or Input Manually Below</option>');
            $.each(rates, function(index, rate) {
                 $('[name="client_car_rate_select"]').append('<option value="'+rate+'">'+rate+'</option>');
            });
        }
            

    });

    $('[name="client_car_rate_select"]').on('change', function(event) {
        event.preventDefault();
        $('[name="client_car_rate"]').val($(this).val());
    });

    $('#pageTable_bookings').on('change', '.select_status', function(event) {
        event.preventDefault();
        var booking_id = $(this).attr('booking_id');
        var transaction_status = $(this).val();
        var data =  {
                        table: 'tbl_client_booking',
                        pk: booking_id,
                        action: 'save',
                        fields: {
                                    transaction_status:transaction_status
                                },
                        return: ''
                    };
        
        $.post('<?php echo base_url('admin/bookings/modelTable') ?>', data , function(data, textStatus, xhr) { 
            console.log(data);
            pageTable_bookings.ajax.reload();
            Swal.fire(
              'Status Successfully Updated to '+transaction_status,
              '',
              'success'
            )
        });
    }); 



});
</script>
<?php
$this->load->view('admin/includes/footer.php');
?>
