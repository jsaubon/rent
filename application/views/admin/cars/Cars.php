<?php
$this->load->view('admin/includes/header.php');
$this->load->view('admin/includes/nav-left.php');
$this->load->view('admin/includes/nav-top.php');
?>

<style>
    .table-loader {
        background-color: rgba(255,255,255,.7);
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
    }

    .dataTables_length {
        float: right;;
    }

    .dataTables_filter {
        float: left;
    }

    .control-label {
        margin-bottom: 0px;
    }

    .table {
        width: 100% !important;
    }
</style>
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div id="listSection" class="row fade show">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-loader" style="display: none;" >
                                    <svg class="circular" viewBox="25 25 50 50">
                                        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
                                    </svg>
                                </div>
                                    <h4 class="card-title">
                                        <?php if($this->input->get('active') == 0) { echo 'Under Maintenance' ;} else {echo 'Active Cars';} ?>
                                        <a href="#" class="" id="btn-new-form" data-toggle="modal" data-target="#add-edit-modal" ><small><i class="fas fa-plus-circle"></i> Add New</small></a>
                                    </h4>
                                    <!-- <div class="row container-cars">
                                        <div class="col-3">
                                            <div class="card">
                                                <div class="card-body ">
                                                    <img src="http://butuanwheels.csjsolutions.net/assets/uploads/1579656891.jpg" alt="" width="100%">
                                                    <h3 class="text-center" style="margin-top: 20px;">Toyota Vios</h3>
                                                    <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis voluptate, optio minus quisquam eligendi adipisci quas voluptates delectus nam praesentium nostrum facilis dicta quia repellat quaerat magni, voluptas deleniti saepe!
                                                    <br>
                                                    IMEI: <span></span>
                                                    <br>
                                                    Capacity: <span></span>
                                                    <br>
                                                    Transition: <span></span>
                                                    <br>
                                                    Rates: <br>
                                                    <span></span>
                                                    <br>
                                                    Status: <span></span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                                    <div class="table-responsive" >
                                        
                                        <table id="pageTable_car_list" class="table stylish-table">
                                            <thead>
                                                <tr>
                                                    <th style="width: 1%">Active</th>
                                                    <th style="width: 1%">Image</th>
                                                    <th style="width: 1%">Brand</th>
                                                    <th style="width: 1%">Model</th>
                                                    <th style="width: 1%">IMEI</th>
                                                    <th style="width: 1%">Capacity</th>
                                                    <th style="width: 1%">Transition</th>
                                                    <th>Description</th>
                                                    <th style="width: 15%">Rate</th>
                                                    <th style="width: 1%">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->





<!-- sample modal content -->
<div id="add-edit-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                Car Information
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data" id="formAddCar" method="post" action="<?php echo base_url('admin/cars/cars/save_detail') ?>" class="floating-labels">
                    <input type="hidden" name="car_car_id">
                    
                    <div class="row">
                        <div class="col-12 col-md-6 m-t-40">
                            <div class="card">
                                <div class="card-body">
                                    <h4 style="margin-bottom: 17px">Basic Information</h4>
                                    <div class="form-group">
                                        <label for="image">Image</label>
                                        <input type="file"  name="car_new_image" class="form-control">
                                        <input type="hidden"  name="car_image" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="brand">Brand</label>
                                        <select name="car_brand_id" class="form-control" required>
                                            <option value="">Select Brand</option>
                                            <?php foreach ($brands as $key => $value): ?>
                                                <option value="<?php echo $value['brand_id'] ?>"><?php echo $value['brand'] ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="model">Model</label>
                                        <select name="car_model_id" class="form-control" required>
                                        </select>

                                    </div>
                                    <div class="form-group">
                                        <label for="capacity">Capacity</label>
                                        <input type="text" required name="car_capacity" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="capacity">IMEI</label>
                                        <input type="text"  name="car_imei" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="transmission">Transmission</label>
                                        <select required name="car_transmission" class="form-control">
                                            <option value="">Select Transmission</option>
                                            <option value="Automatic">Automatic</option>
                                            <option value="Manual">Manual</option>

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea name="car_description" class="form-control" cols="30" rows="10"></textarea>
                                    </div>

                                </div>
                                    
                            </div>
                        </div>

                        <div class="col-12 col-md-6 m-t-40">
                            <div class="card">
                                <div class="card-body">
                                    <h4 style="margin-bottom: 17px">Rate </h4>
                                    <div class="rate_container">

                                        <div class="form-group row">
                                            <input type="hidden" name="rate_rate_id[]">
                                            <div class="col-sm-6">
                                                <label for="rate_coverage">Coverage</label>
                                                <input type="text" name="rate_coverage[]" placeholder="ex. Butuan City Only" required  class="form-control">
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="rate_rate">Rate per Day</label>
                                                <input type="number" name="rate_rate[]" required  class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-success btn-block btn-add-rate"><i class="fas fa-plus"></i></button>
                                </div>
                                    
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row" style="margin-top: 20px;">
                        <div class="col-12 col-md-4 offset-md-4">
                            
                            <div class="form-group">
                                <label for="car_active">Status</label>
                                <select required name="car_active" class="form-control">
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>

                                </select>
                            </div>
                            <button type="submit" class="btn btn-success waves-effect waves-light btn-block ">Save changes</button>
                        </div>
                    </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- sample modal content -->
<div id="logs-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                Car Maintenance
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <form id="formAddLogs" action="return false" method="POST">
                    <div class="row" >
                        <input type="hidden" name="log_id">
                        <input type="hidden" name="car_id">
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="date">Date</label>
                                <input type="date" class="form-control" name="date">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="changes">Changes</label>
                                <input type="text" class="form-control" name="changes">
                            </div>
                            
                        </div>
                        <div class="col-12 col-md-2">
                            <div class="form-group">
                                <label for="brand">Brand</label>
                                <input type="text" class="form-control" name="brand">
                            </div>
                            
                        </div>
                        <div class="col-12 col-md-2">
                            <div class="form-group">
                                <label for="part">Part</label>
                                <input type="text" class="form-control" name="part">
                            </div>
                            
                        </div>
                        <div class="col-12 col-md-2">
                            <div class="form-group">
                                <label for="" style="color: white">Save</label>
                                <button type="submit" class="btn btn-success waves-effect waves-light btn-block ">Save</button>
                            </div>
                                
                        </div>
                    </div>
                </form>

                <div class="row">
                    <div class="col-12">
                        <table class="table table-stripped table_car_logs">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Changes</th>
                                    <th>Brand</th>
                                    <th>Part</th>
                                    <th style="width: 1px !important">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
 <script>
$(document).ready(function() {
    $('[data-target="#car_inventory"]').click();
    
    var pageTable_car_list;
    var pageTable_car_list_data =    [
                                {name: 'active' , value: '<?php echo $car_active; ?>'}
                            ];
    pageTable_car_list = $('#pageTable_car_list').DataTable({
        language: {
            search: '',
            searchPlaceholder: 'Search'
        },
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 20,
        dom: 'Rlfrtip',
        // "aoColumnDefs": [{ "asSorting": [ "desc", "asc" ], "aTargets": [ 8 ] }],
        bProcessing: true,
        bServerSide: true,
        sServerMethod: 'POST',
        sAjaxSource: '<?php echo base_url("admin/cars/cars/ajax_table") ?>',
        fnServerParams: function(aoData) {
            $.each(pageTable_car_list_data, function(i, field) {
                aoData.push({ name: field.name, value: field.value });
            });
            $('.client-dashboard-loader').fadeIn('50');
        },
        fnDrawCallback: function(data) {
            // console.log(data);
            $('.client-dashboard-loader').fadeOut('50');
        },createdRow: function( row, data, dataIndex ) {
            


        }
    });

    var table_car_logs = $('.table_car_logs').DataTable({
        language: {
            search: '',
            searchPlaceholder: 'Search'
        },
        'aaSorting': [],
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
    });

    // var current_password = '';
    // $('#formAddCar').on('submit', function(event) {
    //     event.preventDefault();
    //     var car_id = $('#formAddCar').find('[name="car_car_id"]').val();
    //     var image = $('#formAddCar').find('[name="car_image"]').val();
    //     var new_image = $('#formAddCar').find('[name="car_new_image"]').val();
    //     var brand_id = $('#formAddCar').find('[name="car_brand_id"]').val();
    //     var model_id = $('#formAddCar').find('[name="car_model_id"]').val();
    //     var price = $('#formAddCar').find('[name="car_price"]').val();
    //     var capacity = $('#formAddCar').find('[name="car_capacity"]').val();
    //     var transmission = $('#formAddCar').find('[name="car_transmission"]').val();
    //     var description = $('#formAddCar').find('[name="car_description"]').val();


        
    //     $.post('<?php echo base_url('admin/cars/cars/save_detail') ?>', {'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',car_id,new_image,image,brand_id,model_id,price,capacity,transmission,description} , function(data, textStatus, xhr) { 
    //         $('#add-edit-modal').modal('hide');
    //         pageTable_car_list.ajax.reload();
    //     });
    // });


    $('#pageTable_car_list').on('click', '.btn_edit', function(event) {
        event.preventDefault();
        var car_id = $(this).attr('car_id');
        var data =  {
                        '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
                        table: 'tbl_car',
                        action: 'get',
                        select: {
                                    
                                },
                        where:  {
                                    car_id:car_id
                                },
                        field: '',
                        order: '',
                        limit: 0,
                        offset: 0,
                        group_by: ''
        
                    };
        resetForm();
        $.post('<?php echo base_url('admin/cars/cars/modelTable') ?>', data, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            $.each(data, function(index, val) {
                $.each(val, function(k, v) {
                    $('#formAddCar').find('[name="car_'+k+'"]').val(v);
                });

                $('[name="car_brand_id"]').attr('car_model_id', val.model_id);

                get_car_rates(val.car_id);
            });
            $('[name="car_brand_id"]').trigger('change');


            $('#add-edit-modal').modal('show');
        });
    });   


    function get_car_rates(car_id) {
        var data =  {
                        table: 'tbl_car_rate',
                        action: 'get',
                        select: {
                                    
                                },
                        where:  {
                                    car_id:car_id
                                },
                        field: '',
                        order: '',
                        limit: 0,
                        offset: 0,
                        group_by: ''
        
                    };
        $.post('<?php echo base_url('admin/cars/cars/modelTable') ?>', data, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            $('.rate_container').empty();
            $.each(data, function(index, val) {
                var new_row = '<div class="form-group row rate_extra">\
                            <input value="'+val.rate_id+'" name="rate_rate_id[]" type="hidden"/>\
                            <div class="col-sm-6">\
                                <label for="rate_coverage">Coverage</label>\
                                <input value="'+val.coverage+'" type="text" name="rate_coverage[]" placeholder="ex. Butuan City Only" required  class="form-control">\
                            </div>\
                            <div class="col-sm-6">\
                                <i class="fa fa-minus-circle text-danger btn-remove-rate" style="float:right;cursor:pointer"></i>\
                                <label for="rate_rate">Rate per Day </label>\
                                <input value="'+val.rate+'" type="number" name="rate_rate[]" required  class="form-control">\
                            </div>\
                        </div>';
                $('.rate_container').append(new_row);
            });
        });
    }

    $('#pageTable_car_list').on('click', '.btn_delete', function(event) {
        var car_id = $(this).attr('car_id');
        Swal.fire({
            title: 'Are you sure?',
            text: "This car will be transferred to Under Maintenance!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Transfer this!'
        }).then((result) => {
            if (result.value) {
                var data =  {
                                table: 'tbl_car',
                                pk: car_id,
                                action: 'save',
                                fields: {
                                            active: 0
                                        },
                                return: ''
                            };
                
                $.post('<?php echo base_url('admin/cars/cars/modelTable') ?>', data , function(data, textStatus, xhr) { 
                    pageTable_car_list.ajax.reload();
                });
                
            }
        })
                
    });

    $('#btn-new-form').on('click', function(event) {
        event.preventDefault();
        resetForm();
    });

    function resetForm() {

        $('#formAddCar')[0].reset();
        $('[name="car_car_id"]').val('');
        $('[name="car_model_id"]').empty();
        $('.rate_extra').remove();
    }

    $('[name="car_brand_id"]').on('change', function(event) {
        event.preventDefault();
        var brand_id = $(this).val();
        var data =  {
                        '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
                        table: 'tbl_car_model',
                        action: 'get',
                        select: {
                                    
                                },
                        where:  {
                                    'brand_id': brand_id
                                },
                        field: '',
                        order: '',
                        limit: 0,
                        offset: 0,
                        group_by: ''
        
                    };
        $('[name="car_model_id"]').empty();
        $.post('<?php echo base_url('admin/cars/cars/modelTable') ?>', data, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            console.log(data);
            $.each(data, function(index, val) {
                var new_opt = '<option value="'+val.model_id+'">'+val.model+'</option>';
                $('[name="car_model_id"]').append(new_opt);
            });
            var model_id = $('[name="car_brand_id"]').attr('car_model_id');
            $('[name="car_model_id"]').val(model_id);
        });
    }); 

    $('.btn-add-rate').on('click', function(event) {
        event.preventDefault();
        var new_row = '<div class="form-group row rate_extra">\
                            <input name="rate_rate_id" type="hidden"/>\
                            <div class="col-sm-6">\
                                <label for="rate_coverage">Coverage</label>\
                                <input type="text" name="rate_coverage[]" placeholder="ex. Butuan City Only" required  class="form-control">\
                            </div>\
                            <div class="col-sm-6">\
                                <i class="fa fa-minus-circle text-danger btn-remove-rate" style="float:right;cursor:pointer"></i>\
                                <label for="rate_rate">Rate per Day </label>\
                                <input type="number" name="rate_rate[]" required  class="form-control">\
                            </div>\
                        </div>';
        $('.rate_container').append(new_row);
    });

    $('.rate_container').on('click', '.btn-remove-rate', function(event) {
        event.preventDefault();
        $(this).closest('.row').remove();
    });

    $('#pageTable_car_list').on('click', '.btn_logs' , function(event) {
        event.preventDefault();
        var car_id = $(this).attr('car_id');
        $('[name="car_id"]').val(car_id);
        get_car_logs(car_id);
        $('#logs-modal').modal('show');
    });

    $('#formAddLogs').on('submit', function(event) {
        event.preventDefault();
        var data =  {
                        table: 'tbl_car_logs',
                        pk: $(this).find('[name="log_id"]').val() == '' ? '' : $(this).find('[name="log_id"]').val(),
                        action: 'save',
                        fields: {
                                    car_id: $(this).find('[name="car_id"]').val(),
                                    date: $(this).find('[name="date"]').val(),
                                    changes: $(this).find('[name="changes"]').val(),
                                    brand: $(this).find('[name="brand"]').val(),
                                    part: $(this).find('[name="part"]').val(),
                                },
                        return: ''
                    };
        
        $.post('<?php echo base_url('admin/cars/cars/modelTable') ?>', data , function(data, textStatus, xhr) { 
            get_car_logs($('#formAddLogs').find('[name="car_id"]').val());
        });
    });

    function get_car_logs(car_id) {
        $('#formAddLogs')[0].reset();
        var data =  {
                        table: 'tbl_car_logs',
                        action: 'get',
                        select: {
                                    
                                },
                        where:  {
                                    car_id:car_id
                                },
                        field: 'date',
                        order: 'desc',
                        limit: 0,
                        offset: 0,
                        group_by: ''
        
                    };
        table_car_logs.clear().draw();
        $.post('<?php echo base_url('admin/cars/cars/modelTable') ?>', data, function(data, textStatus, xhr) {
            // console.log(data);
            data = JSON.parse(data);
            $.each(data, function(index, val) {
                var btn_delete_log = '<div class="text-center"><button class="btn btn-danger btn_delete_log btn-sm" car_id="'+car_id+'" log_id="'+val.log_id+'"><i class="fa fa-trash"></i> </button></div>';
                 table_car_logs.row.add([moment(val.date).format('LL'),val.changes,val.brand,val.part,btn_delete_log]);
                 table_car_logs.draw(false);
            });
        });     
    }

    $('.table_car_logs').on('click', '.btn_delete_log', function(event) {
        event.preventDefault();
        var log_id = $(this).attr('log_id');
        var car_id = $(this).attr('car_id');
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                var data =  {
                                table: 'tbl_car_logs',
                                pk: log_id,
                                action: 'delete'
                            }; 
                $.post('<?php echo base_url('admin/cars/cars/modelTable') ?>', data, function(data, textStatus, xhr) { 
                    get_car_logs(car_id);
                });
            }
        })
    });

});
</script>
<?php
$this->load->view('admin/includes/footer.php');
?>
