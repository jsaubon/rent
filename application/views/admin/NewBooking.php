<?php
$this->load->view('admin/includes/header.php');
$this->load->view('admin/includes/nav-left.php');
$this->load->view('admin/includes/nav-top.php');
?>
<style>
    #map {
        height: 100%;
    }

    .table {
        font-size: 11px;
    }
    .table select{
        font-size: 11px;
    }
</style>
<link rel="stylesheet" href="<?php echo base_url('assets/admin_template/vendor/clockpicker/clockpicker.min.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/admin_template/vendor/plugins/jqueryui/jquery-ui.min.css') ?>">
<script src="<?php echo base_url('assets/admin_template/vendor/clockpicker/clockpicker.min.js') ?>"></script>
<script src="<?php echo base_url('assets/admin_template/vendor/plugins/jqueryui/jquery-ui.min.js') ?>"></script>
<!-- Begin Page Content -->
<div class="container-fluid">
    <div class="row">
        
        <div class="col-12 col-md-12">
            <div class="card">
               <div class="card-body">
                <h3>New Booking</h3>
                   <form enctype="multipart/form-data" id="formAddClient" method="post" action="return false" class="floating-labels">
                        <div class="row">
                            <div class="col-12 col-md-6  m-t-40">
                                <div class="card">
                                    <div class="card-body">
                                        <input type="hidden" name="client_booking_id">
                                        <input type="hidden" name="client_client_id">
                                        <h4 style="margin-bottom: 17px">Client Information</h4>

                                        <div class="form-group">
                                            <label for="client_name" class="control-label">Client Name</label>
                                            <input type="text" required name="client_client_name" class="form-control stronly">
                                        </div>
 
                                        <div class="form-group">
                                            <label for="client_email" class="control-label">Client Email</label>
                                            <input type="email" required name="client_client_email" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="client_phone" class="control-label">Client Phone</label>
                                            <input type="text" required name="client_client_phone" class="form-control numonly">
                                        </div>
                                        

                                    </div>
                                        
                                </div>
                            </div>
                            <div class="col-12 col-md-6  m-t-40">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 style="margin-bottom: 17px">Transaction</h4>

                                        <div class="form-group">
                                            <label for="transaction_type" class="control-label">Transaction Type</label>
                                            <select required name="client_transaction_type" class="form-control">
                                                <option value="">Select Type</option>
                                                <option value="Self Drive">Self Drive</option>
                                                <option value="Pick Up and Drop Off">Pick Up and Drop Off</option>
                                                <option value="Chauffeur Driven">Chauffeur Driven</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="car_id" class="control-label">Car</label>
                                            <select required name="client_car_id" class="form-control">
                                                <option value="">Select Car</option>
                                                <?php foreach ($cars as $key => $car): ?>
                                                    <option rates="<?php echo $car['rate'] ?>" value="<?php echo $car['car_id'] ?>"><?php echo $car['brand'].' '.$car['model'] ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="car_rate" class="control-label">Rate</label>
                                            <select  name="client_car_rate_select" class="form-control">
                                                
                                            </select>
                                            <input type="text" class="form-control" name="client_car_rate"> 
                                        </div>
                                        <div class="row">
                                            <div class="col-6">
                                                <label for="date_start" class="control-label">Date Start</label>
                                                <input type="date" required name="client_date_start" class="form-control">
                                            </div>
                                            <div class="col-6 clockpicker">
                                                <label for="time_start" class="control-label">Time Start</label>
                                                <input type="text" required name="client_time_start" class="form-control " value="06:00AM">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">
                                                <label for="date_end" class="control-label">Date End</label>
                                                <input type="date" required name="client_date_end" class="form-control">
                                            </div>
                                            <div class="col-6 clockpicker">
                                                <label for="time_end" class="control-label">Time End</label>
                                                <input type="text" required name="client_time_end" class="form-control " value="06:00PM">
                                            </div>
                                        </div>

                                        
                                        
                                        
                                    </div>
                                        
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group mt-10">
                                    <label for="transaction_status" class="control-label">Status</label>
                                    <select required name="client_transaction_status" class="form-control">
                                        <?php foreach ($transaction_statuses as $key => $value): ?>
                                            <option value="<?php echo $value['transaction_status'] ?>"><?php echo $value['transaction_status'] ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <label for="date_end" class="control-label">Invoice Number</label>
                                <input type="text"  name="client_invoice_number" class="form-control">
                            </div>
                            <div class="col-12 col-md-4">
                                <label for="date_end" class="control-label">Date of Payment</label>
                                <input type="date"  name="client_date_of_payment" class="form-control">
                            </div>
                            <div class="col-12 col-md-4">
                                <label for="date_end" class="control-label">Amount Paid</label>
                                <input type="number"  name="client_amount_paid" class="form-control">
                            </div>
                            <div class="col-12 col-md-6">
                                <label for="date_end" class="control-label">Remitted By</label>
                                    <select required name="client_remitted_by" class="form-control">
                                        <option value="">Select Remitted By</option>
                                        <?php foreach ($users as $key => $value): ?>
                                            <option value="<?php echo $value['admin_id'] ?>"><?php echo $value['name'] ?></option>
                                        <?php endforeach ?>
                                    </select>
                            </div>
                            <div class="col-12 col-md-6">
                                <label for="date_end" class="control-label">Received By</label>
                                    <select required name="client_received_by" class="form-control">
                                        <option value="">Select Received By</option>
                                        <?php foreach ($users as $key => $value): ?>
                                            <option value="<?php echo $value['admin_id'] ?>"><?php echo $value['name'] ?></option>
                                        <?php endforeach ?>
                                    </select>
                            </div>
                        </div>

                        <div class="form-group mt-3">
                            <label for="client_notes">Additional Note</label>
                            <textarea name="client_client_note" class="form-control" cols="15" rows="5"></textarea>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-success waves-effect waves-light btn_add_edit_client">Submit</button>
                        </div>


               </div>
            </div>
        </div>
        
        
    </div>
    
        

</div>
<!-- /.container-fluid -->

 <script>
$(document).ready(function() {

    $('[data-target="#nav_bookings"]').click();
    $('.clockpicker').clockpicker({
        placement: 'top',
        align: 'top',
        autoclose: true,
        twelvehour: true 
    });
    get_clients();
    function get_clients() {
        var data =  {
                        table: 'tbl_clients',
                        action: 'get',
                        select: {
                                    
                                },
                        where:  {
        
                                },
                        field: '',
                        order: '',
                        limit: 0,
                        offset: 0,
                        group_by: 'client_id'
        
                    };
        $.post('<?php echo base_url('admin/bookings/modelTable') ?>', data, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            console.log(data);
            var clients = [];
            $.each(data, function(index, val) {
                clients.push({
                    value: val.client_email,
                    label: val.client_name,
                    desc: val.client_phone,
                    id: val.client_id,
                });
            });
            
            $( "[name='client_client_name']" ).autocomplete({
              minLength: 0,
              source: clients,
              focus: function( event, ui ) {
                $( "[name='client_client_name']" ).val( ui.item.label );
                return false;
              },
              select: function( event, ui ) {
                // ui.item.value
                $( "[name='client_client_id']" ).val( ui.item.id );
                $( "[name='client_client_name']" ).val( ui.item.label );
                $( "[name='client_client_email']" ).val( ui.item.value );
                $( "[name='client_client_phone']" ).val( ui.item.desc );
                return false;
              }
            })
            .autocomplete( "instance" )._renderItem = function( ul, item ) {
              return $( "<li>" )
                .append( "<div>" + item.label + " : " + item.value + "</div>" )
                .appendTo( ul );
            };
        });
    }

    $('[name="client_car_id"]').on('change', function(event) {
        event.preventDefault();
        var rates = $(this).find('option:selected').attr('rates');
        if (rates) {
            rates = rates.split('<br>');
            $('[name="client_car_rate_select"]').empty();
            $('[name="client_car_rate_select"]').append('<option>Select Rate or Input Manually Below</option>');
            $.each(rates, function(index, rate) {
                 $('[name="client_car_rate_select"]').append('<option value="'+rate+'">'+rate+'</option>');
            });
        }
            

    });

    $('[name="client_car_rate_select"]').on('change', function(event) {
        event.preventDefault();
        $('[name="client_car_rate"]').val($(this).val());
    });

    $('#formAddClient').on('submit', function(event) {
        event.preventDefault();
        var booking_id = $('#formAddClient').find('[name="client_booking_id"]').val();
        var client_id = $('#formAddClient').find('[name="client_client_id"]').val();
        var car_id = $('#formAddClient').find('[name="client_car_id"]').val();
        var car_rate = $('#formAddClient').find('[name="client_car_rate"]').val();
        var client_name = $('#formAddClient').find('[name="client_client_name"]').val();
        var client_email = $('#formAddClient').find('[name="client_client_email"]').val();
        var client_phone = $('#formAddClient').find('[name="client_client_phone"]').val();
        var client_note = $('#formAddClient').find('[name="client_client_note"]').val();
        var transaction_type = $('#formAddClient').find('[name="client_transaction_type"]').val();
        var date_start = $('#formAddClient').find('[name="client_date_start"]').val();
        var time_start = $('#formAddClient').find('[name="client_time_start"]').val();
        var date_end = $('#formAddClient').find('[name="client_date_end"]').val();
        var time_end = $('#formAddClient').find('[name="client_time_end"]').val();
        var transaction_status = $('#formAddClient').find('[name="client_transaction_status"]').val();
        var date_of_payment = $('#formAddClient').find('[name="client_date_of_payment"]').val();
        var invoice_number = $('#formAddClient').find('[name="client_invoice_number"]').val();
        var amount_paid = $('#formAddClient').find('[name="client_amount_paid"]').val();
        var remitted_by = $('#formAddClient').find('[name="client_remitted_by"]').val();
        var received_by = $('#formAddClient').find('[name="client_received_by"]').val();

        
        $.post('<?php echo base_url('admin/bookings/save_client') ?>', {booking_id,client_id,car_id,car_rate,client_name,client_email,client_phone,client_note,transaction_type,date_start,time_start,date_end,time_end,transaction_status,date_of_payment,invoice_number,amount_paid,remitted_by,received_by} , function(data, textStatus, xhr) { 
            Swal.fire(
              'Success!',
              'New Booking Submitted!',
              'success'
            );

            $('#formAddClient')[0].reset();
            $('[name="client_car_rate_select"]').empty();
        });
    });
});
</script>


<?php
$this->load->view('admin/includes/footer.php');
?>
