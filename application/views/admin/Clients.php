<?php
$this->load->view('admin/includes/header.php');
$this->load->view('admin/includes/nav-left.php');
$this->load->view('admin/includes/nav-top.php');
?>

<style>
    .table-loader {
        background-color: rgba(255,255,255,.7);
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
    }

    .dataTables_length {
        float: right;;
    }

    .dataTables_filter {
        float: left;
    }

    .control-label {
        margin-bottom: 0px;
    }

</style>
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div id="listSection" class="row fade show">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-loader" style="display: none;" >
                                    <svg class="circular" viewBox="25 25 50 50">
                                        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
                                    </svg>
                                </div>
                                    <h4 class="card-title">
                                        Clients
                                        <a href="#" class="" id="btnNewUser" data-toggle="modal" data-target="#add-edit-modal" ><small><i class="fas fa-plus-circle"></i> Add New</small></a>
                                    </h4>

                                    <div class="table-responsive" >
                                        
                                        <table id="pageTable_clients" class="table stylish-table">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Phone</th>
                                                    <th>Date Registered</th>
                                                    <th style="width: 14%">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->





<!-- sample modal content -->
<div id="add-edit-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                Client Information
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data" id="formAddClient" method="post" action="return false" class="floating-labels">
                    <input type="hidden" name="client_id">
                    <input type="hidden" name="client_pass_reset" value="0">
                    
                    <div class="row">
                        <div class="col-12 col-md-6 offset-md-3 m-t-40">
                            <div class="card">
                                <div class="card-body">
                                    <input type="hidden" name="client_client_id">
                                    <h4 style="margin-bottom: 17px">User Account Information</h4>
                                    <div class="form-group">
                                        <label for="client_name">Name</label>
                                        <input type="text" class="form-control" name="client_client_name">
                                    </div>
                                    <div class="form-group">
                                        <label for="client_email">Email</label>
                                        <input type="email" class="form-control" name="client_client_email">
                                    </div>
                                    <div class="form-group">
                                        <label for="client_phone">Phone</label>
                                        <input type="text" class="form-control" name="client_client_phone">
                                    </div>
                                    <div class="form-group">
                                        <label for="client_password">Password</label>
                                        <input type="password" class="form-control" name="client_client_password">
                                    </div>
                                    <button type="submit" class="btn btn-success waves-effect waves-light btn-block btn_add_edit_admin">Save changes</button>
                                </div>
                                    
                            </div>
                        </div>
                    </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
 <script>
$(document).ready(function() {
    $('#nav_clients').addClass('active');
    
    var pageTable_clients;
    var pageTable_clients_data =    [
                                {name: '<?php echo $this->security->get_csrf_token_name(); ?>' , value: '<?php echo $this->security->get_csrf_hash(); ?>'}
                            ];
    pageTable_clients = $('#pageTable_clients').DataTable({
        language: {
            search: '',
            searchPlaceholder: 'Search'
        },
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 50,
        dom: 'Rlfrtip',
        // "aoColumnDefs": [{ "asSorting": [ "desc", "asc" ], "aTargets": [ 8 ] }],
        bProcessing: true,
        bServerSide: true,
        sServerMethod: 'POST',
        sAjaxSource: '<?php echo base_url("admin/clients/ajax_table") ?>',
        fnServerParams: function(aoData) {
            $.each(pageTable_clients_data, function(i, field) {
                aoData.push({ name: field.name, value: field.value });
            });
            $('.client-dashboard-loader').fadeIn('50');
        },
        fnDrawCallback: function(data) {
            // console.log(data);
            $('.client-dashboard-loader').fadeOut('50');
        },createdRow: function( row, data, dataIndex ) {
            


        }
    });

    var current_password = '';
    $('#formAddClient').on('submit', function(event) {
        event.preventDefault();
        var client_id = $('#formAddClient').find('[name="client_client_id"]').val();
        var client_name = $('#formAddClient').find('[name="client_client_name"]').val();
        var client_email = $('#formAddClient').find('[name="client_client_email"]').val();
        var client_phone = $('#formAddClient').find('[name="client_client_phone"]').val();
        var client_password = $('#formAddClient').find('[name="client_client_password"]').val();
        
        $.post('<?php echo base_url('admin/clients/save_client') ?>', {client_id,client_name,client_email,client_phone,client_password,current_password} , function(data, textStatus, xhr) { 
            $('#add-edit-modal').modal('hide');
            pageTable_clients.ajax.reload();
        });
    });


    $('#pageTable_clients').on('click', '.btn_edit', function(event) {
        event.preventDefault();
        var client_id = $(this).attr('client_id');
        var data =  {
                        '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
                        table: 'tbl_clients',
                        action: 'get',
                        select: {
                                    
                                },
                        where:  {
                                    client_id:client_id
                                },
                        field: '',
                        order: '',
                        limit: 0,
                        offset: 0,
                        group_by: ''
        
                    };
        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            $.each(data, function(index, val) {
                $.each(val, function(k, v) {
                     $('#formAddClient').find('[name="client_'+k+'"]').val(v);
                     setTimeout(function(){
                        $('#formAddClient').find('[name="client_'+k+'"]').focus();
                    },1000);
                });

                current_password = val.password;
            });

            $('#add-edit-modal').modal('show');
        });
    });   

    $('#pageTable_clients').on('click', '.btn_delete', function(event) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                var client_id = $(this).attr('client_id');
                var data =  {
                                '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
                                table: 'tbl_clients',
                                pk: client_id,
                                action: 'delete'
                            }; 
                $.post('<?php echo base_url('admin/clients/modelTable') ?>', data, function(data, textStatus, xhr) { 
                    pageTable_clients.ajax.reload();
                });
            }
        })
                
    });

    $('#btnNewUser').on('click', function(event) {
        event.preventDefault();
        $('#formAddClient')[0].reset();
        $('[name="client_client_id"]').val('');
    });


});
</script>
<?php
$this->load->view('admin/includes/footer.php');
?>
