<?php
$this->load->view('admin/includes/header.php');
$this->load->view('admin/includes/nav-left.php');
$this->load->view('admin/includes/nav-top.php');
?>

<style>
    .table-loader {
        background-color: rgba(255,255,255,.7);
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
    }

    .dataTables_length {
        float: right;;
    }

    .dataTables_filter {
        float: left;
    }

    .control-label {
        margin-bottom: 0px;
    }

</style>
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div id="listSection" class="row fade show">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-loader" style="display: none;" >
                                    <svg class="circular" viewBox="25 25 50 50">
                                        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
                                    </svg>
                                </div>
                                    <h4 class="card-title">
                                        Reviews
                                        <a href="#" class="" id="btn-new-form" data-toggle="modal" data-target="#add-edit-modal" ><small><i class="fas fa-plus-circle"></i> Add New</small></a>
                                    </h4>

                                    <div class="table-responsive" >
                                        
                                        <table id="pageTable_reviews" class="table stylish-table">
                                            <thead>
                                                <tr>
                                                    <th>Client</th>
                                                    <th>Car</th>
                                                    <th>Review</th>
                                                    <th>Star</th>
                                                    <th>Date</th>
                                                    <!-- <th style="width: 14%">Actions</th> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->





<!-- sample modal content -->
<div id="add-edit-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                Review Information
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data" id="formAddCarReviews" method="post" action="return false" class="floating-labels">
                    <input type="hidden" name="review_review_id">
                    
                    <div class="row">
                        <div class="col-12 col-md-6 offset-md-3 m-t-40">
                            <div class="card">
                                <div class="card-body">
                                    <h4 style="margin-bottom: 17px">Reviews Information</h4>

                                    <div class="form-group">
                                        <label for="description" class="control-label">Client</label>
                                        <select name="review_client_id" class="form-control">
                                            <option value="">Select Client</option>
                                            <?php foreach ($clients as $key => $value): ?>
                                                <option value="<?php echo $value['client_id'] ?>"><?php echo $value['client_name'] ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="description" class="control-label">Car</label>
                                        <select name="review_car_id" class="form-control">
                                            <option value="">Select Car</option>
                                            <?php foreach ($cars as $key => $value): ?>
                                                <option value="<?php echo $value['car_id'] ?>"><?php echo $value['brand'].' '.$value['model'] ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="amount" class="control-label">Review</label>
                                        <textarea name="review_review" class="form-control"  rows="5"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="requested_by" class="control-label">Star</label>
                                        <input type="number" name="review_star" max="5" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="date" class="control-label">Date</label>
                                        <input type="date" name="review_date" class="form-control">
                                    </div>

                                    <button type="submit" class="btn btn-success waves-effect waves-light btn-block ">Save changes</button>
                                </div>
                                    
                            </div>
                        </div>
                    </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
 <script>
$(document).ready(function() {
    $('#nav_car_reviews').addClass('active');
    
    var pageTable_reviews;
    var pageTable_reviews_data =    [
                                {name: '<?php echo $this->security->get_csrf_token_name(); ?>' , value: '<?php echo $this->security->get_csrf_hash(); ?>'}
                            ];
    pageTable_reviews = $('#pageTable_reviews').DataTable({
        language: {
            search: '',
            searchPlaceholder: 'Search'
        },
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 50,
        dom: 'Rlfrtip',
        // "aoColumnDefs": [{ "asSorting": [ "desc", "asc" ], "aTargets": [ 8 ] }],
        bProcessing: true,
        bServerSide: true,
        sServerMethod: 'POST',
        sAjaxSource: '<?php echo base_url("admin/carReviews/ajax_table") ?>',
        fnServerParams: function(aoData) {
            $.each(pageTable_reviews_data, function(i, field) {
                aoData.push({ name: field.name, value: field.value });
            });
            $('.client-dashboard-loader').fadeIn('50');
        },
        fnDrawCallback: function(data) {
            // console.log(data);
            $('.client-dashboard-loader').fadeOut('50');
        },createdRow: function( row, data, dataIndex ) {
            


        }
    });

    var current_password = '';
    $('#formAddCarReviews').on('submit', function(event) {
        event.preventDefault();
        var data = {
            review_id:$('#formAddCarReviews').find('[name="review_review_id"]').val(),
            date:$('#formAddCarReviews').find('[name="review_date"]').val(),
            description:$('#formAddCarReviews').find('[name="review_description"]').val(),
            particulars:$('#formAddCarReviews').find('[name="review_particulars"]').val(),
            amount:$('#formAddCarReviews').find('[name="review_amount"]').val(),
            requested_by:$('#formAddCarReviews').find('[name="review_requested_by"]').val(),
            remarks:$('#formAddCarReviews').find('[name="review_remarks"]').val()
        };
            
        
        $.post('<?php echo base_url('admin/carReviews/save_detail') ?>',data , function(data, textStatus, xhr) { 
            $('#add-edit-modal').modal('hide');
            pageTable_reviews.ajax.reload();
        });
    });


    $('#pageTable_reviews').on('click', '.btn_edit', function(event) {
        event.preventDefault();
        var review_id = $(this).attr('review_id');
        var data =  {
                        '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
                        table: 'tbl_car_reviews',
                        action: 'get',
                        select: {
                                    
                                },
                        where:  {
                                    review_id:review_id
                                },
                        field: '',
                        order: '',
                        limit: 0,
                        offset: 0,
                        group_by: ''
        
                    };
        $.post('<?php echo base_url('admin/carReviews/modelTable') ?>', data, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            $.each(data, function(index, val) {
                $.each(val, function(k, v) {
                     $('#formAddCarReviews').find('[name="review_'+k+'"]').val(v);
                });

                var date = val.date;
                $('#formAddCarReviews').find('[name="review_date"]').val(moment(date).format('YYYY-MM-DD'));
            });

            $('#add-edit-modal').modal('show');
        });
    });   

    $('#pageTable_reviews').on('click', '.btn_delete', function(event) {
        var review_id = $(this).attr('review_id');
        var data =  {
                        '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
                        table: 'tbl_car_reviews',
                        pk: review_id,
                        action: 'delete'
                    }; 
        $.post('<?php echo base_url('admin/carReviews/modelTable') ?>', data, function(data, textStatus, xhr) { 
            pageTable_reviews.ajax.reload();
        });
    });

    $('#btn-new-form').on('click', function(event) {
        event.preventDefault();
        $('#formAddCarReviews')[0].reset();
        $('[name="review_review_id"]').val('');
    });



});
</script>
<?php
$this->load->view('admin/includes/footer.php');
?>
