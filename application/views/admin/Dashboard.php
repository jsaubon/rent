<?php
$this->load->view('admin/includes/header.php');
$this->load->view('admin/includes/nav-left.php');
$this->load->view('admin/includes/nav-top.php');
?>
<style>
	#map {
		height: 100%;
	}

	.table {
		font-size: 11px;
	}
	.table select{
		font-size: 11px;
	}
</style>
<!-- Begin Page Content -->
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-2 col-md-6 mb-4">
          <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                  <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Filter</div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">
                      <select name="" class="form-control btn-filter-year">
                        <?php
                            for ($i=date('Y') - 5; $i < date('Y'); $i++) { 
                            ?>
                                <option value="<?php echo $i ?>"><?php echo $i ?></option>
                            <?php
                            }
                        ?>
                        <?php
                            for ($i=date('Y'); $i < date('Y') +10; $i++) { 
                            ?>
                                <option value="<?php echo $i ?>"><?php echo $i ?></option>
                            <?php
                            }
                        ?>
                      </select>
                      <select name="" class="form-control btn-filter-month">
                          <option value="01">January</option>
                          <option value="02">February</option>
                          <option value="03">March</option>
                          <option value="04">April</option>
                          <option value="05">May</option>
                          <option value="06">June</option>
                          <option value="07">July</option>
                          <option value="08">August</option>
                          <option value="09">September</option>
                          <option value="10">October</option>
                          <option value="11">November</option>
                          <option value="12">December</option>
                      </select>

                  </div>
                </div>
                <div class="col-auto">
                  <i class="fas fa-filter fa-2x text-gray-300"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-2 col-md-6 mb-4">
          <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                  <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Pending</div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800 transaction_status_count" transaction_status="Pending">0</div>
                </div>
                <div class="col-auto">
                  <i class="fas fa-calendar fa-2x text-gray-300"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-2 col-md-6 mb-4">
          <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                  <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Approved</div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800 transaction_status_count" transaction_status="Approved">0</div>
                </div>
                <div class="col-auto">
                  <i class="fas fa-thumbs-up fa-2x text-gray-300"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-2 col-md-6 mb-4">
          <div class="card border-left-info shadow h-100 py-2">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                  <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Ongoing</div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800 transaction_status_count" transaction_status="Ongoing">0</div>
                </div>
                <div class="col-auto">
                  <i class="fas fa-car fa-2x text-gray-300"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-2 col-md-6 mb-4">
          <div class="card border-left-danger shadow h-100 py-2">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                  <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Cancelled</div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800 transaction_status_count" transaction_status="Cancelled">0</div>
                </div>
                <div class="col-auto">
                  <i class="fas fa-times fa-2x text-gray-300"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-2 col-md-6 mb-4">
          <div class="card border-left-secondary shadow h-100 py-2">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                  <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Expired</div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800 transaction_status_count" transaction_status="Expired">0</div>
                </div>
                <div class="col-auto">
                  <i class="fas fa-history fa-2x text-gray-300"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
            
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary" style="position: relative;">Chart Based on Status
                        <select name="" class="select-chart" style="position: absolute;right: 0;width: 100px;">
                            <?php foreach ($this->session->transaction_statuses as $key => $value): ?>
                                <option value="<?php echo $value['transaction_status'] ?>"><?php echo $value['transaction_status'] ?></option>
                            <?php endforeach ?>
                        </select>
                    </h6>
                      
                    
                   
                      
                </div>
                <div class="card-body">
                    <div class="chart-bar">
                        <canvas id="myBarChart"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	  	

</div>
<!-- /.container-fluid -->

<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyATi13_7gCD7vftO_ccX1NSxvmo461GdPQ"
></script>
<script type="text/javascript" src="<?php echo base_url('assets/admin_template/js/jquery.googlemap.js') ?>"></script> -->

<!-- Page level plugins -->
<script src="<?php echo base_url('assets/admin_template/vendor/chart.js/Chart.min.js') ?>"></script>
 <script>
$(document).ready(function() {

    $('.btn-filter-month').val('<?php echo date('m') ?>');
    $('.btn-filter-year').val('<?php echo date('Y') ?>');
    $('.btn-filter-month').on('change', function(event) {
         event.preventDefault();
         get_cards();
         get_chart();
     });

    $('.select-chart').on('change', function(event) {
         event.preventDefault();
         get_chart();
     });

    $('#nav_dashboard').addClass('active');
    get_cards();
    function get_cards() {
        var month = $('.btn-filter-month').val();
        var year = $('.btn-filter-year').val();
        $.post('<?php echo base_url('admin/dashboard/get_cards') ?>', {month,year}, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            // console.log(data);
            if (data.length > 0) {
                $('.transaction_status_count').html(0);
                $.each(data, function(index, val) {
                    $('[transaction_status="'+val.transaction_status+'"]').html(val.count);
                });
            } else {
                $('[transaction_status="*"]').html(0);
            }
                
        });
    }
    // $("#map").googleMap({
    //   zoom: 14, // Initial zoom level (optional)
    //   coords: [8.947643, 125.540666], // Map center (optional)
    //   type: "ROADMAP" // Map type (optional)
    // });
    // $("#map").addMarker({
    //   coords: [8.947643, 125.540666], // GPS coords
    //   title: 'Marker n°1', // Title
    //   text:  '<b>Lorem ipsum</b> dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.' // HTML content
    // });



    // Set new default font family and font color to mimic Bootstrap's default styling
    Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
    Chart.defaults.global.defaultFontColor = '#858796';

    function number_format(number, decimals, dec_point, thousands_sep) {
      // *     example: number_format(1234.56, 2, ',', ' ');
      // *     return: '1 234,56'
      number = (number + '').replace(',', '').replace(' ', '');
      var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function(n, prec) {
          var k = Math.pow(10, prec);
          return '' + Math.round(n * k) / k;
        };
      // Fix for IE parseFloat(0.55).toFixed(0) = 0;
      s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
      if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
      }
      if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
      }
      return s.join(dec);
    }

    get_chart();
    function get_chart() {
        var month = $('.btn-filter-month').val();
        var year = $('.btn-filter-year').val();
        var transaction_status = $('.select-chart').val();
        console.log(month,transaction_status);
        $.post('<?php echo base_url('admin/dashboard/get_chart') ?>', {month,year,transaction_status},function(data, textStatus, xhr) {
            data = JSON.parse(data);
            var labels = [];
            var counts = [];
            $.each(data, function(index, val) {
                labels.push(val.car);
                counts.push(parseInt(val.count));
            });

            set_chart(labels,counts);
        });
    }

    var myBarChart;

    function set_chart(labels,counts) {
        // Bar Chart Example
        console.log(labels);
        console.log(counts);
        var ctx = document.getElementById("myBarChart");

        if (myBarChart) {
            myBarChart.destroy();
        }
        myBarChart = new Chart(ctx, {
          type: 'bar',
          data: {
            labels: labels,
            datasets: [{
              label: "Rents",
              backgroundColor: "#4e73df",
              hoverBackgroundColor: "#2e59d9",
              borderColor: "#4e73df",
              data: counts,
            }],
          },
          options: {
            maintainAspectRatio: false,
            layout: {
              padding: {
                left: 10,
                right: 25,
                top: 25,
                bottom: 0
              }
            },
            scales: {
              xAxes: [{
                time: {
                  unit: 'month'
                },
                gridLines: {
                  display: false,
                  drawBorder: false
                },
                ticks: {
                  maxTicksLimit: 6
                },
                maxBarThickness: 25,
              }],
              yAxes: [{
                ticks: {
                  min: 0,
                  max: Math.max(...counts) +3,
                  maxTicksLimit: 5,
                  padding: 10,
                  // Include a dollar sign in the ticks
                  callback: function(value, index, values) {
                    return '' + number_format(value);
                  }
                },
                gridLines: {
                  color: "rgb(234, 236, 244)",
                  zeroLineColor: "rgb(234, 236, 244)",
                  drawBorder: false,
                  borderDash: [2],
                  zeroLineBorderDash: [2]
                }
              }],
            },
            legend: {
              display: false
            },
            tooltips: {
              titleMarginBottom: 10,
              titleFontColor: '#6e707e',
              titleFontSize: 14,
              backgroundColor: "rgb(255,255,255)",
              bodyFontColor: "#858796",
              borderColor: '#dddfeb',
              borderWidth: 1,
              xPadding: 15,
              yPadding: 15,
              displayColors: false,
              caretPadding: 10,
              callbacks: {
                label: function(tooltipItem, chart) {
                  var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                  return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
                }
              }
            },
          }
        });
    }
        

});
</script>


<?php
$this->load->view('admin/includes/footer.php');
?>
