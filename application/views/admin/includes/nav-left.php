<style>
  .hide {
    display: none;;
  }
</style>
<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <!-- <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-car"></i>
        </div> -->
        <img src="<?php echo base_url('assets/images/fav.png') ?>" alt="" style="height: 75px;">

      </a>
      <div class="sidebar-brand-text mx-3 text-center hidden-sm-down" style="color: white;">Butuan Wheels <br>Travel & Services</div>

      <!-- Divider -->
      <hr class="sidebar-divider my-0 mt-2">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item hide" id="nav_dashboard" page_name="Dashboard">
        <a class="nav-link" href="<?php echo base_url('admin/dashboard') ?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>
      <!-- Nav Item - Dashboard -->
      <li class="nav-item hide" id="nav_protrack" page_name="ProTrack">
        <a class="nav-link" href="<?php echo base_url('admin/Protrack') ?>">
          <i class="fas fa-fw fa-car"></i>
          <span>ProTrack GPS</span></a>
      </li>
      <!-- Nav Item - Dashboard -->
      <li class="nav-item hide" id="nav_purechat" page_name="PureChat">
        <a class="nav-link" href="<?php echo base_url('admin/PureChat') ?>">
          <i class="fas fa-fw fa-comments"></i>
          <span>PureChat</span></a>
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item hide" page_name="Bookings">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#nav_bookings" aria-expanded="true" aria-controls="nav_bookings">
          <i class="fas fa-fw fa-calendar"></i>
          <span>Bookings</span>
        </a>
        <div id="nav_bookings" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="<?php echo base_url('admin/bookings?status=new')?>">New Booking</a>
            <?php foreach ($this->session->transaction_statuses as $key => $value): ?>
              <a class="collapse-item" href="<?php echo base_url('admin/bookings?status=').$value['transaction_status'] ?>"><?php echo $value['transaction_status'] ?> <?php echo $value['count'] != 0 ? '('.$value['count'].')' : ''; ?></a>
            <?php endforeach ?>
          </div>
        </div>
      </li>
      

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item hide" page_name="Cars Inventory">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#car_inventory" aria-expanded="true" aria-controls="car_inventory">
          <i class="fas fa-fw fa-car"></i>
          <span>Cars Inventory</span>
        </a>
        <div id="car_inventory" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="<?php echo base_url('admin/cars/cars?active=1') ?>">Active</a>
            <a class="collapse-item" href="<?php echo base_url('admin/cars/cars?active=0') ?>">Under Maintenance</a>
          </div>
        </div>
      </li>

      <li class="nav-item hide" id="nav_car_reviews" page_name="Car Reviews">
        <a class="nav-link" href="<?php echo base_url('admin/carReviews') ?>">
          <i class="fas fa-fw fa-star"></i>
          <span>Car Reviews</span></a>
      </li>

      <li class="nav-item hide" id="nav_maintenance" page_name="Car Maintenance">
        <a class="nav-link" href="<?php echo base_url('admin/maintenance') ?>">
          <i class="fas fa-fw fa-cogs"></i>
          <span>Car Maintenance</span></a>
      </li>

      <li class="nav-item hide" id="nav_expenses" page_name="Expenses">
        <a class="nav-link" href="<?php echo base_url('admin/expenses') ?>">
          <i class="fas fa-fw fa-wallet"></i>
          <span>Expenses</span></a>
      </li>


      <!-- Divider -->
      <hr class="sidebar-divider hide" page_name="Reports">

     <!-- Heading -->
      <div class="sidebar-heading hide" page_name="Reports">
        Reports
      </div>
      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item hide" page_name="Reports">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#client_reports" aria-expanded="true" aria-controls="client_reports">
          <i class="fas fa-fw fa-file"></i>
          <span>Reports</span>
        </a>
        <div id="client_reports" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="<?php echo base_url('admin/reports/account') ?>">Account Tracking</a>
            <a class="collapse-item" href="<?php echo base_url('admin/reports/expense') ?>">Expense Tracking</a>
          </div>
        </div>
      </li>


      <!-- Divider -->
      <hr class="sidebar-divider hide" page_name="Cars Setup">
     <!-- Heading -->
      <div class="sidebar-heading hide" page_name="Cars Setup">
        Setup
      </div>
      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item hide" page_name="Cars Setup">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#setup_cars" aria-expanded="true" aria-controls="setup_cars">
          <i class="fas fa-fw fa-cog"></i>
          <span>Cars Setup</span>
        </a>
        <div id="setup_cars" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Cars Setup:</h6>
            <a class="collapse-item" href="<?php echo base_url('admin/cars/brands') ?>">Brands</a>
            <a class="collapse-item" href="<?php echo base_url('admin/cars/models') ?>">Models</a>
          </div>
        </div>
      </li>

      <li class="nav-item hide" id="nav_userTypes" page_name="User Types">
        <a class="nav-link" href="<?php echo base_url('admin/settings/userTypes') ?>">
          <i class="fas fa-fw fa-user"></i>
          <span>User Types</span></a>
      </li>
      <li class="nav-item hide" id="nav_transactionStatus" page_name="Transaction Statuses">
        <a class="nav-link" href="<?php echo base_url('admin/settings/transactionStatus') ?>">
          <i class="fas fa-fw fa-question"></i>
          <span>Transaction Status</span></a>
      </li>


      <!-- Divider -->
      <hr class="sidebar-divider hide" page_name="Clients">

  
      <!-- Heading -->
      <div class="sidebar-heading hide" page_name="Clients">
        Clients
      </div>
      <li class="nav-item hide" id="nav_clients" page_name="Clients">
        <a class="nav-link" href="<?php echo base_url('admin/clients') ?>">
          <i class="fas fa-fw fa-user"></i>
          <span>Client Accounts</span></a>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider hide" page_name="User Accounts">

      <!-- Heading -->
      <div class="sidebar-heading hide" page_name="User Accounts">
        Users
      </div>
      <li class="nav-item hide" id="nav_admins" page_name="User Accounts">
        <a class="nav-link" href="<?php echo base_url('admin/administrators') ?>">
          <i class="fas fa-fw fa-users"></i>
          <span>User Accounts</span></a>
      </li>


      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->
<?php
  $user_data = $this->session->userdata('user_data');
?>
<script>
  get_pages();
  function get_pages() {
    var data =  {
                    table: 'tbl_user_types',
                    action: 'get',
                    select: {
                                
                            },
                    where:  {
                              user_type_id: '<?php echo $user_data['login_type'] ?>'
                            },
                    field: '',
                    order: '',
                    limit: 0,
                    offset: 0,
                    group_by: ''
    
                };
    $.post('<?php echo base_url('admin/dashboard/modelTable') ?>', data, function(data, textStatus, xhr) {
      console.log(data);
      data = JSON.parse(data);
      $.each(data, function(index, val) {
        var pages = (val.pages).split(',');
        $.each(pages, function(k, page) {
           $('[page_name="'+page+'"]').removeClass('hide');
        });
      });

      $('.nav-item.hide').remove();;
      $('.sidebar-heading.hide').remove();;
      $('.sidebar-divider.hide').remove();;
    });
  }
</script>