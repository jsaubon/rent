<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Butuan Wheels Admin Portal</title>
  <link rel="shortcut icon" href="<?php echo base_url('assets/images/fav.png');?>" />

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url('assets/admin_template/vendor/fontawesome-free/css/all.min.css') ?>" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo base_url('assets/admin_template/css/sb-admin-2.min.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/admin_template/vendor/datatables/dataTables.bootstrap4.min.css') ?>" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url('assets/admin_template/vendor/plugins/sweetalert/sweetalert.css') ?>">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">


  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url('assets/admin_template/vendor/jquery/jquery.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/admin_template/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url('assets/admin_template/vendor/jquery-easing/jquery.easing.min.js') ?>"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url('assets/admin_template/js/sb-admin-2.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/admin_template/vendor/datatables/jquery.dataTables.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/admin_template/vendor/datatables/dataTables.bootstrap4.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/admin_template/vendor/plugins/moment/moment.js') ?>"></script>
  <script src="<?php echo base_url('assets/admin_template/vendor/plugins/sweetalert/sweetalert.min.js') ?>"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  <style>
    .form-group label {
        margin-bottom: 0px;
    }

    .blinking{
        animation:blinkingText 1.2s infinite;
    }
    @keyframes blinkingText{
        0%{     color: red;}
        49%{    color: red;}
        60%{    color: red;}
        99%{    color: transparent;}
        100%{   color: red;}
    }
  </style>
</head>