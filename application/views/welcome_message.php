<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>
	<link href="https://admin.creditlynx.com/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://admin.creditlynx.com/assets/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="https://admin.creditlynx.com/assets/css/colors/blue.css" id="theme" rel="stylesheet">


	<script src="https://admin.creditlynx.com/assets/plugins/jquery/jquery.min.js"></script>
	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}

	.text-green {
		color: rgb(85, 206, 99);
	}
	</style>
</head>
<body>

<div id="container">
	<div class="card">
        <div class="card-body">
            <h4 class="card-title">Line Chart</h4>
            <div>
                <canvas id="chart_forecast" height="150"></canvas>
            </div>
        </div>
    </div>
		    
</div>

    <!-- <script src="https://admin.creditlynx.com/assets/plugins/Chart.js/chartjs.init.js"></script> -->
    <script src="https://admin.creditlynx.com/assets/plugins/Chart.js/Chart.min.js"></script>
<!-- <script src="https://admin.creditlynx.com/assets/js/morris-data.js"></script> -->
<script>
		

	function forecast(x, ky, kx){
	   var i=0, nr=0, dr=0,ax=0,ay=0,a=0,b=0;
	   function average(ar) {
	          var r=0;
	      for (i=0;i<ar.length;i++){
	         r = r+ar[i];
	      }
	      return r/ar.length;
	   }
	   ax=average(kx);
	   ay=average(ky);
	   for (i=0;i<kx.length;i++){
	      nr = nr + ((kx[i]-ax) * (ky[i]-ay));
	      dr = dr + ((kx[i]-ax)*(kx[i]-ax))
	   }
	  b=nr/dr;
	  a=ay-b*ax;
	  return (a+b*x);
	}

	get_forecase_iphone();
	function get_forecase_iphone() {
		
		var data_8 = forecast(8,[65,59,80,81,56,55,70],[1,2,3,4,5,6,7]);
		


		var data_9 = forecast(9,[59,80,81,56,55,70,data_8],[2,3,4,5,6,7,8]);
		
		var data_10 = forecast(10,[80,81,56,55,70,data_8,data_9],[3,4,5,6,7,8,9]);
		var data_11 = forecast(11,[81,56,55,70,data_8,data_9,data_10],[4,5,6,7,8,9,10]);
		var data_12 = forecast(11,[56,55,70,data_8,data_9,data_10,data_11],[5,6,7,8,9,10,11]);
		

		var forecase_iphone = [data_8,data_9,data_10,data_11,data_12];

        
        /*<!-- ============================================================== -->*/
        /*<!-- Line Chart -->*/
        /*<!-- ============================================================== -->*/
        new Chart(document.getElementById("chart_forecast"),
            {
                "type":"line",
                "data":{"labels":["January","February","March","April","May","June","July","August","September","October","November","December"],

                "datasets":[{
                                "label":"Actual",
                                "data":[65,59,80,81,56,55,70],
                                "fill":false,
                                "borderColor":"rgb(38, 198, 218)",
                                "lineTension":0.1
                            },{
                                "label":"Forecast",
                                "data":[null,null,null,null,null,null,null,data_8,data_9,data_10,data_11,data_12],
                                "fill":false,
                                "borderColor":"red",
                                "lineTension":0.1
                            }]
                            
            },"options":{}});
        
	}
	// alert(forecast(30,[6,7,9,15,21],[20,28,31,38,40]));
</script>	
</body>
</html>