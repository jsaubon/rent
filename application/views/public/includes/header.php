<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="img/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="codepixer">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>Butuan Wheels</title>
  	<link rel="shortcut icon" href="<?php echo base_url('assets/images/fav.png');?>" />

	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
		<!--
		CSS
		============================================= -->
		<link rel="stylesheet" href="<?php echo base_url('assets/public_template/css/linearicons.css')?>">
		<link rel="stylesheet" href="<?php echo base_url('assets/public_template/css/font-awesome.min.css')?>">
		<link rel="stylesheet" href="<?php echo base_url('assets/public_template/css/bootstrap.css')?>">
		<link rel="stylesheet" href="<?php echo base_url('assets/public_template/css/magnific-popup.css')?>">
		<link rel="stylesheet" href="<?php echo base_url('assets/public_template/css/nice-select.css')?>">					
		<link rel="stylesheet" href="<?php echo base_url('assets/public_template/css/animate.min.css')?>">
		<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">			
		<link rel="stylesheet" href="<?php echo base_url('assets/public_template/css/owl.carousel.css')?>">
		<link rel="stylesheet" href="<?php echo base_url('assets/public_template/css/main.css')?>">
		<link rel="stylesheet" href="<?php echo base_url('assets/public_template/plugins/wickedpicker/stylesheets/wickedpicker.css')?>">
  		<link rel="stylesheet" href="<?php echo base_url('assets/admin_template/vendor/plugins/sweetalert/sweetalert.css') ?>">
		<link rel="stylesheet" href="<?php echo base_url('assets/admin_template/vendor/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') ?>">


			

		<script src="<?php echo base_url('assets/public_template/')?>js/vendor/jquery-2.2.4.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="<?php echo base_url('assets/public_template/')?>js/vendor/bootstrap.min.js"></script>			
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>			
			<script src="<?php echo base_url('assets/public_template/')?>js/easing.min.js"></script>			
		<script src="<?php echo base_url('assets/public_template/')?>js/hoverIntent.js"></script>
		<script src="<?php echo base_url('assets/public_template/')?>js/superfish.min.js"></script>	
		<script src="<?php echo base_url('assets/public_template/')?>js/jquery.ajaxchimp.min.js"></script>
		<script src="<?php echo base_url('assets/public_template/')?>js/jquery.magnific-popup.min.js"></script>	
		<script src="<?php echo base_url('assets/public_template/')?>js/owl.carousel.min.js"></script>			
		<script src="<?php echo base_url('assets/public_template/')?>js/jquery.sticky.js"></script>
		<script src="<?php echo base_url('assets/public_template/')?>js/jquery.nice-select.min.js"></script>	
		<script src="<?php echo base_url('assets/public_template/')?>js/waypoints.min.js"></script>
		<script src="<?php echo base_url('assets/public_template/')?>js/jquery.counterup.min.js"></script>					
		<script src="<?php echo base_url('assets/public_template/')?>js/parallax.min.js"></script>		
		<script src="<?php echo base_url('assets/public_template/')?>js/mail-script.js"></script>	
		<script src="<?php echo base_url('assets/public_template/')?>js/main.js"></script>	
		<script src="<?php echo base_url('assets/public_template/plugins/wickedpicker/src/wickedpicker.js')?>"></script>	
  		<script src="<?php echo base_url('assets/admin_template/vendor/plugins/sweetalert/sweetalert.min.js') ?>"></script>
  		<script src="<?php echo base_url('assets/admin_template/vendor/plugins/moment/moment.js') ?>"></script>
		<script src="<?php echo base_url('assets/admin_template/vendor/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') ?>"></script>

		<style>
			.hide {
				display: none;
			}

			.nice-select .list { max-height: 300px; overflow-y: scroll; }

			.wickedpicker {
				height: 155px !important;
			}
		</style>
	</head>
	<body>