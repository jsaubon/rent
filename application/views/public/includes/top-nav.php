
		  <header id="header" id="home">
		    <div class="container">
		    	<div class="row align-items-center justify-content-between d-flex">
			      <div id="logo">
			        <a href=""><img src="<?php echo base_url('assets/images/fav.png')?>" width="50px" alt="" title="" /> <span style="color: white;font-size: 18px;">Butuan Wheels</span></a>
			      </div>
			      <nav id="nav-menu-container">
			        <ul class="nav-menu">
			          <li class="menu-active"><a href="<?php echo base_url() ?>">Home</a></li>
			          <!-- <li><a href="about.html">About</a></li> -->
			          <li><a href="<?php echo base_url('cars') ?>">Cars</a></li>
			          <?php if ($this->session->userdata('client_data')): ?>
			          	<li><a href="<?php echo base_url('profile') ?>">Profile</a></li>
			          	<li><a href="<?php echo base_url('public/login/logout') ?>">Logout</a></li>
			          <?php endif ?>
			          <?php if (!$this->session->userdata('client_data')): ?>
			          	<li><a href="<?php echo base_url('login') ?>">Login</a></li>
			          <?php endif ?>
			          
			          <!-- <li><a href="service.html">Service</a></li> -->
			          <!-- <li><a href="team.html">Team</a></li>	 -->
			          <!-- <li><a href="blog-home.html">Blog</a></li>	 -->
			          <!-- <li><a href="contact">Contact</a></li>	 -->
			          <!-- <li class="menu-has-children"><a href="">Pages</a> -->
			            <!-- <ul>
			              <li><a href="blog-single.html">Blog Single</a></li>
			              <li><a href="elements.html">Elements</a></li>
			            </ul>
			          </li>			           -->
			        </ul>
			      </nav><!-- #nav-menu-container -->		    		
		    	</div>
		    </div>
		  </header><!-- #header -->