<?php $this->load->view('public/includes/header')?>
<?php $this->load->view('public/includes/top-nav')?>
<style>
    .car_rates {
        color: #fab700;
    }
    
    .car_likes_comments {
        text-align: center;
        margin-top: 10px;
    }
    .car_likes_comments a {
        text-decoration: none;
        color: #777777;
    }

    .nice-select {
        width: 100%;
    }

    .fa.text-default {
        color: #D9D9D9;
    }
    #gSignIn{
      width: 100%;
      margin-top: 20px; 
    }

    #gSignIn > div{
      margin: 0 auto;
    }

    .new_review_stars {
        cursor: pointer;
        color: #D9D9D9;
    }
    .fa-star {
        cursor: pointer;
        color: #D9D9D9;
    }
</style>
<?php $user_data = $this->session->userdata('client_data')?>

            <!-- start banner Area -->
            <section class="banner-area relative" id="home">    
                <div class="overlay overlay-bg"></div>
                <div class="container">
                    <div class="row d-flex align-items-center justify-content-center">
                        <div class="about-content col-lg-12">
                            <h1 class="text-white">
                                Cars            
                            </h1>   
                            <p class="text-white link-nav"><a href="<?php echo base_url() ?>">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href=""> Cars</a></p>
                        </div>                                          
                    </div>
                </div>
            </section>
            <!-- End banner Area -->    

            <!-- Start blog-posts Area -->
            <section class="blog-posts-area section-gap">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 post-list blog-post-list">
                            <div class="single-post">
                                <div class="text-center">
                                    <img class="img-fluid" src="<?php echo base_url('assets/uploads/'.$car['image']) ?>" width="100%" alt="">
                                </div>
                                <a href="#">
                                    <h1 class="mt-10"><?php echo $car['brand'] ?> <?php echo $car['model'] ?></h1>
                                </a>
                                <div class="content-wrap">
                                    <p>
                                        <?php echo $car['description'] ?>
                                    </p>

                                    <p>
                                        Capacity         : <?php echo $car['capacity'] ?> Person <br>
                                        Transmission     : <?php echo $car['transmission'] ?> <br>
                                        <hr>
                                        <strong class="car_rates">
                                            <h4>Rates</h4>
                                            <?php 
                                                $rates = explode('<br>',$car['rate']);
                                                foreach ($rates as $key => $rate) {
                                                    echo $rate.'<small>/day</small><br>';
                                                }
                                            ?>
                                        </strong>
                                    </p>                              
                                    <hr>
                        
                                </div> 
                           

                            <!-- Start comment-sec Area -->
                            <section class="comment-sec-area pt-30 pb-80">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12 text-center">
                                            <h3 >Reviews</h3>
                                            <h1 style="font-size: 50px;margin-bottom: 10px;" class="avg_stars"></h1>
                                            <h4 style="margin-top: 10px;" class="avg_stars_container">
                                                <i class="fa fa-star text-warning"></i>
                                                <i class="fa fa-star text-warning"></i>
                                                <i class="fa fa-star text-warning"></i>
                                                <i class="fa fa-star text-warning"></i>
                                                <i class="fa fa-star text-default"></i>
                                            </h4>
                                            <h6><i class="fa fa-user"></i> <span class="reviews_count"></span> total</h6>
                                        </div>
                                    </div>
                                    
                                </div>  
                                <hr>
                                <?php if ($user_data == null): ?>
                                    <div class="container text-center pt-50">
                                        <h5>Login to Write a Review</h5>

                                        <button type="submit" class="genric-btn warning large" style="width: 120px;">Login</button> 
                                        <a href="<?php echo base_url('register') ?>" class="genric-btn success large mt-20" style="width: 120px;">Register</a>  
                                        <div id="gSignIn"></div>
                                    </div>
                                <?php endif ?>  
                                    
                                <?php if ($user_data != null): ?>
                                    <div class="container pt-50">
                                        <h5 class="pb-10">Write a Review</h5>
                                        <div class="row flex-row d-flex">
                                            <div class="col-md-12">
                                                <textarea class="form-control mb-10" name="car_review" placeholder="Tell others what you think about this car. Would you recommend it, and why?"  required=""></textarea>
                                                <div class="text-right">
                                                    <span class="">Most helpful reviews have 100 words or more</span>
                                                    <h2 class="">
                                                        <i star="1" class="fa fa-star new_review_stars"></i>
                                                        <i star="2" class="fa fa-star new_review_stars"></i>
                                                        <i star="3" class="fa fa-star new_review_stars"></i>
                                                        <i star="4" class="fa fa-star new_review_stars"></i>
                                                        <i star="5" class="fa fa-star new_review_stars"></i>
                                                    </h2>
                                                </div>
                                                    
                                                <a class="primary-btn mt-20 pull-right btn-submit-review" href="#">Submit</a>
                                            </div>
                                        </div>
                                    </div>  
                                <?php endif ?>

                                    
                                <div class="container pt-50">
                                    <hr>
                                    <div class="row flex-column">
                                        <h5 class="text-uppercase"><span class="reviews_count"></span> Reviews</h5>
                                        <br>
                                        <div class="reviews_container">
                                            <div class="comment-list">
                                                <div class="single-comment justify-content-between d-flex">
                                                    <div class="user justify-content-between d-flex">
                                                        <div class="desc">
                                                            <h5><a href="#">Emilly Blunt</a></h5>
                                                            <p class="date">December 4, 2017 at 3:12 pm </p>
                                                            <p class="comment">
                                                                Never say goodbye till the end comes!
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                                                                                                                                                                            
                                    </div>
                                </div>  
                            </section>
                            <!-- End comment-sec Area -->
                            
                            <!-- Start commentform Area -->
                            <section class="commentform-area pt-80">
                                
                            </section>
                            <!-- End commentform Area -->


                            </div>                                                                      
                        </div>
                        <div class="col-lg-4 sidebar">
                            <div class="single-widget search-widget">
                                
                              <a href="<?php echo base_url('cars') ?>" class="btn btn-block" style="color: #777777"><i class="fa fa-arrow-left"></i> Pick Another Car </a>
                                                    
                            </div>

                            <div class="single-widget recent-posts-widget">
                                <h4 class="title">Booking Details</h4>
                                <form id="formAddClient" action="return false" method="POST" class="form" role="form" autocomplete="off" novalidate>
                                    <?php $rates = explode('<br>',$car['rate']);?>
                                    <div class="form-group">
                                        <div class="default-select " >
                                            <select required name="car_rate" class="select_rate">
                                                <option value="" disabled selected hidden>Select Car Rate</option>
                                               <?php foreach ($rates as $key => $rate): ?>
                                                   <option value="<?php echo $rate ?>"><?php echo $rate ?></option>
                                               <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6 wrap-left">
                                            <div class="input-group dates-wrap">                                          
                                                <input required name="date_start" class="dates form-control" placeholder="Date" type="text" value="<?php echo date('m/d/Y',strtotime(' + 1 day')) ?>">                        
                                                <div class="input-group-prepend">
                                                    <span  class="input-group-text"><span class="lnr lnr-calendar-full"></span></span>
                                                </div>                                          
                                            </div>
                                        </div>
                                        <div class="col-md-6 wrap-right">
                                            <div class="input-group dates-wrap">                                          
                                                <input required name="time_start" class="times_start form-control form-date" placeholder="Time" type="text">                        
                                                <div class="input-group-prepend">
                                                    <span  class="input-group-text"><span class="lnr lnr-clock"></span></span>
                                                </div>                                          
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6 wrap-left">
                                            <div class="input-group dates-wrap">                                          
                                                <input required name="date_end" class="dates form-control" placeholder="Date" type="text" value="<?php echo date('m/d/Y',strtotime(' + 1 day')) ?>">                        
                                                <div class="input-group-prepend">
                                                    <span  class="input-group-text"><span class="lnr lnr-calendar-full"></span></span>
                                                </div>                                          
                                            </div>
                                        </div>
                                        <div class="col-md-6 wrap-right">
                                            <div class="input-group dates-wrap">                                          
                                                <input required name="time_end" class="times_end form-control" placeholder="Time" data-format="hh:mm:ss" type="text">                        
                                                <div class="input-group-prepend">
                                                    <span  class="input-group-text"><span class="lnr lnr-clock"></span></span>
                                                </div>                                          
                                            </div>
                                        </div>
                                    </div>                          
                                    <div class="from-group <?php if ($this->session->userdata('client_data')): ?>hide <?php endif ?>">
                                        
                                        <input style="margin-top: 15px;" required class="form-control txt-field" type="text" name="client_name" placeholder="Your name" value="<?php echo $user_data['client_name'] ?>">
                                        <input style="margin-top: 15px;" required class="form-control txt-field" type="email" name="client_email" placeholder="Email address" value="<?php echo $user_data['client_email'] ?>">
                                        <input style="margin-top: 15px;" required class="form-control txt-field" type="tel" name="client_phone" placeholder="Phone number" value="<?php echo $user_data['client_phone'] ?>">
                                    </div> 
                                    <div class="form-group row " style="margin-top: 15px;">
                                        <div class="col-md-12">
                                            <button type="submit" class="genric-btn primary btn-lg btn-block text-center text-uppercase">Confirm Car Booking</button>
                                        </div>
                                    </div>
                                </form>                    
                            </div>

                                   

                        </div>
                    </div>
                </div>  
            </section>
            <!-- End blog-posts Area -->


<meta name="google-signin-client_id" content="1051254273169-lgtjeqc7hucku98cd6hje32dfasfvqsf.apps.googleusercontent.com">
<script src="https://apis.google.com/js/platform.js"  ></script>
<script>
    renderButton();
    function renderButton() {
        gapi.signin2.render('gSignIn', {
            'scope': 'profile email',
            'width': 240,
            'height': 50,
            'longtitle': true,
            'theme': 'dark',
            'onsuccess': signOut,
            'onfailure': onFailure
        });
    }
        function signOut(googleUser) {
            var auth2 = gapi.auth2.getAuthInstance();
            auth2.signOut().then(function () {
                console.log('User signed out.');
                var profile = googleUser.getBasicProfile();
                window.location.href = '<?php echo base_url() ?>'+'register?name='+profile.getName()+'&email='+profile.getEmail();

                console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
                console.log('Name: ' + profile.getName());
                console.log('Image URL: ' + profile.getImageUrl());
                console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
            });
        }

        // Sign-in failure callback
        function onFailure(error) {
            alert(error);
        }

    // });
</script>
<script>
    $(document).ready(function() {
        
        $('select').niceSelect();

        $('.dates').bootstrapMaterialDatePicker({
                weekstart: 0,
                time: false,
                minDate: '<?php echo date('m/d/Y') ?>',
                format: 'mm/DD/Y',
                disabledDays: [
                    '02/22/2020'
                ]
        });

        $('.times_start').wickedpicker({now: "06:00"});
        $('.times_end').wickedpicker({now: "18:00"});

        $('.select_car').on('change', function(event) {
            event.preventDefault();
            var rates = $(this).find('option:selected').attr('rates');
            rates = rates.split('<br>');
            $('.select_rate').empty();
            $('[name="car_rate"]').next().remove();
            $('.select_rate').append('<option>Select Rate </option>');
            $.each(rates, function(index, rate) {
                 $('.select_rate').append('<option value="'+rate+'">'+rate+'</option>');
            });
            $('.select_rate').niceSelect();

        });

        $('#formAddClient').on('submit', function(event) {
            event.preventDefault();
            var client_id = '<?php echo $user_data['client_id'] ?>';
            var car_id = '<?php echo $car['car_id'] ?>';
            var car_rate = $('[name="car_rate"]').next().find('.selected').attr('data-value');
            var client_name = $('#formAddClient').find('[name="client_name"]').val();
            var client_email = $('#formAddClient').find('[name="client_email"]').val();
            var client_phone = $('#formAddClient').find('[name="client_phone"]').val();
            var client_note = 'New Booking from Website';
            var transaction_type = 'Self Drive';
            var date_start = moment($('#formAddClient').find('[name="date_start"]').val()).format('YYYY-MM-DD');
            var time_start = $('#formAddClient').find('[name="time_start"]').val();
            var date_end = moment($('#formAddClient').find('[name="date_end"]').val()).format('YYYY-MM-DD');;
            var time_end = $('#formAddClient').find('[name="time_end"]').val();
            var transaction_status = 'Pending';
            console.log(date_start);
            if (time_end != '' || time_start != '') {
                if (car_rate) { 
                    if (car_rate == 'Select Rate') {
                        Swal.fire(
                            'Error',
                            'Please select Rate',
                            'error'
                        );
                    } else {
                        $.post('<?php echo base_url('public/bookings/save_client') ?>', {client_id,car_id,car_rate,client_name,client_email,client_phone,client_note,transaction_type,date_start,time_start,date_end,time_end,transaction_status} , function(data, textStatus, xhr) { 
                            var swal_text = "We Created an Account for you!<br>your default password is your <b>phone number</b><br>please change your password on the profile page<br>thank you!";
                            console.log(data);
                            if (data == 'booking exist') {
                                Swal.fire({
                                    title: 'Booking Date is not Available!',
                                    html: 'Please pick another date, thank you!',
                                    icon: 'error',
                                    showCancelButton: false,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Okay'
                                }).then((result) => {
                                    if (result.value) {
                                       
                                    }
                                })
                            } else if (data == 'exist') {
                                swal_text = '';
                                Swal.fire({
                                    title: 'New Booking Submitted!',
                                    html: swal_text,
                                    icon: 'success',
                                    showCancelButton: false,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Okay'
                                }).then((result) => {
                                    if (result.value) {
                                        $.post('<?php echo base_url('public/login/login_user') ?>', {email:client_email,password:client_phone}, function(data, textStatus, xhr) {
                                            console.log(data);
                                                window.location.href = '<?php echo base_url('profile') ?>';
                                        });
                                    }
                                })

                                $('#formAddClient')[0].reset();
                                $('[name="client_car_rate_select"]').empty();
                            } else {
                                Swal.fire({
                                    title: 'New Booking Submitted!',
                                    html: swal_text,
                                    icon: 'success',
                                    showCancelButton: false,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Okay'
                                }).then((result) => {
                                    if (result.value) {
                                        $.post('<?php echo base_url('public/login/login_user') ?>', {email:client_email,password:client_phone}, function(data, textStatus, xhr) {
                                            console.log(data);
                                                window.location.href = '<?php echo base_url('profile') ?>';
                                        });
                                    }
                                })

                                $('#formAddClient')[0].reset();
                                $('[name="client_car_rate_select"]').empty();

                            }
                        });
                    }
                } else {
                    Swal.fire(
                        'Error',
                        'Please select Rate',
                        'error'
                    );
                }
            } else {
                Swal.fire(
                    'Error',
                    'Please select Time',
                    'error'
                );
            } 
        });

        $('.new_review_stars').hover( function(event) {
            event.preventDefault();
            $('.new_review_stars').removeClass('text-warning');
            var star = $(this).attr('star');

            for (var i = 1; i <= star; i++) {
                $('.new_review_stars[star="'+i+'"]').addClass('text-warning');
            }
        });

        $('.btn-submit-review').on('click', function(event) {
            event.preventDefault();
            var review = $('textarea[name="car_review"]').val();
            var star = 0;
            var count = 5;
            if ($('.new_review_stars').hasClass('text-warning')) {

                while(star == 0) {
                    if ($('.new_review_stars[star="'+count+'"]').hasClass('text-warning')) {
                        star = count;
                    } else {
                        count--;
                    }
                }
            } else {

            }

            if (star != 0) {
                if (review.length == '') {
                    Swal.fire('Please write a review','','error');
                } else {
                    var data =  {
                                    table: 'tbl_car_reviews',
                                    pk: '',
                                    action: 'save',
                                    fields: {
                                                client_id: '<?php echo $user_data['client_id'] ?>',
                                                car_id: '<?php echo $car['car_id'] ?>',
                                                review: review,
                                                star: star,
                                                date: moment().format('YYYY-MM-DD HH:mm:ss')
                                            },
                                    return: ''
                                };
                    
                    $.post('<?php echo base_url('public/cars/modelTable') ?>', data , function(data, textStatus, xhr) { 
                        Swal.fire('Review Submitted!','Thank you for writing a review...','success');
                        get_car_reviews();
                        $('textarea[name="car_review"]').val('');
                        $('.new_review_stars').removeClass('text-warning');
                    });
                }
            } else {
                Swal.fire('Rating is required','','error');
            }

        });

        get_car_reviews();
        function get_car_reviews() {
            var car_id = '<?php echo $car['car_id'] ?>';
            $.post('<?php echo base_url('public/cars/get_car_reviews') ?>', {car_id}, function(data, textStatus, xhr) {
                console.log(data);
                data = JSON.parse(data);
                var reviews_count = data.length;
                $('.reviews_count').html(reviews_count);
                $('.reviews_container').empty();

                var stars = 0;
                if (data.length != 0) {
                    $.each(data, function(index, val) {
                        var star = val.star;
                        stars += parseInt(star);;
                        var fa_star = [];
                        for (var i = 1; i <= 5; i++) {
                            if (i <= star) {
                                fa_star.push('<i class="fa fa-star text-warning"></i>');
                            } else {
                                fa_star.push('<i class="fa fa-star "></i>');
                            }
                        }

                        fa_star = fa_star.join('');

                        var new_review = ' <div class="comment-list">\
                                                    <div class="single-comment justify-content-between d-flex">\
                                                        <div class="user justify-content-between d-flex">\
                                                            <div class="desc">\
                                                                <h5><a href="#">'+val.client_name+'</a></h5>\
                                                                <p class="date">'+fa_star+' '+moment(val.date).format('LLL')+'</p>\
                                                                <p class="comment">\
                                                                    '+val.review+'\
                                                                </p>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                </div>';
                        $('.reviews_container').append(new_review);
                    });
                } else {
                    $('.reviews_container').append('<div class="text-center">Sign In and write a review</div>');
                }
                    

                var avg_stars = Math.round(stars / reviews_count);

                console.log(stars,reviews_count,avg_stars);

                $('.avg_stars').html(avg_stars ? avg_stars : 0)

                var fa_star = [];
                for (var i = 1; i <= 5; i++) {
                    if (i <= avg_stars) {
                        fa_star.push('<i class="fa fa-star text-warning"></i>');
                    } else {
                        fa_star.push('<i class="fa fa-star "></i>');
                    }
                }

                fa_star = fa_star.join('');
                $('.avg_stars_container').html(fa_star);

            });
        }
    });
</script>


<?php $this->load->view('public/includes/footer')?>