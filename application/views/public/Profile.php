<?php $this->load->view('public/includes/header')?>
<?php $this->load->view('public/includes/top-nav')?>
<style>
	.car_rates {
		color: #fab700;
	}
	
	.car_likes_comments {
		text-align: center;
		margin-top: 10px;
	}
	.car_likes_comments a {
		text-decoration: none;
		color: #777777;
	}
</style>
			<!-- start banner Area -->
			<section class="banner-area relative" id="home">	
				<div class="overlay overlay-bg"></div>
				<div class="container">
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								Profile			
							</h1>	
							<p class="text-white link-nav"><a href="<?php echo base_url() ?>">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="<?php echo base_url('profile') ?>"> Profile</a></p>
						</div>											
					</div>
				</div>
			</section>
			<!-- End banner Area -->	

			
			<!-- Start blog-posts Area -->
			<section class="blog-posts-area mt-50 mt-md-20">
				<div class="container">
					<div class="row">
						<div class="col-12 col-md-4 border-right ">
							<h1>Profile Information</h1>
							<form action="<?php echo base_url('public/profile/save_profile') ?>" method="POST" class="mt-20">
								<input type="hidden" name="client_id" value="<?php echo $user_data['client_id'] ?>">
								<label for="">Email Address</label>
								<input name="client_email" disabled placeholder="Enter email address"  class="common-input mb-20 form-control" required="" type="email" value="<?php echo $user_data['client_email'] ?>">
								<label for="">Name</label>
								<input name="client_name" disabled placeholder="Enter name"  class="common-input mb-20 form-control" required="" type="text" value="<?php echo $user_data['client_name'] ?>">
								<label for="">Phone Number</label>
								<input name="client_phone" disabled placeholder="Enter phone number"  class="common-input mb-20 form-control" required="" type="text" value="<?php echo $user_data['client_phone'] ?>">
								<div class=" alert-msg text-success" style="text-align: center;"><?php echo $this->session->profile_success_message; ?></div>
								<div class="text-center">
									<button type="button" class="genric-btn success mt-20 large btn-edit-profile"  style="width: 160px;">Edit</button>
									<button type="submit" class="genric-btn primary mt-20 large hide btn-save-profile" style="width: 160px;">Save</button>
								</div>

								
							</form>
							<h5 class="pt-50">Click <a href="#" class="btn-view-change-password">here</a> to change your password</h5>
							<form action="<?php echo base_url('public/profile/change_password') ?>"  method="POST" class="<?php echo $this->input->get('saved') != 2 ?  'hide' : ''?> form-change-password mb-100">
								<h3>New Password</h3>
								<input type="hidden" name="client_id" value="<?php echo $user_data['client_id'] ?>">
								<div class="row mt-20">
									<div class="col-md-6">
										<label for="">Password</label>
										<input name="client_password" placeholder="New password" class="common-input mb-20 form-control" required="" type="password">
									</div>
									<div class="col-md-6">
										<label for="">Confirm Password</label>
										<input name="confirm_password" placeholder="Confirm password" class="common-input mb-20 form-control" required="" type="password">
									</div>
								</div>
								<div class=" alert-msg text-danger" style="text-align: center;"><?php echo $this->session->change_password_error; ?></div>
								<div class="text-center">
									<button type="submit" class="genric-btn danger mt-10 large" >Change Password</button>
								</div>
							</form>
							
								
						</div>
						<div class="col-12 col-md-8 mt-40 mt-md-0">
							<h1 class="text-center">Bookings</h1>
							<div class="table-responsive">
								<table class="table mt-20 ">
									<thead>
										<tr>
											<th>Status</th>
											<th>Date Start</th>
											<th>Date End</th>
											<th>Vehicle</th>
											<th>Rate</th>
											<th>Payment Date</th>
										</tr>
									</thead>
									<tbody>
										<?php if (count($bookings) > 0): ?>
											<?php foreach ($bookings as $key => $value): ?>
												<tr>
													<td class="<?php if ($value['transaction_status'] == 'Ongoing' || $value['transaction_status'] == 'Approved' || $value['transaction_status'] == 'Satisfied'){ ?>
														text-success
													<?php } else { ?>
														text-danger
													<?php } ?>"><?php echo $value['transaction_status'] ?></td>
													<td><?php echo date('m/d/Y h:iA',strtotime($value['date_start'])) ?></td>
													<td><?php echo date('m/d/Y h:iA',strtotime($value['date_end'])) ?></td>
													<td><a href="<?php echo base_url('cars/detail/').$value['car_id'] ?>"><?php echo $value['car'] ?></a></td>
													<td><?php echo $value['car_rate'] ?></td>
													<td><?php echo $value['date_of_payment'] == '0000-00-00 00:00:00' ? 'Pay with ' : date('Y-m-d h:m A') ?></td>
												</tr>
											<?php endforeach ?>
										<?php endif ?>
										<?php if (count($bookings) == 0): ?>
											<tr>
												<td colspan="5" class="text-center">Click <a href="<?php echo base_url('cars') ?>">here</a> to Book your first Car!</td>
											</tr>
										<?php endif ?>
											
									</tbody>
								</table>
							</div>
								
						</div>
					</div>
					
				</div>	
			</section>
			<!-- End blog-posts Area -->
<?php
	
			$this->session->profile_success_message = '' ;

			$this->session->change_password_error = '' ;
?>
	
<script>
	$(document).ready(function() {
		$('.btn-edit-profile').on('click', function(event) {
			event.preventDefault();
			$(this).addClass('hide');
			$('[name="client_name"]').attr('disabled', false);
			$('[name="client_phone"]').attr('disabled', false);
			$('.btn-save-profile').removeClass('hide');
		});

		$('.btn-view-change-password').on('click', function(event) {
			event.preventDefault();
			$('.form-change-password').removeClass('hide');
		});
	});
</script>

<?php $this->load->view('public/includes/footer')?>
