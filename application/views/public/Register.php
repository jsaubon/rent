<?php $this->load->view('public/includes/header')?>
<?php $this->load->view('public/includes/top-nav')?>
<style>
	.car_rates {
		color: #fab700;
	}
	
	.car_likes_comments {
		text-align: center;
		margin-top: 10px;
	}
	.car_likes_comments a {
		text-decoration: none;
		color: #777777;
	}
</style>
			<!-- start banner Area -->
			<section class="banner-area relative" id="home">	
				<div class="overlay overlay-bg"></div>
				<div class="container">
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								Register			
							</h1>	
							<p class="text-white link-nav"><a href="<?php echo base_url() ?>">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="<?php echo base_url('register') ?>"> Register</a></p>
						</div>											
					</div>
				</div>
			</section>
			<!-- End banner Area -->	

			
			<!-- Start blog-posts Area -->
			<section class="blog-posts-area section-gap">
				<div class="container">
						
					<form class="form-area " id="form_registration" action="return false" method="post" class="contact-form text-right">
						<h1 class="text-center mb-20">Complete Registration the Form</h1>
						<div class="row">	
							<div class="col-lg-6 offset-md-3 form-group">
								<label for="">Name</label>
								<input name="client_name" placeholder="Enter name"  class="common-input mb-20 form-control stronly" required="" type="text" value="<?php echo $client_name ?>">
								<label for="">Email Address</label>
								<input name="client_email" placeholder="Enter email address"  class="common-input mb-20 form-control" required="" type="email" value="<?php echo $client_email ?>">
								<label for="">Phone Number</label>
								<input name="client_phone" placeholder="Enter phone number"  class="common-input mb-20 form-control numonly" required="" type="text">
								<div class="row">
									<div class="col-md-6">
										<label for="">Password</label>
										<input name="client_password" placeholder="Enter your password" class="common-input mb-20 form-control" required="" type="password">
									</div>
									<div class="col-md-6">
										<label for="">Confirm Password</label>
										<input name="confirm_password" placeholder="Confirm your password" class="common-input mb-20 form-control" required="" type="password">
									</div>
								</div>
								<div class="mt-20 alert-msg text-danger" style="text-align: center;"></div>
								<div class="text-center">
									<button type="submit" class="genric-btn primary mt-20 large" style="width: 160px;">Register</button>
								</div>
								<div class="text-center">
									<a href="<?php echo base_url('login') ?>" class="genric-btn danger mt-20 large" style="width: 160px;"><i class="fa fa-arrow-left fa-lg"></i> Back to Login</a>
								</div>
							</div>
						</div>
					</form>	
				</div>	
			</section>
			<!-- End blog-posts Area -->
			
	
<script>
	$(document).ready(function() {
		$('#form_registration').on('submit', function(event) {
			event.preventDefault();
			password = $('[name="client_password"]').val();
			console.log(password.length);
			if (password.length < 8) {
				$('.alert-msg').html('Password must be at least 8 characters');
			} else {
				confirm_password = $('[name="confirm_password"]').val();
				if (password != confirm_password) {
					$('.alert-msg').html('Password mismatch');
				} else {
					data = {
						client_name : $(this).find('[name="client_name"]').val(),
						client_email : $(this).find('[name="client_email"]').val(),
						client_phone : $(this).find('[name="client_phone"]').val(),
						client_password : $(this).find('[name="client_password"]').val(),
					};
					console.log(data);
					$.post('<?php echo base_url('public/login/register_client') ?>', data, function(data, textStatus, xhr) {
						window.location.href = '<?php echo base_url('cars') ?>';
					});
				}
			}
		});	
	});
</script>

<?php $this->load->view('public/includes/footer')?>
