<?php $this->load->view('public/includes/header')?>
<?php $this->load->view('public/includes/top-nav')?>
<style>
	.car_rates {
		color: #fab700;
	}
	
	.car_likes_comments {
		text-align: center;
		margin-top: 10px;
	}
	.car_likes_comments a {
		text-decoration: none;
		color: #777777;
	}
	#gSignIn{
	  width: 100%;
	  margin-top: 20px;	
	}

	#gSignIn > div{
	  margin: 0 auto;
	}
</style>
			<!-- start banner Area -->
			<section class="banner-area relative" id="home">	
				<div class="overlay overlay-bg"></div>
				<div class="container">

					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								Login			
							</h1>	
							<p class="text-white link-nav"><a href="<?php echo base_url() ?>">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="<?php echo base_url('login') ?>"> Login</a></p>
						</div>											
					</div>
				</div>
			</section>
			<!-- End banner Area -->	

			
			<!-- Start blog-posts Area -->
			<section class="mt-30">
				<div class="container">
					<div class="text-center">
						<img src="http://butuanwheels.csjsolutions.net/assets/images/fav.png" width="150px" alt="" title="">
					</div>
					<h1 class="text-center mb-30">Butuan Wheels <br>Rent a Car Services</h1>

					<form class="form-area " action="<?php echo base_url('public/login/login_user') ?>" method="post" class="contact-form text-right">
						<div class="row">	
							<div class="col-lg-6 offset-md-3 form-group">
								<input name="email" placeholder="Enter email address" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$"  class="common-input mb-20 form-control" required="" type="email">

								<input name="password" placeholder="Enter your password" class="common-input mb-20 form-control" required="" type="password">
								<div class="mt-20 alert-msg text-danger" style="text-align: center;"><?php echo $this->session->login_error && ''; ?></div>
								<div class="text-center">
									<button type="submit" class="genric-btn warning large" style="width: 120px;">Login</button>	
									<a href="<?php echo base_url('register') ?>" class="genric-btn success large mt-20" style="width: 120px;">Register</a>	
									<div id="gSignIn"></div>
								</div>
								
							</div>
						</div>
					</form>	
				</div>	
			</section>
			<!-- End blog-posts Area -->
			
	
<meta name="google-signin-client_id" content="1051254273169-lgtjeqc7hucku98cd6hje32dfasfvqsf.apps.googleusercontent.com">
<script src="https://apis.google.com/js/platform.js"  ></script>
<script>
	renderButton();
	function renderButton() {
	    gapi.signin2.render('gSignIn', {
	        'scope': 'profile email',
	        'width': 240,
	        'height': 50,
	        'longtitle': true,
	        'theme': 'dark',
	        'onsuccess': signOut,
	        'onfailure': onFailure
	    });
	}
		function signOut(googleUser) {
		    var auth2 = gapi.auth2.getAuthInstance();
		    auth2.signOut().then(function () {
		      	console.log('User signed out.');
		      	var profile = googleUser.getBasicProfile();
		  		window.location.href = '<?php echo base_url() ?>'+'register?name='+profile.getName()+'&email='+profile.getEmail();

			  	console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
			  	console.log('Name: ' + profile.getName());
			  	console.log('Image URL: ' + profile.getImageUrl());
			  	console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
		    });
	  	}

	  	// Sign-in failure callback
		function onFailure(error) {
		    alert(error);
		}

	// });
</script>

<?php $this->load->view('public/includes/footer')?>
