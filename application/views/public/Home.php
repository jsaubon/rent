<?php $this->load->view('public/includes/header')?>
<?php $this->load->view('public/includes/top-nav')?>
<style>
	
	.default-select .current {
		white-space: pre-wrap;
	}
</style>


		<!-- start banner Area -->
		<section class="banner-area relative" id="home">
			<div class="overlay overlay-bg"></div>	
			<div class="container">
				<div class="row fullscreen d-flex align-items-center justify-content-center">
					<div class="banner-content col-lg-7 col-md-6 ">
						<h6 class="text-white ">Butuan Wheels</h6>
						<h1 class="text-white text-uppercase">
							Rent a Car Services				
						</h1>
						<p class="pt-20 pb-20 text-white" style="white-space: pre-line;">
							What's your drive today? Where would you go if you have 24 hours of freedom? A family trip? A summer dip? Partying 'til the break of day? Or going on a getaway? We've got your back!! 😍

Rent a Car and Drive in Style today with Butuan Wheels Rent a Car Services. Experience the freedom of hassle-free driving for as low as Php 1,300 per day. 😲😲😲

For Bookings and Reservations:
Call or message us at
📲 0948 - 373 - 5217 / 0910 - 718 - 0929
☎️ (085) 300 - 2173


						</p>
						<a href="<?php echo base_url('/cars') ?>" class="primary-btn text-uppercase">Rent Car Now</a>
					</div>
					<div class="col-lg-5  col-md-6 header-right">
						<h4 class="text-white pb-30">Book Your Car Today!</h4>
						<form id="formAddClient" method="POST" class="form" role="form" autocomplete="off">
						    <div class="form-group">
						       	<div class="default-select ">
									<select required name="car_id" class="select_car">
										<option value="" disabled selected hidden>Select Your Car</option>
										<?php foreach ($cars as $key => $value): ?>
											<option rates="<?php echo $value['rate'] ?>" value="<?php echo $value['car_id'] ?>"><?php echo $value['brand'].' '.$value['model'].' ('.$value['transmission'].')' ?></option>
										<?php endforeach ?>
									</select>
								</div>
						    </div>
						    <div class="form-group">
						       	<div class="default-select " >
									<select required name="car_rate" class="select_rate">
										<option value="" disabled selected hidden>Select Car Rate</option>
										
									</select>
								</div>
						    </div>
						    <div class="form-group row">
						        <div class="col-md-7 wrap-left">
									<div class="input-group dates-wrap">                                          
										<input required name="date_start" class="dates form-control" placeholder="Date" type="date" min="<?php echo date('Y-m-d') ?>" value="<?php echo date('Y-m-d',strtotime(' + 1 day')) ?>">                        
										<div class="input-group-prepend">
											<span  class="input-group-text"><span class="lnr lnr-calendar-full"></span></span>
										</div>											
									</div>
						        </div>
						        <div class="col-md-5 wrap-right">
									<div class="input-group dates-wrap">                                          
										<input required name="time_start" class="times_start form-control form-date" placeholder="Time" type="text">                        
										<div class="input-group-prepend">
											<span  class="input-group-text"><span class="lnr lnr-clock"></span></span>
										</div>											
									</div>
						        </div>

						    </div>
						    <div class="form-group row">
						        <div class="col-md-7 wrap-left">
									<div class="input-group dates-wrap">                                          
										<input required name="date_end" class="dates form-control" placeholder="Date" type="date" min="<?php echo date('Y-m-d') ?>" value="<?php echo date('Y-m-d',strtotime(' + 1 day')) ?>">                         
										<div class="input-group-prepend">
											<span  class="input-group-text"><span class="lnr lnr-calendar-full"></span></span>
										</div>											
									</div>
						        </div>
						        <div class="col-md-5 wrap-right">
									<div class="input-group dates-wrap">                                          
										<input required name="time_end" class="times_end form-control" placeholder="Time" data-format="hh:mm:ss" type="text">                        
										<div class="input-group-prepend">
											<span  class="input-group-text"><span class="lnr lnr-clock"></span></span>
										</div>											
									</div>
						        </div>
						    </div>						    
						    <div class="from-group <?php if ($this->session->userdata('client_data')): ?>hide <?php endif ?>">
						    	
						    	<input required class="form-control txt-field stronly" type="text" name="client_name" placeholder="Your name" value="<?php echo $user_data['client_name'] ?>">
						    	<input required class="form-control txt-field" type="email" name="client_email" placeholder="Email address" value="<?php echo $user_data['client_email'] ?>">
						    	<input required class="form-control txt-field numonly" type="tel" name="client_phone" placeholder="Phone number" value="<?php echo $user_data['client_phone'] ?>">
						    </div>
						    <div class="pgcalendar"></div>
						    <div class="form-group row">
						        <div class="col-md-12">
						            <button type="submit" class="btn btn-default btn-lg btn-block text-center text-uppercase">Confirm Car Booking</button>
						        </div>
						    </div>
						</form>
					</div>											
				</div>
			</div>					
		</section>
		<input type="hidden" value="">
		<?php


		?>
		<!-- End banner Area -->	
<script>
	$(document).ready(function() {
		$('select').niceSelect();
		// $('.dates').datepicker({
		// 	minDate: new Date(),
		// 	beforeShowDay: function(date) {
		// 		return true;
		// 	}
		// });

		// $('.dates').bootstrapMaterialDatePicker({
		// 		weekstart: 0,
		// 	 	time: false,
		// 	 	minDate: '<?php echo date('m/d/Y') ?>',
		// 	 	format: 'mm/DD/Y',
		// 	 	disabledDays: [
			 		
		// 	 	]
		// });

		$('.times_start').wickedpicker({now: "06:00"});
		$('.times_end').wickedpicker({now: "18:00"});
		$('.select_car').on('change', function(event) {
	        event.preventDefault();
	        var rates = $(this).find('option:selected').attr('rates');
	        rates = rates.split('<br>');
	        $('.select_rate').empty();
	        $('[name="car_rate"]').next().remove();
	        $('.select_rate').append('<option>Select Rate </option>');
	        $.each(rates, function(index, rate) {
	             $('.select_rate').append('<option value="'+rate+'">'+rate+'</option>');
	        });
	        $('.select_rate').niceSelect();

	    });

	    $('#formAddClient').on('submit', function(event) {
	        event.preventDefault();
	        var car_id = $('#formAddClient').find('[name="car_id"]').val();
	        var car_rate = $('[name="car_rate"]').next().find('.selected').attr('data-value');
	        var client_name = $('#formAddClient').find('[name="client_name"]').val();
	        var client_email = $('#formAddClient').find('[name="client_email"]').val();
	        var client_phone = $('#formAddClient').find('[name="client_phone"]').val();
	        var client_note = 'New Booking from Website';
	        var transaction_type = 'Self Drive';
	        var date_start = moment($('#formAddClient').find('[name="date_start"]').val()).format('YYYY-MM-DD');
	        var time_start = $('#formAddClient').find('[name="time_start"]').val();
	        var date_end = moment($('#formAddClient').find('[name="date_end"]').val()).format('YYYY-MM-DD');;
	        var time_end = $('#formAddClient').find('[name="time_end"]').val();
	        var transaction_status = 'Pending';
	        console.log(date_start);
	        if (car_rate) { 
	        	if (car_rate == 'Select Rate') {
	        		Swal.fire(
		        		'Error',
		        		'Please select Rate',
		        		'error'
		        	);
	        	} else {
	        		$.post('<?php echo base_url('public/bookings/save_client') ?>', {car_id,car_rate,client_name,client_email,client_phone,client_note,transaction_type,date_start,time_start,date_end,time_end,transaction_status} , function(data, textStatus, xhr) { 
			        	var swal_text = "We Created an Account for you!<br>your default password is your <b>phone number</b><br>please change your password on the profile page<br>thank you!";
			        	console.log(data);
			        	if (data == 'booking exist') {
			        		Swal.fire({
							  	title: 'Booking Date is not Available!',
							  	html: 'Please pick another date, thank you!',
							  	icon: 'error',
							  	showCancelButton: false,
							  	confirmButtonColor: '#3085d6',
							  	cancelButtonColor: '#d33',
							  	confirmButtonText: 'Okay'
							}).then((result) => {
							  	if (result.value) {
								   
							  	}
							})
			        	} else if (data == 'exist') {
			        		swal_text = '';
			        		Swal.fire({
							  	title: 'New Booking Submitted!',
							  	html: swal_text,
							  	icon: 'success',
							  	showCancelButton: false,
							  	confirmButtonColor: '#3085d6',
							  	cancelButtonColor: '#d33',
							  	confirmButtonText: 'Okay'
							}).then((result) => {
							  	if (result.value) {
								    $.post('<?php echo base_url('public/login/login_user') ?>', {email:client_email,password:client_phone}, function(data, textStatus, xhr) {
								    	console.log(data);
								    		window.location.href = '<?php echo base_url('profile') ?>';
								    });
							  	}
							})

				            $('#formAddClient')[0].reset();
				            $('[name="client_car_rate_select"]').empty();
			        	} else {
			        		Swal.fire({
							  	title: 'New Booking Submitted!',
							  	html: swal_text,
							  	icon: 'success',
							  	showCancelButton: false,
							  	confirmButtonColor: '#3085d6',
							  	cancelButtonColor: '#d33',
							  	confirmButtonText: 'Okay'
							}).then((result) => {
							  	if (result.value) {
								    $.post('<?php echo base_url('public/login/login_user') ?>', {email:client_email,password:client_phone}, function(data, textStatus, xhr) {
								    	console.log(data);
								    		window.location.href = '<?php echo base_url('profile') ?>';
								    });
							  	}
							})

				            $('#formAddClient')[0].reset();
				            $('[name="client_car_rate_select"]').empty();

			        	}

			        	
			        	
				            
			        });
	        	}
		        	
	        } else {
	        	Swal.fire(
	        		'Error',
	        		'Please select Rate',
	        		'error'
	        	);
	        }

	        
		        
	    });
	});
	

	    
</script>		
</script>

<?php $this->load->view('public/includes/footer')?>
