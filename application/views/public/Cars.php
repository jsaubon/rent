<?php $this->load->view('public/includes/header')?>
<?php $this->load->view('public/includes/top-nav')?>
<style>
	.car_rates {
		color: #fab700;
	}
	
	.car_likes_comments {
		text-align: center;
		margin-top: 10px;
	}
	.car_likes_comments a {
		text-decoration: none;
		color: #777777;
	}

	.genric-btn.primary.active {
	    color: #fab700 !important;
	    border: 1px solid #fab700 !important;
	    background: #fff !important;
	}

</style>
			<!-- start banner Area -->
			<section class="banner-area relative" id="home">	
				<div class="overlay overlay-bg"></div>
				<div class="container">
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								Cars			
							</h1>	
							<p class="text-white link-nav"><a href="<?php echo base_url() ?>">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="<?php echo base_url('cars') ?>"> Cars</a></p>
						</div>											
					</div>
				</div>
			</section>
			<!-- End banner Area -->	

			
			<!-- Start blog-posts Area -->
			<section class="blog-posts-area section-gap">
				<div class="container">
					<h2>Available Cars to Rent</h2>			
					<div class="row mt-20">
						<div class="col-lg-8 post-list blog-post-list ">
							<div class="car-list" style="">
								<?php foreach ($available_cars as $key => $value): ?>
									<div class="row align-items-center single-model item img-thumbnail p-3 mb-3">
										<div class="col-lg-6 model-left">
											<div class="title justify-content-between d-flex">
												<h3 class="mt-10"><?php echo $value['brand'] ?> <?php echo $value['model'] ?></h3>
												<!-- <h2>echo<span>/day</span></h2> -->
											</div>
											<p>
												<?php echo $value['description'] ?>
											</p>
											<p>
												Capacity         : <?php echo $value['capacity'] ?> Person <br>
												Transmission     : <?php echo $value['transmission'] ?> <br>
												<hr>
												<strong class="car_rates">
													<h5>Rates</h5>
													<?php 
														$rates = explode('<br>',$value['rate']);
														foreach ($rates as $key => $rate) {
															echo $rate.'<small>/day</small><br>';
														}
													?>
												</strong>
											</p>
											
										</div>
										<div class="col-lg-6 model-right">
											<img class="img-fluid" src="<?php echo base_url('assets/uploads/'.$value['image']) ?>" width="100%" height="100%" alt="">
											<div class="text-center">
												<a class="text-uppercase primary-btn mt-10" href="<?php echo base_url('cars/detail/'.$value['car_id']) ?>">Show Details</a>
											</div>
											<div class="car_likes_comments">
											</div>
											
											
										</div>
									</div>
								<?php endforeach ?>
							</div>		
							<div class="text-center">
								<?php for ($i=0; $i < $paginate_number;  $i++) { ?>
									<a offset="<?php echo $i*5 ?>" href="<?php echo base_url('cars') ?>?offset=<?php echo $i*5 ?>&brand=<?php echo $this->input->get('brand') ?>" class="genric-btn primary"><?php echo $i+1 ?></a>
								<?php }?>
							</div>
								
						</div>
						<div class="col-lg-4 sidebar">
							<!-- <div class="single-widget search-widget">
								<div class="input-group-icon mt-10">
									<div class="icon"><i class="fa fa-car" aria-hidden="true"></i></div>
										<div class="form-select" id="default-select">
											<select id="select_car_brand">
												<option value="" disabled selected hidden>Select Your Car</option>
												<?php foreach ($brands as $key => $value): ?>
													<option value="<?php echo $value['brand_id'] ?>"><?php echo $value['brand'] ?></option>
												<?php endforeach ?>
											</select>
										</div>
								</div>
								<div class="input-group-icon mt-10 hide" id="select_car_model_container">
									<div class="icon"><i class="fa fa-car" aria-hidden="true"></i></div>
										<div class="form-select" id="default-select">
											<select id="select_car_model">
												<option value="" disabled selected hidden>Select Car Model</option>
												
											</select>
										</div>
								</div>
							</div> -->

							

							<div class="single-widget category-widget">
								<h4 class="title">Available Vehicles</h4>
								<ul>
									<?php foreach ($brands as $key => $brand): ?>
										<li><a href="<?php echo base_url('cars') ?>?offset=0&brand=<?php echo $brand['brand'] ?>" class="justify-content-between align-items-center d-flex"><h6><?php echo $brand['brand'] ?></h6> <span><?php echo $brand['count'] ?></span></a></li>
									<?php endforeach ?>
								</ul>
							</div>
						</div>
					</div>
				</div>	
			</section>
			<!-- End blog-posts Area -->
			
	
<script>
	$(document).ready(function() {
		$('select').niceSelect();
		$('#select_car_brand').on('change', function(event) {
			event.preventDefault();
			var brand_id = $(this).val();
			get_car_models(brand_id);
			get_cars({brand_id:brand_id});
		});

		$('#select_car_model').on('change', function(event) {
			event.preventDefault();
			var brand_id = $('#select_car_brand').val();
			var model_id = $(this).val();
			get_cars({brand_id:brand_id,model_id:model_id});
		});

		function get_car_models(brand_id) {
			var data =  {
			                table: 'tbl_car_model',
			                action: 'get',
			                select: {
			                            
			                        },
			                where:  {
										brand_id:brand_id
			                        },
			                field: '',
			                order: '',
			                limit: 0,
			                offset: 0,
			                group_by: ''
			
			            };
			$('#select_car_model').empty();
			$.post('<?php echo base_url('public/cars/modelTable') ?>', data, function(data, textStatus, xhr) {
				data = JSON.parse(data);
				$('#select_car_model').append('<option value="" disabled selected hidden>Select Car Model</option>');
				$.each(data, function(index, val) {
					$('#select_car_model').append('<option value="'+val.model_id+'" >'+val.model+'</option>');
				});
				$('select').niceSelect('update');
				$('#select_car_model_container').removeClass('hide');
			});		
		}

		function get_cars(car_data) {

			$.post('<?php echo base_url('public/cars/get_cars') ?>', car_data, function(data, textStatus, xhr) {
				data = JSON.parse(data);
				$('.car-list').empty();
				$.each(data, function(index, val) {
					var rates = (val.rate).split('<br>');
					$.each(rates, function(i, v) {
						rates[i] = v+'<small>/day</small><br>';
					});
					rates = rates.join('<br>');
					var new_car = '<div class="row align-items-center single-model item img-thumbnail p-3 mb-3">\
										<div class="col-lg-6 model-left">\
											<div class="title justify-content-between d-flex">\
												<h3 class="mt-10">'+val.brand+' '+val.model+'</h3>\
												<!-- <h2>echo<span>/day</span></h2> -->\
											</div>\
											<p>\
												'+val.description+'\
											</p>\
											<p>\
												Capacity         : '+val.capacity+' Person <br>\
												Transmission     : '+val.transmission+' <br>\
												<hr>\
												<strong class="car_rates">\
													<h5>Rates</h5>\
													'+rates+'\
												</strong>\
											</p>\
											\
										</div>\
										<div class="col-lg-6 model-right">\
											<img class="img-fluid" src="<?php echo base_url('assets/uploads/') ?>'+val.image+'" width="100%" height="100%" alt="">\
											<div class="text-center">\
												<a class="text-uppercase primary-btn mt-10" href="<?php echo base_url('cars/detail/') ?>'+val.car_id+'">Show Details</a>\
											</div>\
											<div class="car_likes_comments">\
												<a href="#"><span class="lnr lnr-heart"></span>	4 likes</a>\
												<a href="#"><span class="lnr lnr-bubble"></span> 06 Comments</a>\
											</div>\
										</div>\
									</div>';
					$('.car-list').append(new_car);
				});
			});
		}



		var offset = '<?php echo $this->input->get('offset'); ?>';

		if (offset == '' || offset== 0) {
			console.log(offset);
			$('[offset="0"]').addClass('active');
		} else {

			$('[offset="'+offset+'"]').addClass('active');
		}
	});
</script>

<?php $this->load->view('public/includes/footer')?>
