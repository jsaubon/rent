<?php

/**
 *
 */
class MY_Controller extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		// $this->checkLog();
		$this->session->transaction_statuses = $this->fetchRawData("SELECT * FROM view_tbl_transaction_statuses ORDER BY status_id ASC");
		$update = $this->db->query("UPDATE tbl_client_booking SET transaction_status='Cancelled' WHERE transaction_status='Pending' AND DATE(date_start)<CURDATE()");
	}

	function format_rates($rates) {
		if ($rates != '') {
			$rates = explode('<br>',$rates);
			foreach ($rates as $key => $rate) {
				$rate = explode(' ',$rate);
				$rate[count($rate) -1] = '₱'.number_format($rate[count($rate) -1]);
				$rate = implode(' ',$rate);
				$rates[$key] = $rate;
			}

			$rates = implode('<br>',$rates);
		}	

		return $rates;
	}


	function get_encrypted_string($all_url) {
		$this->load->library('encrypt');
		$exploded_url = explode('?', $all_url);
		if (!empty($exploded_url[1])) {
			$exploded_url[1] = $this->encrypt->encode($exploded_url[1]);
			// $this->pprint($exploded_url);
			$all_url = implode('?temp=', $exploded_url);
			
		}


		$all_url = $this->bitly_short_url($all_url);
		return $all_url;
			
	}


	function get_decrypted_string($temp) {
		$temp = str_replace(' ', '+', $temp);
		$this->load->library('encrypt');
		$temp = $this->encrypt->decode($temp);
		return $temp;
	}

	function isJson($str) {
	    $json = json_decode($str);
	    return $json && $str != $json;
	}

	public function trim_str($str) {
		$str = trim($str);
		$str = strip_tags($str);
		$str = stripslashes($str);
		$str = str_replace("'", "\'", $str);
		return $str;
	}

	public function pprint($arr) {
		echo "<pre>";
		print_r($arr);
		echo "</pre>";
	}

	public function fetchRawData($query) {
		$query = $this->db->query($query);
		return $query->result_array();
	}

	function checkLog() {
		$userdata = $this->session->userdata('user_data');
		$login_folder = $userdata['login_folder'];
		$this->load->library('user_agent');
		if (!empty($userdata) && $this->uri->segment(2) == 'login') {
			redirect(base_url('admin/dashboard')); 
		} else if (empty($userdata) && $this->uri->segment(2) != 'login') {
			redirect(base_url('admin/login'));
		}
	}

	function lookUpCardNumber() {
		$card_number = $this->input->post('card_number');
		// $card_number = 12345112;
		// Get cURL resource
		$curl = curl_init();
		// Set some options - we are passing in a useragent too here
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => 'https://lookup.binlist.net/' . $card_number,
		));
		// Send the request & save response to $resp
		$resp = curl_exec($curl);
		// echo $resp;
		echo $resp;
		// Close request to clear up some resources
		curl_close($curl);
	}

	private function trim_str_fetch_data($str) {
		$str = trim($str);
		// $str = strip_tags($str);
		$str = stripslashes($str);
		$str = str_replace("\\", '', $str);
		return $str;
	}


	function phone_trim($phone) {
		$phone = str_replace('(', '', $phone);
		$phone = str_replace(')', '', $phone);
		$phone = str_replace('-', '', $phone);
		$phone = str_replace(' ', '', $phone);
		return $phone;
	}


	function json_validator($data=NULL) {
	  if (!empty($data)) {
	                @json_decode($data);
	                return (json_last_error() === JSON_ERROR_NONE);
	        }
	        return false;
	}

	function modelTable() {
		$data = $this->input->post(); 

		$this->load->model('Model_' . $data['table']);
		$model = 'Model_' . $data['table'];
		$db_table = new $model;
		if ($data['action'] == 'get') {
			if (isset($data['select'])) {
				$db_table->selects = $data['select'];
			}
			$get_data = $db_table->search(isset($data['where']) ? $data['where'] : [], $data['field'], $data['order'], $data['limit'], $data['offset'], $data['group_by']);
			echo json_encode($get_data);
		} else { 
			if ($this->isJson($data['pk'])) { 
				$where = json_decode($data['pk'],TRUE);
				$data_search = $db_table->search($where);
				// $this->pprint($data);
				if (count($data_search) > 0) {
					foreach ($data_search as $key => $value) {
						$pk = reset($value);
						$db_table = new $model;
						$db_table->load($pk);
						if (isset($data['fields'])) {
							foreach ($data['fields'] as $field => $value) {
								$db_table->$field = $value;
							} 
						}
							
						if ($data['action'] == 'save') {
							$db_table->save();
						}
						if ($data['action'] == 'delete') {
							$db_table->delete();
						}  
					}
				} else { 
					$db_table = new $model; 
					if (isset($data['fields'])) {
						foreach ($data['fields'] as $field => $value) {
							$db_table->$field = $value;
						} 
					}
						
					if ($data['action'] == 'save') {
						$db_table->save();
					}
					if ($data['action'] == 'delete') {
						$db_table->delete();
					}   
				}
					
 
				 
			} else{ 
				if ($data['pk'] != '') {
					$db_table->load($data['pk']);
				}
				
				if (isset($data['fields'])) {
					foreach ($data['fields'] as $field => $value) {
						$db_table->$field = $value;
					} 
				}
				if ($data['action'] == 'save') {
					$db_table->save();
				}
				if ($data['action'] == 'delete') {
					$db_table->delete();
				} 
				echo reset($db_table);
			} 

				
		}

	}



}