<?php
class MY_Model extends CI_Model {
	const DB_TABLE = "default";
	const DB_TABLE_PK = "default";

	// toJoin *array -> array('model to join' => 'model already joined or loaded');
	public $toJoin = array();
	public $selects = array();
	// $order_field and $order_type used for order queries
	public $sqlQueries = array("order_field" => '',
		"order_type" => '',
		"toGroup" => '',
		"join_type" => '',
		"selects" => '*',
	);

	// $ss-sqlQueries['join_type'] = 'LEFT';

	public function getSelect() {
		$selectQry = "";
		if ($this->selects) {
			$counter = 0;
			foreach ($this->selects as $key => $value) {
				$counter++;
				if ($counter == count($this->selects)) {
					$selectQry .= " {$value} ";
				} else {
					$selectQry .= " {$value}, ";
				}
			}
		}
		return $selectQry;
	}
	// use instantiate for more than one record
	public function instatiate($qry_result) {
		$toret = array();
		$class = get_class($this);
		foreach ($qry_result as $row) {
			$model = new $class;
			$model->populate($row);
			$toret[$row->{$this::DB_TABLE_PK}] = $model;
		}
		return $toret;
	}

	// use populate if you are looking for only one record
	public function populate($row) {
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
	}
	public function load($id, $getRows = 0) {
		if ($this->getSelect() != "") {
			$qry->select($this->getSelect());
		}
		$this->db->from($this::DB_TABLE);
		$this->addJoin();
		$query = $this->db->where(array($this::DB_TABLE . "." . $this::DB_TABLE_PK => $id));
		$query = $this->db->get();
		$this->populate($query->row());
	}
	public function empty_table() {
		$this->db->empty_table($this::DB_TABLE);
	}
	public function delete($all = false) {
		$this->db->delete($this::DB_TABLE, array(
			$this::DB_TABLE_PK => $this->{$this::DB_TABLE_PK}));
		unset($this->{$this::DB_TABLE_PK});
	}
	public function save() {
		if (!empty($this->{$this::DB_TABLE_PK})) {
			$this->update();
		} else {
			$this->insert();
		}

		// $this->insertLog();
	}
	public function get($limit = 0, $offset = 0) {
		$this->db->select('*');
		$this->db->from($this::DB_TABLE);
		$this->addJoin();
		$this->addGroup();
		$this->addOrder();
		if ($limit) {
			$query = $this->db->limit($limit, $offset);
		}
		$qry = $this->db->get();
		$toret = $this->instatiate($qry->result());
		return $this->fetchArray($toret);
	}
	public function get_join() {
		// echo "asddas";
		$this->db->select('*');
		$this->db->from($this::DB_TABLE);
		$this->addJoin();
		$query = $this->db->get();
		$toret = $this->instatiate($query->result());

		return $toret;

	}
	// @search
	// @parameter $where => array('field' => value)
	// @parameter $joins => array( 'model to join'=> 'model already joined' )
	public function search($where = [], $field = '', $order = '', $limit = 0, $offset = 0, $group_by = '') {
		if ($this->getSelect() != "") {
			$this->db->select($this->getSelect());
		}
		$this->db->from($this::DB_TABLE);
		$this->addJoin();
		$this->db->where($where);
		if ($field != '') {
			$this->db->order_by($field, $order);
		}
		if ($group_by != '') {
			$this->db->group_by($group_by);
		}
		if ($limit) {
			$this->db->limit($limit);
			$this->db->offset($offset);
			$qry = $this->db->get();
		} else {
			$qry = $this->db->get();
		}
		$toret = $this->instatiate($qry->result());
		return $this->fetchArray($toret);
	}
	public function whereLike($orLikes, $andLikes, $where, $orWhere, $between = array(), $limit = 0, $offset = 0) {
		$counter = 0;
		$qry = $this->db;
		if ($this->getSelect() != "") {
			$qry->select($this->getSelect());
		}
		$qry->from($this::DB_TABLE);
		$this->addJoin('left');
		$whereQuery = "";

		if ($orLikes) {
			$whereQuery .= "(";
			foreach ($orLikes as $key => $value) {
				$counter++;
				if ($counter == 1) {
					if (is_int($value)) {
						$whereQuery .= "{$key} = {$value} ";
					} else {
						$whereQuery .= "{$key} like '%{$value}%' ";
					}
				} else {
					if (is_int($value)) {
						$whereQuery .= "OR {$key} = {$value} ";
					} else {
						$whereQuery .= "OR {$key} like '%{$value}%' ";
					}
				}
			}
			$whereQuery .= ")";
			if ($andLikes OR $where OR $between || $orWhere) {
				$whereQuery .= " AND ";
			}
		}
		if ($andLikes) {
			$whereQuery .= "( ";
			$counter = 0;
			foreach ($andLikes as $key => $value) {
				$counter++;
				if ($counter == 1) {
					if (is_int($value)) {
						$whereQuery .= "{$key}  = {$value} ";
					} else {
						$whereQuery .= "{$key} like '%{$value}%' ";
					}
				} else {
					if (is_int($value)) {
						$whereQuery .= "AND {$key} = {$value} ";
					} else {
						$whereQuery .= "AND {$key} like '%{$value}%' ";
					}
				}
			}
			$whereQuery .= ") ";
			if ($where || $orWhere || $between) {
				$whereQuery .= " AND ";
			}

		}
		if ($where) {
			$counter = 0;
			$whereQuery .= "( ";
			foreach ($where as $key2 => $value2) {
				$counter++;
				foreach ($value2 as $key => $value) {
					if ($counter == 1) {
						if (is_int($value)) {
							$whereQuery .= "{$key} = {$value} ";
						} else {
							$whereQuery .= "{$key} = '{$value}' ";
						}
					} else {
						if (is_int($value)) {
							$whereQuery .= "AND {$key} = {$value} ";
						} else {
							$whereQuery .= "AND {$key} = '{$value}' ";
						}
					}
				}

			}
			$whereQuery .= ")";
			if ($orWhere || $between) {
				$whereQuery .= " AND ";
			}
		}
		if ($orWhere) {
			$whereQuery .= "( ";
			$counter = 0;
			foreach ($orWhere as $key2 => $value2) {
				foreach ($value2 as $key => $value) {
					$counter++;
					foreach ($value as $key3 => $value3) {
						if ($counter == 1) {
							if (is_int($value)) {
								$whereQuery .= "{$key3} = {$value3} ";
							} else {
								$whereQuery .= "{$key3} = '{$value3}' ";
							}
						} else {
							if (is_int($value)) {
								$whereQuery .= "OR {$key3} = {$value3} ";
							} else {
								$whereQuery .= "OR {$key3} = '{$value3}' ";
							}
						}
					}
				}
			}
			$whereQuery .= ")";
			if ($between) {
				$whereQuery .= " AND ";
			}
		}
		if ($between) {
			$whereQuery .= "( ";
			$counter = 0;
			foreach ($between as $key2 => $value2) {
				$whereQuery .= "{$key2} BETWEEN ";
				$counter = 0;
				foreach ($value2 as $key3 => $value3) {
					$counter++;
					if (is_int($value3)) {
						if ($counter == 2) {
							$whereQuery .= "{$value3}";
						} else {
							$whereQuery .= "{$value3} AND ";
						}
					} else {
						if ($counter == 2) {
							$whereQuery .= "'{$value3}'";
						} else {
							$whereQuery .= "'{$value3}' AND ";
						}
					}
				}

			}
			$whereQuery .= ")";
		}

		$qry->where($whereQuery);
		$this->addOrder();
		if ($this->sqlQueries['toGroup']) {
			$this->db->group_by($this->sqlQueries['toGroup']);
		}
		if ($limit) {
			$this->db->limit($limit, $offset);
		}
		$query = $this->db->get();

		return $this->instatiate($query->result());
	}
	public function load_last_input() {
		$this->get_order_limit1($this::DB_TABLE_PK, 'desc', array());
	}
	public function load_first_input() {
		$this->get_order_limit1($this::DB_TABLE_PK, 'ASC', array());
	}
	public function get_order_limit1($field, $order, $where) {
		$this->db->from($this::DB_TABLE);
		$this->addJoin();
		$this->db->where($where);
		$this->db->order_by($field, $order);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query) {
			$this->populate($query->row());
		}
	}
	public function get_ordered($where, $field, $order) {
		$qry = $this->db;
		$qry->from($this::DB_TABLE);
		$qry->where($where);
		$qry->order_by($field, $order);
		$query = $this->db->get();
		$toret = $this->instatiate($query->result());

		return $this->fetchArray($toret);
	}

	private function insert() {
		$this->cleanModel();
		$this->db->insert($this::DB_TABLE, $this);
		$this->{$this::DB_TABLE_PK} = $this->db->insert_id();
	}

	private function insertLog() {
		$this->cleanModel();
		$userdata = $this->session->userdata('user_data');
		$json_log = json_encode($this);
		if (isset($userdata)) {
			if ($this::DB_TABLE != 'logs') {
				$this->load->model('Model_logs');
				$logs = new Model_logs();
				$logs->user_type = $userdata['login_type'];
				$logs->user_id = $userdata['user_id'];
				$logs->json_log = $json_log;
				$logs->date = date('Y-m-d H:i:s');
				$logs->ip_address = $this->input->ip_address();
				$logs->save();
				if ($this::DB_TABLE == 'clients' || $this::DB_TABLE == 'client_joints') {
					//$this->addToList($this->email_address, $this->name, $this->cell_phone, $this->getConverted($this->client_id) == '1' ? $this->client_status : $this->getLeadStatus($this->client_id), $this->getConverted($this->client_id) == '1' ? 'Client' : 'Lead', $this->date_of_birth);
				}
			}
		}
			

	}

	private function getConverted($client_id) {
		$data = $this->db->query("SELECT converted FROM clients WHERE client_id=$client_id LIMIT 1");
		$data = $data->result_array();
		$data = reset($data);
		$data = reset($data);
		return $data;
	}
	private function getLeadStatus($client_id) {
		$data = $this->db->query("SELECT lead_status FROM clients WHERE client_id=$client_id LIMIT 1");
		$data = $data->result_array();
		$data = reset($data);
		$data = reset($data);
		return $data;
	}

	public function force_insert() {
		$this->cleanModel();
		$this->db->insert($this::DB_TABLE, $this);
		$this->{$this::DB_TABLE_PK} = $this->db->insert_id();
	}
	private function addJoin($joinType = null) {
		if (isset($this->sqlQueries['join_type'])) {
			if ($this->sqlQueries['join_type'] != "") {
				$joinType = $this->sqlQueries['join_type'];
			}
		}
		if (count($this->toJoin) > 0) {
			foreach ($this->toJoin as $key => $value) {
				$this->load->model($key);
				$this->load->model($value);
				$classname2 = ucfirst($value);
				$classname = ucfirst($key);
				$model1 = new $classname;
				$model2 = new $classname2;
				if (property_exists($model2, $model1::DB_TABLE_PK)) {
					$commonPk = $model1::DB_TABLE_PK;
				} elseif (property_exists($model1, $model2::DB_TABLE_PK)) {
					$commonPk = $model2::DB_TABLE_PK;
				}

				$this->db->join($model1::DB_TABLE, $model1::DB_TABLE . "." . $commonPk . " = " . $model2::DB_TABLE . "." . $commonPk, $joinType, "left outer");
			}
		}
	}
	private function update() {
		$this->cleanModel();
		$this->db->where($this::DB_TABLE_PK, $this->{$this::DB_TABLE_PK});
		$this->db->update($this::DB_TABLE, $this);
	}
	private function addOrder() {
		if ($this->sqlQueries['order_field'] != '' and $this->sqlQueries['order_type'] != '') {
			$this->db->order_by($this->sqlQueries['order_field'], $this->sqlQueries['order_type']);
		}
	}
	private function addGroup() {
		if ($this->sqlQueries['toGroup'] != '') {
			$this->db->group_by($this->sqlQueries['toGroup']);
		}
	}

	public function searchWithSelect($where = [], $field = '', $order = '', $limit = 0, $offset = 0, $group_by = '') {
		if ($this->getSelect() != "") {
			$this->db->select($this->getSelect());
		}
		$this->db->from($this::DB_TABLE);
		$this->addJoin();
		$this->db->where($where);
		if ($group_by != '') {
			$this->db->group_by($group_by);
		}
		if ($field != '') {
			$this->db->order_by($field, $order);
		}

		if ($limit) {
			$this->db->limit($limit);
			$this->db->offset($offset);
			$qry = $this->db->get();
		} else {
			$qry = $this->db->get();
		}
		// $toret = $this->instatiate($qry->result());
		$result = $qry->result_array();
		foreach ($result as $index => $value) {
			foreach ($value as $key => $value) {
				$result[$index][$key] = $this->trim_str_fetch($value);
			}
		}

		return $result;
	}

	private function fetchArray($data) {
		if (is_array($data) || is_object($data)) {
			$result = array();
			foreach ($data as $key => $value) {
				$result[$key] = $this->fetchInnerArray($value);

			}
			unset($result['sqlQueries']);
			unset($result['selects']);
			unset($result['toJoin']);

			foreach ($result as $index => $value) {
				foreach ($value as $key => $value) {
					$result[$index][$key] = $this->trim_str_fetch($value);
				}
			}

			return array_values($result);
		}

		return $data;
	}

	private function fetchInnerArray($data) {
		if (is_array($data) || is_object($data)) {
			$result = array();
			foreach ($data as $key => $value) {
				$result[$key] = $this->fetchInnerArray($value);

			}
			unset($result['sqlQueries']);
			unset($result['selects']);
			unset($result['toJoin']);

			return $result;
		}

		return $data;
	}

	private function cleanModel() {
		foreach ($this as $field => $value) {
			if (!is_array($value)) {
				$this->$field = $this->trim_str($value);
			}
		}
	}

	private function trim_str($str) {
		$str = trim($str);
		$str = str_replace('<script>', '', $str);
		$str = str_replace('</script>', '', $str);
		// $str = addslashes($str);
		return $str;
	}

	private function trim_str_fetch($str) {
		$str = trim($str);
		// $str = strip_tags($str);
		$str = stripslashes($str);
		$str = str_replace("\\", '', $str);
		return $str;
	}

}